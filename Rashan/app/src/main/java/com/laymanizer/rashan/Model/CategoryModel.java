package com.laymanizer.rashan.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhimanyu on 09-10-2015.
 */
public class CategoryModel implements Model {
    String mCategoryId;
    String mCategoryName;
    String mCategoryDetail;
    String mImageUrl;
    List<Model> mSubCategoryModelList = null;

    @Override
    public String getId() {
        return mCategoryId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mCategoryName;
    }

    public String getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(String mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public String getCategoryName() {
        return mCategoryName;
    }

    public String getCategoryDetail() {
        return mCategoryDetail;
    }

    public void setCategoryDetail(String mCategoryDescription) {
        this.mCategoryDetail = mCategoryDescription;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setCategoryName(String mCategoryName) {
        this.mCategoryName = mCategoryName;
    }

    public List<Model> getSubCategoryModelList() {
        return mSubCategoryModelList;
    }

    public void setSubCategoryModelList(List<Model> mSubCategoryModelList) {
        this.mSubCategoryModelList = mSubCategoryModelList;
    }

    public void addToCategoryModelList(Model model){
        if(mSubCategoryModelList == null) {
            mSubCategoryModelList = new ArrayList<Model>();
        }
        mSubCategoryModelList.add(model);
    }

    public void removeFromCategoryModelList(Model model) {
        if (mSubCategoryModelList != null) {
            mSubCategoryModelList.remove(model);
            if (mSubCategoryModelList.size() == 0) {
                mSubCategoryModelList = null;
            }
        }
    }

    public int getCategoryModelListSize() {
        if (mSubCategoryModelList == null) {
            return 0;
        }
        return mSubCategoryModelList.size();
    }
}
