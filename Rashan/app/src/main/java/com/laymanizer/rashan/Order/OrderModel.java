package com.laymanizer.rashan.Order;

import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.Model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhimanyu on 03-01-2016.
 */
public class OrderModel implements Model{
    String mOrderId;
    String mSubTotal;
    String mDeliveryCharges;
    String mPayableAmount;
    String mOrderDate;
    String mDeliveryDateTime;
    AddressModel mDeliveryAddress;
    String mOrderStatus;
    List<Model> mStoreOrderList = new ArrayList<>();

    @Override
    public String getId() {
        return mOrderId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return null;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String mOrderId) {
        this.mOrderId = mOrderId;
    }

    public String getSubTotal() {
        return mSubTotal;
    }

    public void setSubTotal(String mSubTotal) {
        this.mSubTotal = mSubTotal;
    }

    public String getDeliveryCharges() {
        return mDeliveryCharges;
    }

    public void setDeliveryCharges(String mDeliveryCharges) {
        this.mDeliveryCharges = mDeliveryCharges;
    }

    public String getPayableAmount() {
        return mPayableAmount;
    }

    public void setPayableAmount(String mPayableAmount) {
        this.mPayableAmount = mPayableAmount;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String mOrderDate) {
        this.mOrderDate = mOrderDate;
    }

    public String getDeliveryDateTime() {
        return mDeliveryDateTime;
    }

    public void setDeliveryDateTime(String mDeliveryDateTime) {
        this.mDeliveryDateTime = mDeliveryDateTime;
    }

    public AddressModel getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(AddressModel mDeliveryAddress) {
        this.mDeliveryAddress = mDeliveryAddress;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String mOrderStatus) {
        this.mOrderStatus = mOrderStatus;
    }

    public List<Model> getStoreOrderList() {
        return mStoreOrderList;
    }

    public void setStoreOrderList(List<Model> mStoreList) {
        this.mStoreOrderList = mStoreList;
    }

    public void addStoresOrderInOrder(Model store) {
        mStoreOrderList.add(store);
    }
}