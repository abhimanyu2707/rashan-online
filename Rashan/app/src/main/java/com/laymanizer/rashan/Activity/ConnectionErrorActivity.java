package com.laymanizer.rashan.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.ConnectionUtil;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

/**
 * Created by Laymanizer Pvt Ltd on 18-08-2015.
 */
public class ConnectionErrorActivity extends Activity {

    Button mRetryButton;
    LinearLayout mConnectionLayout;
    ConnectionUtil mConnectionUtil;
    SharedPreferenceUtil mSharedPreferenceUtil;
    ProgressDialog mProgressDialog;
    Handler mHandler;
    final int TIME_OUT = 200;
    final int MAX_CHECK = 5;
    final int CHECK_CONNECTION = 100;
    int mChecker = 0;
    Context mContext;
    private Boolean mExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection_error);
        mConnectionUtil = ConnectionUtil.getInstance();
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(this);
        mContext = this;
        setHandler();
        initViews();
    }

    /* todo:  need to add image animation instead of progress bar*/

    private void initViews() {
        mRetryButton = (Button) findViewById(R.id.button_retry);
        mConnectionLayout = (LinearLayout) findViewById(R.id.connection_error_layout);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mRetryButton.setOnClickListener(mClickListener);
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mRetryButton.setEnabled(false);
            mChecker = 0;
            mProgressDialog.show();
            mHandler.sendEmptyMessageDelayed(CHECK_CONNECTION,TIME_OUT);
        }
    };

   public void setHandler() {
       mHandler = new Handler() {
           @Override
           public void handleMessage(Message msg) {
              switch (msg.what) {
                  case CHECK_CONNECTION :
                      if (mConnectionUtil.isConnectedToInternet(mContext)|| mChecker > MAX_CHECK) {
                          mProgressDialog.hide();
                          mHandler.removeMessages(CHECK_CONNECTION);
                          if (mConnectionUtil.isConnectedToInternet(mContext)) {
                             if (Const.SplashActivity.equals(getIntent().getStringExtra(Const.LaunchFrom))){
                                 if (mSharedPreferenceUtil != null && !mSharedPreferenceUtil.isLocationPresent()) {
                                     launchActivity(Const.LocationEntryActivity);
                                 } else {
                                     launchActivity(Const.HomeActivity);
                                 }
                             }
                             finish();
                          } else {
                              mRetryButton.setEnabled(true);
                          }
                      } else {
                          mChecker++;
                          mHandler.sendEmptyMessageDelayed(CHECK_CONNECTION, TIME_OUT);
                      }
                      break;
              }
           }
       };
   }

    private void launchActivity(Class activityName){
        Intent intent = new Intent(this,activityName);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (mExit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            mExit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mExit = false;
                }
            }, 2 * 1000);

        }

    }

}
