package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.laymanizer.rashan.Adapter.HomeAdapter;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.ConnectionUtil;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;
import com.laymanizer.rashan.View.DrawerManager;
import com.laymanizer.rashan.View.ErrorDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quantumgraph.sdk.QG;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, QueryHelper.IQueryHelperCallback,ErrorDialog.IErrorDialogListener {

    SharedPreferenceUtil mSharedPreferenceUtil;
    QueryHelper mQueryHelper;
    ErrorDialog mErrorDialog;
    ConnectionUtil mConnectionUtil;

    Context mContext;
    private Boolean mExit = false;

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    HomeAdapter mAdapter;
    LinearLayout mDrawerView;
    View mRootLayout;
    LinearLayout mLayoutProgress;

    DrawerManager mDrawerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("Abhi", "HomeActivity onCreate");
        super.onCreate(savedInstanceState);
        mContext = this;
        mRootLayout = LayoutInflater.from(mContext).inflate(R.layout.home_activity, null);
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        drawer.setPadding(0, getStatusBarHeight(), 0, 0);

        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(this);
        mQueryHelper = new QueryHelper(this);
        mQueryHelper.setQueryHelperCallback(this);
        mErrorDialog = new ErrorDialog(this);
        mErrorDialog.setOnRetrySelectedListener(this);
        mConnectionUtil = ConnectionUtil.getInstance();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        initView();
        if(Const.PaymentActivityName.equals(getIntent().getStringExtra(Const.LaunchFrom))) {
            Log.e("keelback", "order");
            launchActivity(Const.OrderConfirmActivity, getIntent().getStringExtra("delivery_date"), getIntent().getStringExtra("delivery_time"));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        QG qg = QG.getInstance(getApplicationContext());
        qg.onStart("b5bd9b52da219d9c1ca4");
    }

    public void initView() {
        mLayoutProgress = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);
        mDrawerManager = new DrawerManager(mContext,mRootLayout);
        mDrawerView = (LinearLayout)findViewById(R.id.drawer_id);
        mDrawerView.setPadding(0, getStatusBarHeight(), 0, 0);
        // The number of Columns
        mRecyclerView = (RecyclerView)findViewById(R.id.category_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new HomeAdapter(mContext,mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void loadCategories() {
        if (!mConnectionUtil.isConnectedToInternet(this)) {
            mErrorDialog.showDialog(Const.ERROR_NO_CONNECTION);
        }else {
            mLayoutProgress.setVisibility(View.VISIBLE);
            mSharedPreferenceUtil.clearCategoryList();
            Map<String, String> queryParams = new HashMap<String, String>();
            queryParams.put("get_list", "1");
            queryParams.put("location_id", mSharedPreferenceUtil.getCurrentLocation().getmLocationId());
            mQueryHelper.getList(Const.CATEGORY_MODEL_LIST_TYPE, queryParams);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawerManager != null) {
            mDrawerManager.invalidateDrawerLayout();
        }
        List<Model> categoryModelList = mSharedPreferenceUtil.getCategoryModelList();
        if(categoryModelList == null || categoryModelList.size() == 0) {
            loadCategories();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (mExit) {
                finish(); // finish activity
            } else {
                Toast.makeText(this, "Press Back again to Exit.",
                        Toast.LENGTH_SHORT).show();
                mExit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mExit = false;
                    }
                }, 2 * 1000);

            }
        }
    }

    @Override
         public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            launchActivity(Const.SearchActivity);
        }
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void launchActivity(Class activityName){
        Intent intent = new Intent(mContext,activityName);
        intent.putExtra(Const.LaunchFrom, Const.HomeActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }

    private void launchActivity(Class activityName, String delivery_date, String delivery_time){
        Intent intent = new Intent(mContext,activityName);
        intent.putExtra(Const.LaunchFrom, Const.HomeActivity);
        intent.putExtra("delivery_date", delivery_date);
        intent.putExtra("delivery_time", delivery_time);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
       // SharedPreferenceUtil.getInstance(this).exit();

    }

    @Override
    public void copyModelList(List<Model> modelList) {

    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {
        mLayoutProgress.setVisibility(View.GONE);
        if (errorType == Const.NO_ERROR) {
            mAdapter.setDataList(mSharedPreferenceUtil.getCategoryModelList());
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        } else {
            mErrorDialog.showDialog(errorType);
        }
    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }

    @Override
    public void onRetrySelected() {
        mErrorDialog.dismissDialog();
        loadCategories();
    }
}
