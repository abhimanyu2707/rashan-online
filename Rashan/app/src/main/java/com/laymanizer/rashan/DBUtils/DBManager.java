package com.laymanizer.rashan.DBUtils;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Abhimanyu on 01-11-2015.
 */
public class DBManager {
    private static final String DATABASE_NAME = "dailyneeds.db";
    private static final int DATABASE_VERSION = 1;

    private final Context mContext;
    protected DatabaseHelper mDBHelper;
    protected SQLiteDatabase mDataBase;

    /**
     * Constructor
     * @param ctx
     */
    public DBManager(Context ctx)
    {
        this.mContext = ctx;
        this.mDBHelper = new DatabaseHelper(this.mContext);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(MyCartDBManager.CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion)
        {
            Log.w(DBManager.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + MyCartDBManager.TABLE);
            onCreate(db);
        }
    }
    /**
     * open the db
     * @return this
     * @throws SQLException
     * return type: DBAdapter
     */
    public DBManager open() throws SQLException
    {
        this.mDataBase = this.mDBHelper.getWritableDatabase();
        return this;
    }

    /**
     * close the db
     * return type: void
     */
    public void close()
    {
        this.mDBHelper.close();
    }

    public String getIds() {
        String ids = null;
        // Select All ids Query
        String selectQuery = "SELECT GROUP_CONCAT( `" + MyCartDBManager.COLUMN_ID + "` )  FROM " + MyCartDBManager.TABLE + " WHERE 1";
        open();
        Cursor cursor = mDataBase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            ids = cursor.getString(0);
        }
        close();
        return ids;
    }

}
