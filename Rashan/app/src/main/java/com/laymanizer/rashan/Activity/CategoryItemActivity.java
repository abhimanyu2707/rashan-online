package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.laymanizer.rashan.Adapter.SectionsPagerAdapter;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryItemActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback{

    Context mContext;
    View mRootLayout;
    Model mCurrentCategory;
    String mCurrentCategoryId;
    int mCurrentCategoryPosition;
    CartManager mCartManager;
    SharedPreferenceUtil mSharedPrefUtil;

   // ImageView mCategoryImage;
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    TextView mTotalCount;
    TextView mTotalPrice;
    LinearLayout mCartLayout;
    LinearLayout mLayoutProgress;
    QueryHelper mQueryHelper;
    int mCurrentSubCategoryPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mCurrentCategoryPosition = getIntent().getIntExtra("category", 0);
        mCartManager = CartManager.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mSharedPrefUtil = SharedPreferenceUtil.getInstance(mContext);
        mCurrentCategory = mSharedPrefUtil.getCategoryModelList().get(mCurrentCategoryPosition);
        mCurrentCategoryId = mCurrentCategory.getId();
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mCurrentCategory.getName());
    }

    public void initViews() {
        mRootLayout = LayoutInflater.from(mContext).inflate(R.layout.category_item_activity, null);
        mCartLayout = (LinearLayout) mRootLayout.findViewById(R.id.my_cart_checkout);
        mTotalCount = (TextView)mRootLayout.findViewById(R.id.total_count);
        mTotalPrice = (TextView)mRootLayout.findViewById(R.id.total_price);
        mViewPager = (ViewPager)mRootLayout.findViewById(R.id.pager);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),this,mCurrentCategoryPosition);

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //Log.e("keelback", "onPageSelected position="+position);
                mCurrentSubCategoryPosition = position;
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mCartLayout.setOnClickListener(mOnCartClickListener);
        mLayoutProgress = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);
        updateValues();
        if (mSharedPrefUtil.isNeedToLoadCategoryItems(mCurrentCategoryId)) {
            mLayoutProgress.setVisibility(View.VISIBLE);
            loadItems();
        }
    }

    public View getCartLayout() {
        return mCartLayout;
    }

    public TextView getTotalPriceTextView() {
        return mTotalPrice;
    }

    public TextView getTotalCountTextView() {
        return mTotalCount;
    }


    View.OnClickListener mOnCartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (mCartManager.getTotalCount() == 0) {
              Toast.makeText(mContext,"Please add items to cart",Toast.LENGTH_SHORT).show();
              return;
          }
          launchActivity(Const.CartActivity);
        }
    };

    public void updateValues() {
        //Log.e("keelback", "updateValues position="+mCurrentSubCategoryPosition);
        mViewPager.setCurrentItem(mCurrentSubCategoryPosition);
        mTotalPrice.setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getTotalPrice());
        mTotalCount.setText("" +  mCartManager.getTotalCount());
        if(mCartManager.getTotalCount() <= 0)
            mCartLayout.setVisibility(View.GONE);
        else
            mCartLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViewPager.setAdapter(mSectionsPagerAdapter);
        updateValues();
    }

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }

    private void launchActivity(Class activityName){
        Intent intent = new Intent(this,activityName);
        intent.putExtra(Const.LaunchFrom, activityName);
        startActivity(intent);
    }

    public void loadItems() {
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_list", "1");
        queryParams.put("location_id", mSharedPrefUtil.getCurrentLocation().getmLocationId());
        queryParams.put("category_id",mCurrentCategoryId);
        mQueryHelper.getList(Const.STORE_ITEM_MODEL_LIST_TYPE, queryParams);
    }

    @Override
    public void copyModelList(List<Model> modelList) {

    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {
        switch (errorType) {
            case Const.NO_ERROR:
                mSharedPrefUtil.setCategoryLoaded(mCurrentCategoryId);
                mViewPager.setAdapter(mSectionsPagerAdapter);
                mLayoutProgress.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        exitActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                exitActivity();
                break;
            case R.id.action_search:
                launchActivity(Const.SearchActivity);
                break;
        }
        return true;
    }

    private void exitActivity(){
        finish();
        overridePendingTransition(R.anim.left_side_in, R.anim.rignt_side_out);
    }
}
