package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.laymanizer.rashan.Activity.LocationEntryActivity;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhimanyu on 09-09-2015.
 */
public class CustomArrayAdapter extends BaseAdapter implements Filterable {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Model> mModelList;
    private List<Model> mFilteredModelList;
    RadioButton mRadioButton;
    private ValueFilter mValueFilter;


    public CustomArrayAdapter(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setDataList(List<Model> dataList) {
        mModelList = dataList;
        mFilteredModelList = dataList;
    }

    @Override
    public int getCount() {
        if (mFilteredModelList != null) {
            return mFilteredModelList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = (LinearLayout) mLayoutInflater.inflate(R.layout.dialog_row_item, null);

        }
        mRadioButton = (RadioButton) convertView.findViewById(R.id.dialog_radio_button);
        mRadioButton.setText(((Model) mFilteredModelList.get(position).getItem()).getName());
        return convertView;
    }


    @Override
    public Filter getFilter() {
        if (mValueFilter == null) {
            mValueFilter = new ValueFilter();
        }
        return mValueFilter;
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            Log.e("mannu", "filtering");
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<Model> filterList = new ArrayList<Model>();
                for (int i = 0; i < mModelList.size(); i++) {
                    if ((mModelList.get(i).getName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        filterList.add(mModelList.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mModelList.size();
                results.values = mModelList;
            }
            return results;
        }

        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mFilteredModelList = (ArrayList<Model>) results.values;
            if (mContext != null && mContext instanceof LocationEntryActivity) {
                ((LocationEntryActivity) mContext).updateModelList(mFilteredModelList);
            }
            notifyDataSetChanged();
        }
    }
}