package com.laymanizer.rashan.Address;

import com.laymanizer.rashan.Model.Model;

/**
 * Created by Abhimanyu on 19-09-2015.
 */
public class AddressModel implements Model {
    String mAddressId;
    String mAddressLabel;
    String mAddressReceiver;
    String mAddress1;
    String mAddress2;
    String mLocationId;
    String mLocationName;

    public String getAddressId() {
        return mAddressId;
    }

    public void setAddressId(String addressId) {
        this.mAddressId = addressId;
    }

    public String getAddressLabel() {
        return mAddressLabel;
    }

    public void setAddressLabel(String addressLabel) {
        this.mAddressLabel = addressLabel;
    }

    public String getAddressReceiver() {
        return mAddressReceiver;
    }

    public void setAddressReceiver(String mAddressReceiver) {
        this.mAddressReceiver = mAddressReceiver;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String address1) {
        this.mAddress1 = address1;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public void setAddress2(String address2) {
        this.mAddress2 = address2;
    }

    public String getLocationId() {
        return mLocationId;
    }

    public void setLocationId(String locationId) {
        this.mLocationId = locationId;
    }

    public String getLocationName() {
        return mLocationName;
    }

    public void setLocationName(String locationName) {
        this.mLocationName = locationName;
    }

    @Override
    public String getId() {
        return mAddressId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mAddressLabel;
    }
}
