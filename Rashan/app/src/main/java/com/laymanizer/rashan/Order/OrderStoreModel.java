package com.laymanizer.rashan.Order;

import com.laymanizer.rashan.Model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhimanyu on 10-01-2016.
 */
public class OrderStoreModel implements Model{

    String mStoreOrderNumber;
    String mStoreName;
    String mStoreOrderAmount;
    String mStoreOrderStatus;
    List<Model> mStoreItemList = new ArrayList<>();

    @Override
    public String getId() {
        return mStoreOrderNumber;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mStoreName;
    }

    public String getStoreOrderNumber() {
        return mStoreOrderNumber;
    }

    public void setStoreOrderNumber(String mStoreOrderId) {
        this.mStoreOrderNumber = mStoreOrderId;
    }

    public String getStoreName() {
        return mStoreName;
    }

    public void setStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }

    public String getStoreOrderAmount() {
        return mStoreOrderAmount;
    }

    public void setStoreOrderAmount(String mStoreOrderAmount) {
        this.mStoreOrderAmount = mStoreOrderAmount;
    }

    public String getStoreOrderStatus() {
        return mStoreOrderStatus;
    }

    public void setStoreOrderStatus(String mStoreOrderStatus) {
        this.mStoreOrderStatus = mStoreOrderStatus;
    }

    public List<Model> getStoreItemList() {
        return mStoreItemList;
    }

    public void setStoreItemList(List<Model> mStoreItemList) {
        this.mStoreItemList = mStoreItemList;
    }

    public void addItemInStore(Model item) {
        mStoreItemList.add(item);
    }
}
