package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.laymanizer.rashan.Adapter.OrderAdapter;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Order.OrderManager;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class OrderActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback{
    Context mContext;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    OrderAdapter mOrderAdapter;

    View mRootLayout;
    LinearLayout mLoadingLayout;
    LayoutInflater mInflater;

    List<Model> mOrderList;
    QueryHelper mQueryHelper;
    OrderManager mOrderManager;

    SharedPreferenceUtil mSharedPreferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initViews() {
        mOrderManager = OrderManager.getInstance(mContext);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mInflater = LayoutInflater.from(mContext);
        mRootLayout = (View)mInflater.inflate(R.layout.order_activity, null);

        mRecyclerView = (RecyclerView) mRootLayout.findViewById(R.id.item_list);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, null));
        mRecyclerView.setVisibility(View.GONE);

        mLoadingLayout = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);
        mLoadingLayout.setVisibility(View.VISIBLE);

        mOrderAdapter = new OrderAdapter(mContext);
        JSONObject queryParams = new JSONObject();
        try {
            queryParams.put("get_list", "1");
            queryParams.put("get", "1");
            queryParams.put("user_id", mSharedPreferenceUtil.getCurrentUser().getId());
            queryParams.put("user_password", mSharedPreferenceUtil.getCurrentUser().getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mQueryHelper.getList(Const.ORDER_MODEL_LIST_TYPE, queryParams);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mOrderList = mOrderManager.getOrderList();
        mOrderAdapter.setDataList(mOrderList);
        mRecyclerView.setAdapter(mOrderAdapter);
        mOrderAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }

    @Override
    public void copyModelList(List<Model> modelList) {
        mOrderManager.setOrderList(modelList);
        mOrderList = modelList;
        mLoadingLayout.setVisibility(View.GONE);
        mOrderAdapter.setDataList(mOrderList);
        mRecyclerView.setAdapter(mOrderAdapter);
        mOrderAdapter.notifyDataSetChanged();
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {

    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }

}
