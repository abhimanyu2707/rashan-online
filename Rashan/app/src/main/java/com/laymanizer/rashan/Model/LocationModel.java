package com.laymanizer.rashan.Model;

/**
 * Created by Abhimanyu on 09-09-2015.
 */
public class LocationModel implements Model{
    private String mLocationId;
    private String mCityId;
    private String mLocationName;
    private String mStoreIds;

    public String getmStoreIds() {
        return mStoreIds;
    }

    public void setmStoreIds(String mStoreIds) {
        this.mStoreIds = mStoreIds;
    }

    public String getmLocationId() {
        return mLocationId;
    }

    public void setmLocationId(String mLocationId) {
        this.mLocationId = mLocationId;
    }

    public String getmCityId() {
        return mCityId;
    }

    public void setmCityId(String mCityId) {
        this.mCityId = mCityId;
    }

    public String getmLocationName() {
        return mLocationName;
    }

    public void setmLocationName(String mLocationName) {
        this.mLocationName = mLocationName;
    }


    @Override
    public String getId() {
        return mLocationId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mLocationName;
    }
}
