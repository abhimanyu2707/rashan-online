package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laymanizer.rashan.Adapter.ItemVariantAdapter;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.List;

public class CartActivity extends AppCompatActivity {


    final String TAG = "CartActivity";
    SharedPreferenceUtil mSharedPreferenceUtil;
    ItemVariantAdapter mCartAdapter;

    View mRootLayout;
    LayoutInflater mInflater;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    LinearLayout mEmptyLayout;
    LinearLayout mShopNowButton;
    LinearLayout mCartLayout;
    TextView mDeliveryCharges;
    TextView mTotalPrice;

    Context mContext;
    CartManager mCartManager;
    List<Model> mCartList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initViews() {
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mInflater = LayoutInflater.from(mContext);
        mCartManager = CartManager.getInstance(mContext);
        mRootLayout = (View)mInflater.inflate(R.layout.cart_activity, null);
        mRecyclerView = (RecyclerView) mRootLayout.findViewById(R.id.item_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCartAdapter = new ItemVariantAdapter(mContext, mRecyclerView);
        mCartLayout = (LinearLayout)mRootLayout.findViewById(R.id.my_cart_checkout);
        mDeliveryCharges = (TextView)mRootLayout.findViewById(R.id.delivery_charges);
        mTotalPrice = (TextView)mRootLayout.findViewById(R.id.total_price);
        mCartLayout.setOnClickListener(mOnCheckOutClickListener);
        mEmptyLayout = (LinearLayout)mRootLayout.findViewById(R.id.empty_cart_layout);
        mShopNowButton = (LinearLayout)mRootLayout.findViewById(R.id.shop_now);
        mShopNowButton.setOnClickListener(mOnShopNowClickListener);

    }


    View.OnClickListener mOnShopNowClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            launchActivity(HomeActivity.class);
            finish();
        }
    };


    View.OnClickListener mOnCheckOutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!mSharedPreferenceUtil.isUserDetailsPresent()) {
                launchActivity(LoginActivity.class);
            }else {
                launchActivity(CheckoutActivity.class);
            }

        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        showCart();
    }

    public void showEmptyCart(boolean on) {
        if(on) {
            mDeliveryCharges.setVisibility(View.GONE);
            mCartLayout.setVisibility(View.GONE);
            mEmptyLayout.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.GONE);
            mDeliveryCharges.setVisibility(View.VISIBLE);
            mCartLayout.setVisibility(View.VISIBLE);
        }
    }

    public void showCart() {
        mCartList = mCartManager.getCartItemVariantList();
        if (mCartList == null || mCartList.isEmpty()) {
            showEmptyCart(true);
            return;
        } else {
            showEmptyCart(false);
            mCartAdapter.setDataList(mCartList);
            mRecyclerView.setAdapter(mCartAdapter);
            updateExtraInfo();
        }
    }

    public void updateExtraInfo() {
        mTotalPrice.setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getTotalPrice());
        if(mCartManager.getMinimumOrderForFree() <= mCartManager.getTotalPrice()) {
            mDeliveryCharges.setText("Free Delivery!");
            mDeliveryCharges.setBackgroundColor(mContext.getResources().getColor(R.color.cart_no_delivery_charge_background));
        } else {
            mDeliveryCharges.setText("Free Delivery on orders above "+mContext.getResources().getString(R.string.Rs)+mCartManager.getMinimumOrderForFree());
            mDeliveryCharges.setBackgroundColor(mContext.getResources().getColor(R.color.cart_delivery_charge_background));
        }
    }

    public void updateCart() {
        mCartList = mCartManager.getCartItemVariantList();
        //mCartAdapter.setDataList(mCartList);
        mCartAdapter.notifyDataSetChanged();
        updateExtraInfo();
        if (mCartList == null || mCartList.isEmpty()) {
            showEmptyCart(true);
        }
    }

    public TextView getTotalPriceTextView() {
        return mTotalPrice;
    }

    public void launchActivity(Class targetClass) {
        Intent i = new Intent(mContext, targetClass);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(i);
    }

    public View getEmptyLayout() {
        return mEmptyLayout;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                exitActivity();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        exitActivity();
    }


    private void exitActivity(){
        finish();
        //overridePendingTransition(R.anim.left_side_in, R.anim.rignt_side_out);
    }


}
