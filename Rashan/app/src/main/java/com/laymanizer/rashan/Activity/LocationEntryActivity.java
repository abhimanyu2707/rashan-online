package com.laymanizer.rashan.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.CityModel;
import com.laymanizer.rashan.Model.LocationModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;
import com.laymanizer.rashan.View.CustomDialog;
import com.laymanizer.rashan.View.CustomDialog.OnModelSelectedListener;

import java.util.List;

public class LocationEntryActivity extends Activity implements OnModelSelectedListener {

    LinearLayout mLinearLayoutCity;
    LinearLayout mLinearLayoutLocation;
    LinearLayout mShowGroceriesLayout;
    TextView mCityTextView;
    TextView mLocationTextView;
    Context mContext;
    SharedPreferenceUtil mSharedPreferenceUtil;
    Model mCurrentCityModel;
    Model mCurrentLocationModel;

    CustomDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.location_entry_activity);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(this);
        initialiseViews();
     }

    private void initialiseViews(){
        mLinearLayoutCity = (LinearLayout)findViewById(R.id.layout_city);
        mLinearLayoutLocation = (LinearLayout)findViewById(R.id.layout_location);
        mCityTextView = (TextView)findViewById(R.id.textViewCity);
        mLocationTextView = (TextView)findViewById(R.id.textViewLocation);
        mShowGroceriesLayout = (LinearLayout)findViewById(R.id.layout_show_groceries);
        mShowGroceriesLayout.setEnabled(false);
        mCurrentCityModel = mSharedPreferenceUtil.getCurrentCity();
        mCurrentLocationModel = mSharedPreferenceUtil.getCurrentLocation();
        if (mCurrentCityModel != null) {
            mCityTextView.setText(mCurrentCityModel.getName());
            if (mCurrentLocationModel != null) {
                mLocationTextView.setText(mCurrentLocationModel.getName());
                mShowGroceriesLayout.setEnabled(true);
            } else {
                mLocationTextView.setText("Select Location");
            }
            mLocationTextView.setTextColor(mContext.getResources().getColor(R.color.city_view_color));
        } else {
            mCityTextView.setText("Select City");
            mLocationTextView.setText("Select Location");
            mLinearLayoutLocation.setEnabled(false);
        }

        mLinearLayoutCity.setOnClickListener(mOnCityClickListener);
        mLinearLayoutLocation.setOnClickListener(mOnLocationClickListener);
        mShowGroceriesLayout.setOnClickListener(mOnShowGroceriesClickListener);
    }


    View.OnClickListener mOnCityClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           showCustomDialog(Const.CITY_MODEL_LIST_TYPE,null);
        }
    };


    View.OnClickListener mOnLocationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showCustomDialog(Const.LOCATION_MODEL_LIST_TYPE,mCurrentCityModel.getId());
        }
    };

    View.OnClickListener mOnShowGroceriesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mSharedPreferenceUtil.getCurrentCity() == null || mSharedPreferenceUtil.getCurrentLocation() == null
                    || !mCurrentCityModel.getId().equals(mSharedPreferenceUtil.getCurrentCity().getId())
                    || !mCurrentLocationModel.getId().equals(mSharedPreferenceUtil.getCurrentLocation().getId())) {
                mSharedPreferenceUtil.setCurrentCity(mCurrentCityModel);
                mSharedPreferenceUtil.setCurrentLocation(mCurrentLocationModel);

                //clear cart when location changes
                CartManager.getInstance(mContext).clearCart();
                launchActivity(Const.SplashActivity);
            } else {
                finish();
                //launchActivity(Const.HomeActivity);
            }
        }
    };

    public void showCustomDialog(int modelType,String id) {
        if (mDialog != null) {
            mDialog.dismissDialog();
            mDialog = null;
        }
        mDialog = new CustomDialog(mContext);
        mDialog.setOnModelSelectedListener(this);
        mDialog.setListType(modelType);
        mDialog.showDialog(id);
    }

    @Override
    public void onModelSelected(Model model) {
         if (model != null && model instanceof CityModel) {
             mCityTextView.setText(((CityModel) model).getName());
             mCurrentCityModel = model;
             mLinearLayoutLocation.setEnabled(true);
             mShowGroceriesLayout.setEnabled(false);
             mLocationTextView.setText("Select Location");
             mLocationTextView.setTextColor(mContext.getResources().getColor(R.color.city_view_color));
         } else if (model != null && model instanceof LocationModel) {
             mLocationTextView.setText(((LocationModel) model).getName());
             mCurrentLocationModel = model;
             mShowGroceriesLayout.setEnabled(true);
         }
        mDialog = null;
    }

    private void launchActivity(Class activityName){
        Intent intent = new Intent(this,activityName);
        intent.putExtra(Const.LaunchFrom, Const.LocationEntryActivity);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void updateModelList(List<Model> listModel) {
        if (mDialog != null) {
            mDialog.updateModelList(listModel);
        }
    }
}
