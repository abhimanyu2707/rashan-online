package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.laymanizer.rashan.Activity.CartActivity;
import com.laymanizer.rashan.Activity.ItemVariantActivity;
import com.laymanizer.rashan.Activity.SearchActivity;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.ItemVariantModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Volley.ImageRequestQueue;

import java.util.List;

/**
 * Created by Abhimanyu on 17-10-2015.
 */
public class ItemVariantAdapter extends RecyclerView.Adapter<ItemVariantAdapter.ViewHolder> {
    Context mContext;
    LayoutInflater mLayoutInflater;
    CartManager mCartManager;
    RecyclerView mRecyclerView;
    List<Model> mItemVariantList;

    public ItemVariantAdapter(Context context, RecyclerView recyclerView) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mRecyclerView = recyclerView;
        mCartManager = CartManager.getInstance(mContext);
    }

    public void setDataList(List<Model> list) {
        mItemVariantList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.common_card_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        viewHolder.mPlusButton.setOnClickListener(mOnPlusButtonClickListener);
        viewHolder.mMinusButton.setOnClickListener(mOnMinusButtonClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemVariantModel itemVariantModel = (ItemVariantModel)mItemVariantList.get(position);

        String itemImageUri = itemVariantModel.getItemImageUri();
        String itemName = itemVariantModel.getName();
        String regionalName = itemVariantModel.getRegionalName();
        if(regionalName != null && regionalName.length()>0)
            itemName += " (" + regionalName + ")";
        String itemQuantityPU = itemVariantModel.getQuantityPerUnit();
        String itemPrice = itemVariantModel.getPrice();
        String itemMRP = itemVariantModel.getMRP();
        String itemCount = ""+mCartManager.getCount(itemVariantModel);
        String storeName = itemVariantModel.getStoreName();

        if(mContext != null && (mContext instanceof CartActivity) &&
                (position == 0 || !((ItemVariantModel) mItemVariantList.get(position-1)).getStoreId().equals(itemVariantModel.getStoreId()))) {
            //Log.e("kd", "position "+ position);
            holder.mCartStoreName.setText(storeName);
            holder.mCartStoreAmount.setText(mContext.getResources().getString(R.string.Rs)+mCartManager.getStoreItemPrice(itemVariantModel.getStoreId()));
            holder.mCartStoreLayout.setVisibility(View.VISIBLE);
        } else {
            holder.mCartStoreName.setText("");
            holder.mCartStoreAmount.setText("");
            holder.mCartStoreLayout.setVisibility(View.GONE);
        }


        Log.v("abhi1", "itemName : "+itemName +" itemImageUri : "+itemImageUri);
        holder.mItemImage.setImageResource(R.drawable.default_image);
        ImageLoader imageLoader = ImageRequestQueue.getInstance(mContext).getImageLoader();
        if(!itemImageUri.equals(null) && itemImageUri.length()>0)
            holder.mItemImage.setImageUrl(itemImageUri, imageLoader);
        holder.mItemImage.setDefaultImageResId(R.drawable.default_image);
        holder.mItemImage.setErrorImageResId(R.drawable.default_image);
        holder.mItemName.setText(itemName);
        holder.mItemQuantityPU.setText(itemQuantityPU);
        holder.mMoreItemLayout.setVisibility(View.GONE);
        holder.mItemPrice.setText(mContext.getResources().getString(R.string.Rs)+itemPrice);
        if(!itemMRP.equals(itemPrice) && Float.parseFloat(itemMRP) != 0.0f) {
            holder.mItemMRP.setText(mContext.getResources().getString(R.string.Rs)+itemMRP);
            holder.mItemMRP.setVisibility(View.VISIBLE);
            holder.mItemMRP.setPaintFlags(holder.mItemMRP.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        holder.mItemCount.setText(itemCount);
        holder.mStoreName.setText(storeName);
        holder.mItemVariantModel = itemVariantModel;
    }

    @Override
    public int getItemCount() {
        if (mItemVariantList != null) {
            return mItemVariantList.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout mCartStoreLayout;
        TextView mCartStoreName;
        TextView mCartStoreAmount;
        NetworkImageView mItemImage;
        TextView mItemName;
        TextView mItemQuantityPU;
        LinearLayout mMoreItemLayout;
        TextView mItemPrice;
        TextView mItemMRP;
        TextView mMinusButton;
        TextView mItemCount;
        TextView mPlusButton;
        TextView mStoreName;

        ItemVariantModel mItemVariantModel;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemImage = (NetworkImageView)itemView.findViewById(R.id.item_image);
            mItemName = (TextView)itemView.findViewById(R.id.item_name);
            mItemQuantityPU = (TextView)itemView.findViewById(R.id.item_quantity_pu);
            mMoreItemLayout = (LinearLayout)itemView.findViewById(R.id.more_items_layout);
            mItemPrice = (TextView)itemView.findViewById(R.id.item_price);
            mItemMRP = (TextView)itemView.findViewById(R.id.item_mrp);
            mMinusButton = (TextView)itemView.findViewById(R.id.minus_button);
            mItemCount = (TextView)itemView.findViewById(R.id.item_count);
            mPlusButton = (TextView)itemView.findViewById(R.id.plus_button);
            mPlusButton.setTag(this);
            mMinusButton.setTag(this);
            mStoreName = (TextView)itemView.findViewById(R.id.store_name);
            mCartStoreLayout = (LinearLayout)itemView.findViewById(R.id.cart_store_layout);
            mCartStoreName = (TextView)itemView.findViewById(R.id.cart_store_name);
            mCartStoreAmount = (TextView)itemView.findViewById(R.id.cart_store_amount);
            if (mContext != null && (mContext instanceof CartActivity)) {
                mStoreName.setVisibility(View.GONE);
            }
        }
    }

    View.OnClickListener mOnMinusButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateCount(view,true);

        }
    };

    View.OnClickListener mOnPlusButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateCount(view, false);
        }
    };


    public void updateCount(View view, boolean isMinus) {
        ViewHolder viewHolder = (ViewHolder)view.getTag();
        Model variantModel = viewHolder.mItemVariantModel;
        if (isMinus) {
            mCartManager.removeFromCart(/*(ItemModel) mItemModel, */(ItemVariantModel) variantModel);
        } else {
            mCartManager.addToCart(/*(ItemModel) mItemModel, */(ItemVariantModel) variantModel);
        }
        viewHolder.mItemCount.setText(""+mCartManager.getCount((ItemVariantModel)variantModel));
        if (mContext != null && (mContext instanceof ItemVariantActivity )) {
            ((ItemVariantActivity)mContext).getTotalPriceTextView().setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getTotalPrice());
            ((ItemVariantActivity)mContext).getTotalCountTextView().setText("" + mCartManager.getTotalCount());
            if(mCartManager.getTotalCount() <= 0)
                ((ItemVariantActivity)mContext).getCartLayout().setVisibility(View.GONE);
            else
                ((ItemVariantActivity)mContext).getCartLayout().setVisibility(View.VISIBLE);
        }
        if (mContext != null && (mContext instanceof CartActivity)) {
            ((CartActivity)mContext).updateCart();
        }
    }
}
