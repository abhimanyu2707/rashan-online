package com.laymanizer.rashan.Adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.laymanizer.rashan.Fragments.ListItemFragment;
import com.laymanizer.rashan.Model.CategoryModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SectionsPagerAdapter extends FragmentPagerAdapter {

    Map<Integer,ListItemFragment> mListItemFragment = new HashMap<Integer,ListItemFragment>();
    Context mContext;
    int mCurrentCategory;
    SharedPreferenceUtil mSharedPrefUtil;
    ListItemFragment mCurrentFragment;


    public SectionsPagerAdapter(FragmentManager fm, Context cntxt,int currentCategory) {
        super(fm);
        mContext = cntxt;
        mCurrentCategory =currentCategory;
        mSharedPrefUtil = SharedPreferenceUtil.getInstance(mContext);
    }

    @Override
    public Fragment getItem(int position) {
        if (mListItemFragment.get(position) == null) {
            Model subModel = ((CategoryModel)mSharedPrefUtil.getCategoryModelList().get(mCurrentCategory)).getSubCategoryModelList().get(position);
            ListItemFragment fragment = new ListItemFragment();
            fragment.setArguments(mContext,subModel);
            mListItemFragment.put(position, fragment);
        }
        mCurrentFragment = mListItemFragment.get(position);
        return mCurrentFragment;
    }

    public void notifyFragment(){
        mCurrentFragment.notifyAdapter();
    }

    @Override
    public int getCount() {
        List list =mSharedPrefUtil.getCategoryModelList();
        if (list != null && !list.isEmpty() && list.get(mCurrentCategory) != null )
            return ((CategoryModel)list.get(mCurrentCategory)).getCategoryModelListSize();
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ((CategoryModel)mSharedPrefUtil.getCategoryModelList().get(mCurrentCategory)).getSubCategoryModelList().get(position).getName();
    }

   }
