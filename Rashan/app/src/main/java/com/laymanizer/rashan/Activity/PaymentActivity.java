package com.laymanizer.rashan.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laymanizer.rashan.Address.AddressManager;
import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Cart.CartStoreModel;
import com.laymanizer.rashan.DateTime.DateTimeManager;
import com.laymanizer.rashan.Model.ItemVariantModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class PaymentActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback{
    View mRootLayout;
    LayoutInflater mLayoutInflater;
    TextView mAmountPayable;
    LinearLayout mCOD;
    LinearLayout mLayoutProgress;

    Context mContext;
    CartManager mCartManager;
    AddressManager mAddressManager;
    DateTimeManager mDateTimeManager;
    QueryHelper mQueryHelper;
    SharedPreferenceUtil mSharedPreferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
        }
        return true;
    }
    public void initViews() {
        mLayoutInflater = LayoutInflater.from(mContext);
        mRootLayout = mLayoutInflater.inflate(R.layout.payment_activity, null);
        mCartManager = CartManager.getInstance(mContext);
        mAddressManager = AddressManager.getInstance(mContext);
        mDateTimeManager = DateTimeManager.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);/////
        mAmountPayable = (TextView)mRootLayout.findViewById(R.id.payable_amount);
        mAmountPayable.setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getPayableAmount());
        mCOD = (LinearLayout)mRootLayout.findViewById(R.id.layout_cod);
        mCOD.setOnClickListener(mOnCODClickListener);
        mLayoutProgress = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);
    }

    private void placeOrder() throws JSONException {
        //Map<String, String> queryParams= new HashMap<String, String>();
        JSONObject queryParams = new JSONObject();
        AddressModel addressModel = (AddressModel)mAddressManager.getDeliveryAddress();
        queryParams.put("get_list", "1");
        queryParams.put("add", "1");
        queryParams.put("user_id", mSharedPreferenceUtil.getCurrentUser().getId());
        queryParams.put("user_password", mSharedPreferenceUtil.getCurrentUser().getPassword());

        JSONObject addressJSONObject = new JSONObject();
        addressJSONObject.put("label", addressModel.getAddressLabel());
        addressJSONObject.put("receiver", addressModel.getAddressReceiver());
        addressJSONObject.put("address1", addressModel.getAddress1());
        addressJSONObject.put("address2", addressModel.getAddress2());
        addressJSONObject.put("location_id", addressModel.getLocationId());
        addressJSONObject.put("location_name", addressModel.getLocationName());
        queryParams.put("address", addressJSONObject);

        JSONArray storeJSONArray = new JSONArray();
        List<Model> cartStoreList = mCartManager.getCartStoreList();
        for(Model cartStoreModel : cartStoreList) {
            JSONObject cartStoreObject = new JSONObject();
            cartStoreObject.put("store_id", cartStoreModel.getId());
            cartStoreObject.put("store_name", cartStoreModel.getName());
            cartStoreObject.put("order_amount", ""+(mCartManager.getStoreItemPrice(((CartStoreModel)cartStoreModel).getStoreId())));
            JSONArray storeItemsArray = new JSONArray();
            List<Model>storeItemsList = ((CartStoreModel)cartStoreModel).getStoreItems();
            for (Model storeItem : storeItemsList) {
                JSONObject storeItemObject = new JSONObject();
                try {
                    storeItemObject.put("unit_id", storeItem.getId());
                    storeItemObject.put("count", ""+mCartManager.getCount((ItemVariantModel)storeItem));
                    storeItemsArray.put(storeItemObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            cartStoreObject.put("items", storeItemsArray);
            storeJSONArray.put(cartStoreObject);
        }
        queryParams.put("store_items", storeJSONArray);
        float totalAmount = mCartManager.getTotalPrice();
        float deliveryCharges = 0;
        float payableAmount = 0;
        queryParams.put("sub_total", ""+totalAmount);
        if(mCartManager.getMinimumOrderForFree() < totalAmount) {
            deliveryCharges = 0;
        }else {
            deliveryCharges = mCartManager.getDeliveryCharges();
        }
        queryParams.put("delivery_charges", deliveryCharges);
        payableAmount = totalAmount + deliveryCharges;
        queryParams.put("payable_amount", "" + payableAmount);
        queryParams.put("delivery_date", mDateTimeManager.getDeliveryDate().getDate());
        queryParams.put("delivery_time", mDateTimeManager.getDeliveryTime().getName());
        Log.e("query", queryParams.toString());
        mLayoutProgress.setVisibility(View.VISIBLE);
        mQueryHelper.getList(Const.ORDER_ADD_MODEL_TYPE, queryParams);
    }

    View.OnClickListener mOnCODClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog dialog;
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            dialogBuilder.setTitle("Place Order");
            dialogBuilder.setMessage("Proceed with cash on delivery?");
            dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        placeOrder();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            dialog = dialogBuilder.create();
            dialog.show();
        }
    };

    private void handleOrderComplete(int errorType, HashMap<String, String> args) {
        if(errorType == Const.NO_ERROR) {
            mLayoutProgress.setVisibility(View.GONE);
            mCartManager.clearCart();
            launchActivity(Const.HomeActivity, args);
        }
    }


    private void launchActivity(Class targetActivity, HashMap<String, String> args){
        Intent intent = new Intent(mContext,targetActivity);
        intent.putExtra(Const.LaunchFrom, Const.PaymentActivityName);
        intent.putExtra("delivery_date", args.get("delivery_date"));
        intent.putExtra("delivery_time", args.get("delivery_time"));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
        finish();
    }

    @Override
    public void copyModelList(List<Model> modelList) {
    }

    @Override
    public void copyModel(Model model) {
    }

    @Override
    public void onQueryCompleted(int errorType) {
    }

    @Override
    public void onQueryCompleted(int errorType, HashMap args) {
        handleOrderComplete(errorType, args);
    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {
    }
}
