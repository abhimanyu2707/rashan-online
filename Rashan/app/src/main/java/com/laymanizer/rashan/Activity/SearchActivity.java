package com.laymanizer.rashan.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.laymanizer.rashan.Adapter.ItemAdapter;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.QueryHelper.*;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends Activity implements IQueryHelperCallback {


    final String TAG = "SearchActivity";
    SharedPreferenceUtil mSharedPreferenceUtil;
    EditText mEditText;
    ImageView mBackButton;
    ImageView mCancelButton;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    LinearLayout mLoadingLayout;
    ItemAdapter mItemAdapter;
    LinearLayout mCartLayout;
    TextView mTotalCount;
    TextView mTotalPrice;

    View mRootLayout;
    LayoutInflater mInflater;
    Context mContext;
    String mSearchKey;
    QueryHelper mQueryHelper;
    CartManager mCartManager;

    List<Model> mStoreItemList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);

    }

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }

    public void initViews() {
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mInflater = LayoutInflater.from(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mCartManager = CartManager.getInstance(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mRootLayout = (View)mInflater.inflate(R.layout.search_activity, null);
        mRecyclerView = (RecyclerView) mRootLayout.findViewById(R.id.search_list);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mItemAdapter = new ItemAdapter(mContext,mRecyclerView);
        mRecyclerView.setAdapter(mItemAdapter);

        mEditText = (EditText)mRootLayout.findViewById(R.id.search_textbox);
        mCancelButton = (ImageView)mRootLayout.findViewById(R.id.search_clear_text);
        mBackButton = (ImageView)mRootLayout.findViewById(R.id.search_back_button);
        mLoadingLayout = (LinearLayout)mRootLayout.findViewById(R.id.loading_layout);
        mCartLayout = (LinearLayout)mRootLayout.findViewById(R.id.my_cart);
        mTotalCount = (TextView)mRootLayout.findViewById(R.id.total_count);
        mTotalPrice = (TextView)mRootLayout.findViewById(R.id.total_price);
        //mLoadingLayout.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_list", "1");
        mQueryHelper.getList(Const.SEARCH_ITEM_MODEL_LIST_TYPE, queryParams);
        mEditText.addTextChangedListener(mTextWatcher);
        mBackButton.setOnClickListener(mOnBackButtonClickListener);
        mCancelButton.setOnClickListener(mOnClearTextClickListener);
        mCartLayout.setOnClickListener(mOnCartClickListener);
    }


    TextWatcher mTextWatcher = 	new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mSearchKey = mEditText.getText().toString();

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(mSearchKey!=null && mSearchKey.length()>0) {
                mLoadingLayout.setVisibility(View.VISIBLE);
                Map<String, String> queryParams = new HashMap<String, String>();
                queryParams.put("get_list", "1");
                queryParams.put("location_id", mSharedPreferenceUtil.getCurrentLocation().getmLocationId());
                queryParams.put("item_name", mSearchKey);
                mQueryHelper.getList(Const.SEARCH_ITEM_MODEL_LIST_TYPE, queryParams);
            }
        }
    };


    View.OnClickListener mOnCartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mCartManager.getTotalCount() == 0) {
                Toast.makeText(mContext, "Please add items to cart", Toast.LENGTH_SHORT).show();
                return;
            }
            launchActivity(Const.CartActivity);
        }
    };

    View.OnClickListener mOnBackButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();

        }
    };

    private void launchActivity(Class activityName){
        Intent intent = new Intent(this,activityName);
        intent.putExtra(Const.LaunchFrom, activityName);
        startActivity(intent);
    }


    View.OnClickListener mOnClearTextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mEditText.setText("");
            mSearchKey= "";
        }
    };

    @Override
    public void copyModelList(List<Model> modelList) {
        //Log.e("Mannu", "searchActivity callback"+modelList.size());
        mStoreItemList = modelList;
        mLoadingLayout.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mItemAdapter.setDataList(mStoreItemList);
        mRecyclerView.setAdapter(mItemAdapter);
        mItemAdapter.notifyDataSetChanged();
        mTotalCount.setText("" +  mCartManager.getTotalCount());
        mTotalPrice.setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getTotalPrice());
        if(mCartManager.getTotalCount() <= 0)
            mCartLayout.setVisibility(View.GONE);
        else
            mCartLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {

    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public View getCartLayout() {
        return mCartLayout;
    }

    public TextView getTotalPriceTextView() {
        return mTotalPrice;
    }

    public TextView getTotalCountTextView() {
        return mTotalCount;
    }
}
