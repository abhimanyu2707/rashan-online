package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Order.OrderManager;
import com.laymanizer.rashan.Order.OrderStoreModel;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;

import java.util.List;

/**
 * Created by Abhimanyu on 23-01-2016.
 */
public class OrderChildAdapter extends RecyclerView.Adapter<OrderChildAdapter.ViewHolder> {
    Context mContext;
    Model mOrderModel;
    List<Model> mOrderChildList;
    OrderManager mOrderManager;

    public OrderChildAdapter(Context context) {
        mContext = context;
        mOrderManager = OrderManager.getInstance(mContext);
    }

    public void setOrder(Model orderModel) {
        mOrderModel = orderModel;
    }

    public void setDataList(List<Model> list) {
        mOrderChildList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_child_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        itemView.setOnClickListener(mOnOrderClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderStoreModel orderStoreModel = (OrderStoreModel) mOrderChildList.get(position);
        holder.storeName.setText(orderStoreModel.getStoreName());
        holder.storeAmount.setText(mContext.getResources().getString(R.string.Rs) + orderStoreModel.getStoreOrderAmount());
        holder.storeOrderId.setText("Order id: "+orderStoreModel.getId());
        String orderStatus = orderStoreModel.getStoreOrderStatus();
        if(orderStatus.equals("0")){
            holder.orderStatus.setText("Order Processing");
            holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.order_processing));
            holder.orderStatusImage.setBackgroundResource(R.drawable.ic_action_process_start);
        }else if(orderStatus.equals("-1")){
            holder.orderStatus.setText("Order Canceled");
            holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.order_canceled));
            holder.orderStatusImage.setBackgroundResource(R.drawable.ic_action_cancel);
        }else if(orderStatus.equals("1")){
            holder.orderStatus.setText("Order Delivered");
            holder.orderStatus.setTextColor(mContext.getResources().getColor(R.color.order_delivered));
            holder.orderStatusImage.setBackgroundResource(R.drawable.ic_action_tick);
        }
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        if(mOrderChildList!=null)
            return mOrderChildList.size();
        return 0;
    }


    View.OnClickListener mOnOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mOrderManager.setDetailedOrder(mOrderModel);
            launchActivity(Const.OrderDetailActivity);
        }
    };

    public void launchActivity(Class targetClass) {
        Intent i = new Intent(mContext, targetClass);
        mContext.startActivity(i);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private View itemView;
        private TextView storeName;
        private TextView storeAmount;
        private TextView storeOrderId;
        private ImageView orderStatusImage;
        private TextView orderStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView    = itemView;
            storeName        = (TextView)itemView.findViewById(R.id.text_store_name);
            storeAmount      = (TextView)itemView.findViewById(R.id.text_store_amount);
            storeOrderId     = (TextView)itemView.findViewById(R.id.store_order_id);
            orderStatusImage = (ImageView)itemView.findViewById(R.id.image_order_status);
            orderStatus      = (TextView)itemView.findViewById(R.id.text_order_status);
        }
    }
}
