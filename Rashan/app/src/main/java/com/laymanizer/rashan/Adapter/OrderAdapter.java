package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.laymanizer.rashan.LayoutManager.CustomLinearLayoutManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Order.OrderManager;
import com.laymanizer.rashan.Order.OrderModel;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;

import java.util.List;

/**
 * Created by Abhimanyu on 17-01-2016.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    Context mContext;
    List<Model> mOrderList;
    OrderManager mOrderManager;

    public OrderAdapter(Context context) {
        mContext = context;
        mOrderManager = OrderManager.getInstance(mContext);
    }


    public void setDataList(List<Model> list) {
        mOrderList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        itemView.setOnClickListener(mOnOrderClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderModel orderModel = (OrderModel) mOrderList.get(position);
        holder.orderDate.setText(orderModel.getOrderDate());
        holder.orderDeliveryDateTime.setText("Scheduled for "+orderModel.getDeliveryDateTime());

        OrderChildAdapter orderChildAdapter = new OrderChildAdapter(mContext);
        orderChildAdapter.setOrder(orderModel);
        orderChildAdapter.setDataList(orderModel.getStoreOrderList());
        holder.orderChildRecyclerView.setAdapter(orderChildAdapter);
        orderChildAdapter.notifyDataSetChanged();

        holder.orderSubTotal.setText(mContext.getResources().getString(R.string.Rs) + orderModel.getSubTotal());
        holder.orderDeliveryCharges.setText(mContext.getResources().getString(R.string.Rs) + orderModel.getDeliveryCharges());
        holder.orderTotalAmount.setText(mContext.getResources().getString(R.string.Rs) + orderModel.getPayableAmount());
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        if(mOrderList!=null)
            return mOrderList.size();
        return 0;
    }


    View.OnClickListener mOnOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mOrderManager.setDetailedOrder(mOrderList.get((int)v.getTag()));
            launchActivity(Const.OrderDetailActivity);
        }
    };

    public void launchActivity(Class targetClass) {
        Intent i = new Intent(mContext, targetClass);
        mContext.startActivity(i);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private View itemView;
        private TextView orderDate;
        private TextView orderDeliveryDateTime;
        private TextView orderSubTotal;
        private TextView orderDeliveryCharges;
        private TextView orderTotalAmount;
        private RecyclerView orderChildRecyclerView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            orderDate = (TextView)itemView.findViewById(R.id.text_order_datetime);
            orderDeliveryDateTime = (TextView)itemView.findViewById(R.id.text_order_delivery_datetime);
            orderSubTotal     = (TextView)itemView.findViewById(R.id.text_order_sub_total);
            orderDeliveryCharges     = (TextView)itemView.findViewById(R.id.text_order_delivery_charges);
            orderTotalAmount = (TextView)itemView.findViewById(R.id.text_order_total_amount);
            orderChildRecyclerView = (RecyclerView)itemView.findViewById(R.id.list_store_orders);
            LinearLayoutManager layoutManager = new CustomLinearLayoutManager(mContext);/*, LinearLayoutManager.VERTICAL, false);*/
            orderChildRecyclerView.setHasFixedSize(true);
            orderChildRecyclerView.setLayoutManager(layoutManager);
            orderChildRecyclerView.setNestedScrollingEnabled(false);
        }
    }
}
