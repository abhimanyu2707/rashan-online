package com.laymanizer.rashan.View;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;

/**
 * Created by Abhimanyu on 14-10-2015.
 */
public class ErrorDialog {

    private final String TAG = "ErrorDialog";
    private Context mContext;
    AlertDialog mErrorDialog;
    AlertDialog.Builder mDialogBuilder;
    IErrorDialogListener mOnRetrySelectedListener;

    public ErrorDialog(Context context){
        mContext = context;
    }

    private void createDialog() {
        mDialogBuilder = new AlertDialog.Builder(mContext);
        mDialogBuilder.setNegativeButton("SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mContext.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
            }
        });
        mDialogBuilder.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    if (mOnRetrySelectedListener != null) {
                        mOnRetrySelectedListener.onRetrySelected();
                    }
                }
            }
        });
    }

    public void showDialog(int errorType) {
        Log.i(TAG, "showDialog");
        if (mErrorDialog == null) {
            createDialog();
        }
        mErrorDialog = mDialogBuilder.create();
        mErrorDialog.setTitle(getErrorString(errorType, true));
        mErrorDialog.setMessage(getErrorString(errorType, false));
        mErrorDialog.show();
    }

    public void dismissDialog() {
        Log.i(TAG, "dismissDialog");
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.hide();
        }
    }

    private String getErrorString(int errorType, Boolean isTitle){
        int resourceId;
        switch (errorType) {
            case Const.ERROR_NO_CONNECTION:
                resourceId = isTitle ? R.string.DIALOG_TITLE_NO_CONNECTION : R.string.DIALOG_MESSAGE_NO_CONNECTION;
                break;
            case Const.ERROR_TIME_OUT:
                resourceId = isTitle ? R.string.DIALOG_TITLE_TIME_OUT : R.string.DIALOG_MESSAGE_TIME_OUT;
                break;
            default:
                resourceId = isTitle ? R.string.DIALOG_TITLE_NO_CONNECTION : R.string.DIALOG_MESSAGE_NO_CONNECTION;
        }
        return mContext.getResources().getString(resourceId);
    }

    public void setOnRetrySelectedListener(IErrorDialogListener listener) {
        mOnRetrySelectedListener = listener;
    }

    public interface IErrorDialogListener {
        void onRetrySelected();
    }
}
