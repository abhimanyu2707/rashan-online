package com.laymanizer.rashan.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Laymanizer Pvt Ltd on 18-08-2015.
 */
public class ConnectionUtil {
    private static ConnectionUtil mConnectionUtil = null;
    private ConnectionUtil(){
    }

    public static ConnectionUtil getInstance(){
        if(mConnectionUtil==null)
            mConnectionUtil = new ConnectionUtil();
        return mConnectionUtil;
    }
    /**
     * Checking for all possible internet providers
     * **/
    public boolean isConnectedToInternet(Context context){
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }
}
