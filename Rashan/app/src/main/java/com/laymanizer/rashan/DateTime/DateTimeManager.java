package com.laymanizer.rashan.DateTime;

import android.content.Context;

/**
 * Created by Abhimanyu on 13-12-2015.
 */
public class DateTimeManager {
    public static DateTimeManager mDateTimeManager = null;
    DateTimeModel mDeliveryDate;
    TimeModel mDeliveryTime;
    private static Context mContext;

    private DateTimeManager() {}

    public static DateTimeManager getInstance(Context context) {
        if(mDateTimeManager == null) {
            mContext = context;
            mDateTimeManager = new DateTimeManager();
        }
        return mDateTimeManager;
    }

    public DateTimeModel getDeliveryDate() {
        return mDeliveryDate;
    }

    public void setDeliveryDate(DateTimeModel mDeliveryDate) {
        this.mDeliveryDate = mDeliveryDate;
    }

    public TimeModel getDeliveryTime() {
        return mDeliveryTime;
    }

    public void setDeliveryTime(TimeModel mDeliveryTime) {
        this.mDeliveryTime = mDeliveryTime;
    }
}
