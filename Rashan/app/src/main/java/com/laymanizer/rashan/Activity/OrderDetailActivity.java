package com.laymanizer.rashan.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laymanizer.rashan.Adapter.OrderDetailAdapter;
import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.LayoutManager.CustomLinearLayoutManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Order.OrderManager;
import com.laymanizer.rashan.Order.OrderModel;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class OrderDetailActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback {
    Context mContext;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    OrderDetailAdapter mOrderDetailAdapter;

    View mRootLayout;
    TextView mOrderDeliveryDateTime;
    TextView mOrderSubTotal;
    TextView mOrderDeliveryCharges;
    TextView mOrderPayableAmount;
    TextView mOrderAddressReceiver;
    TextView mOrderAddress1;
    TextView mOrderAddress2;
    TextView mOrderAddressLocation;
    LinearLayout mLoadingLayout;
    LayoutInflater mInflater;

    OrderModel mOrderModel;
    List<Model> mOrderDetailList;
    AddressModel mOrderAddressModel;
    QueryHelper mQueryHelper;
    OrderManager mOrderManager;

    SharedPreferenceUtil mSharedPreferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        fillView();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public void initViews() {
        mOrderManager = OrderManager.getInstance(mContext);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mOrderModel = (OrderModel) mOrderManager.getDetailedOrder();
        mOrderDetailList = mOrderModel.getStoreOrderList();
        mOrderAddressModel = mOrderModel.getDeliveryAddress();

        mInflater = LayoutInflater.from(mContext);
        mRootLayout = mInflater.inflate(R.layout.order_detail_activity, null);

        mOrderDeliveryDateTime = (TextView) mRootLayout.findViewById(R.id.text_order_detail_delivery_datetime);
        mOrderSubTotal = (TextView) mRootLayout.findViewById(R.id.sub_total);
        mOrderDeliveryCharges = (TextView) mRootLayout.findViewById(R.id.delivery_charges);
        mOrderPayableAmount = (TextView) mRootLayout.findViewById(R.id.amount_payable);
        mOrderAddressReceiver = (TextView) mRootLayout.findViewById(R.id.order_detail_address_receiver);
        mOrderAddress1 = (TextView) mRootLayout.findViewById(R.id.order_detail_address1);
        mOrderAddress2 = (TextView) mRootLayout.findViewById(R.id.order_detail_address2);
        mOrderAddressLocation = (TextView) mRootLayout.findViewById(R.id.order_detail_address_location);

        mRecyclerView = (RecyclerView) mRootLayout.findViewById(R.id.item_list);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new CustomLinearLayoutManager(this);//CustomLinearLayoutManagerOrder(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mLoadingLayout = (LinearLayout) mRootLayout.findViewById(R.id.layout_progress);
        mLoadingLayout.setVisibility(View.GONE);
        mOrderDetailAdapter = new OrderDetailAdapter(mContext);
    }

    private void fillView() {
        mOrderDeliveryDateTime.setText("Scheduled for " + mOrderModel.getDeliveryDateTime());
        mOrderSubTotal.setText(mContext.getResources().getString(R.string.Rs) + mOrderModel.getSubTotal());
        mOrderDeliveryCharges.setText(mContext.getResources().getString(R.string.Rs) + mOrderModel.getDeliveryCharges());
        mOrderPayableAmount.setText(mContext.getResources().getString(R.string.Rs) + mOrderModel.getPayableAmount());
        mOrderAddressReceiver.setText(mOrderAddressModel.getAddressReceiver());
        mOrderAddress1.setText(mOrderAddressModel.getAddress1());
        mOrderAddress2.setText(mOrderAddressModel.getAddress2());
        mOrderAddressLocation.setText(mOrderAddressModel.getLocationName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mOrderDetailAdapter.setDataList(mOrderDetailList);
        mRecyclerView.setAdapter(mOrderDetailAdapter);
        mOrderDetailAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mOrderModel.getOrderStatus().equals("0")) {
            getMenuInflater().inflate(R.menu.order_detail_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                exitActivity();
                break;
            case R.id.action_cancel_order:
                onActionCancelOrderClickListener();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        exitActivity();
    }

    private void exitActivity() {
        finish();
    }

    void onActionCancelOrderClickListener() {
        AlertDialog dialog;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle("Cancel Order?");
        dialogBuilder.setMessage("Are you sure you want to cancel this order?");
        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                JSONObject queryParams = new JSONObject();
                try {
                    queryParams.put("get_list", "1");
                    queryParams.put("cancel_order", "1");
                    queryParams.put("user_id", mSharedPreferenceUtil.getCurrentUser().getId());
                    queryParams.put("user_password", mSharedPreferenceUtil.getCurrentUser().getPassword());
                    queryParams.put("order_id", mOrderModel.getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoadingLayout.setVisibility(View.VISIBLE);
                mQueryHelper.getList(Const.ORDER_MODEL_LIST_TYPE, queryParams);
            }
        });
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        dialog = dialogBuilder.create();
        dialog.show();
    }

    @Override
    public void copyModelList(List<Model> modelList) {
        mOrderManager.setOrderList(modelList);
        mLoadingLayout.setVisibility(View.GONE);
        exitActivity();
    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {

    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }
}
