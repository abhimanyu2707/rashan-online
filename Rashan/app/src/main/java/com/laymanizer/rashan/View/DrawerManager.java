package com.laymanizer.rashan.View;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.io.File;
import java.util.ArrayList;

public class DrawerManager implements View.OnClickListener{
	
	DrawerLayout mDrawerLayout;
	Context mContext;
	View mRoot;
    SharedPreferenceUtil mSharedPreferenceUtil;
	CartManager mCartManager;

	ImageView mDrawerHeaderImage;
	TextView mDrawerHeaderText;

    RelativeLayout mLocationLayout;
	TextView mLocationText;
	RelativeLayout mLoginLayout;
	RelativeLayout mAddressLayout;
	TextView mAddressText;
	RelativeLayout mOrdersLayout;
	TextView mOrdersText;
	RelativeLayout mMyCartLayout;
	LinearLayout mMyCartCircleLayout;
	TextView mMycartCountText;
	RelativeLayout mNotificationCenter;

	RelativeLayout mHelpLayout;
	RelativeLayout mCallUsLayout;
	RelativeLayout mRateUsLayout;
	RelativeLayout mShareLayout;
	RelativeLayout mAboutUsLayout;
	RelativeLayout mLogOutLayout;


	public DrawerManager(Context context, View v) {
          mContext = context;
          mRoot = v;
          mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(context);
		  mCartManager = CartManager.getInstance(mContext);
		  mDrawerLayout = (DrawerLayout)mRoot.findViewById(R.id.drawer_layout);
		  initDrawerView();
	}

	public void initDrawerView() {
		mDrawerHeaderImage = (ImageView)mRoot.findViewById(R.id.drawer_header_image);
		mDrawerHeaderText = (TextView)mRoot.findViewById(R.id.drawer_header_text);
		mLocationLayout = (RelativeLayout)mRoot.findViewById(R.id.location_item);
		mLocationText = (TextView)mRoot.findViewById(R.id.location_text);
		mLocationLayout.setOnClickListener(this);
		mLoginLayout = (RelativeLayout)mRoot.findViewById(R.id.login_item);
		mLoginLayout.setOnClickListener(this);
		mAddressLayout = (RelativeLayout)mRoot.findViewById(R.id.my_address_item);
		mAddressText =(TextView)mRoot.findViewById(R.id.my_address_text);
		mAddressLayout.setOnClickListener(this);
		mOrdersLayout = (RelativeLayout)mRoot.findViewById(R.id.my_order_item);
		mOrdersText = (TextView)mRoot.findViewById(R.id.my_orders_text);
		mOrdersLayout.setOnClickListener(this);
		mMyCartLayout = (RelativeLayout)mRoot.findViewById(R.id.my_cart_item);
		mMyCartCircleLayout = (LinearLayout)mRoot.findViewById(R.id.my_cart_circle_layout);
		mMycartCountText = (TextView)mRoot.findViewById(R.id.my_cart_count_text);
		mMyCartLayout.setOnClickListener(this);
		mNotificationCenter = (RelativeLayout)mRoot.findViewById(R.id.notification_center);
		mNotificationCenter.setOnClickListener(this);

		mHelpLayout = (RelativeLayout)mRoot.findViewById(R.id.help_item);
		mHelpLayout.setOnClickListener(this);
		mCallUsLayout = (RelativeLayout)mRoot.findViewById(R.id.call_item);
		mCallUsLayout.setOnClickListener(this);
		mRateUsLayout = (RelativeLayout)mRoot.findViewById(R.id.rate_item);
		mRateUsLayout.setOnClickListener(this);
		mShareLayout = (RelativeLayout)mRoot.findViewById(R.id.share_item);
		mShareLayout.setOnClickListener(this);
		mAboutUsLayout = (RelativeLayout)mRoot.findViewById(R.id.about_item);
		mAboutUsLayout.setOnClickListener(this);
		mLogOutLayout = (RelativeLayout)mRoot.findViewById(R.id.logout_item);
		mLogOutLayout.setOnClickListener(this);
	}

	public void invalidateDrawerLayout() {
		Log.e("abhi", "invalidate");
        mLocationText.setText(mSharedPreferenceUtil.getCurrentLocationCityText());
		if (!mSharedPreferenceUtil.isUserDetailsPresent()) {
			mDrawerHeaderImage.setVisibility(View.GONE);
			mDrawerHeaderText.setText("WELCOME");
			mLoginLayout.setVisibility(View.VISIBLE);
			mLogOutLayout.setVisibility(View.GONE);
			mAddressLayout.setAlpha((float) 0.5);
			mOrdersLayout.setAlpha((float) 0.5);
		} else {
			mDrawerHeaderImage.setVisibility(View.VISIBLE);
			mDrawerHeaderText.setText(mSharedPreferenceUtil.getUserMobileNubmer());
			mAddressLayout.setAlpha(1);
			mOrdersLayout.setAlpha(1);
			mLogOutLayout.setVisibility(View.VISIBLE);
			mLoginLayout.setVisibility(View.GONE);
		}
		if (mCartManager.getTotalCount() == 0) {
			mMyCartCircleLayout.setVisibility(View.GONE);
		} else {
			mMyCartCircleLayout.setVisibility(View.VISIBLE);
			mMycartCountText.setText(""+mCartManager.getTotalCount());
		}

	}


	public void launchActivity(Class targetClass) {
		Bundle args = new Bundle();
		Intent i = new Intent(mContext, targetClass);
		mContext.startActivity(i);
	}

	public void open() {
		if (mDrawerLayout != null)
		    mDrawerLayout.openDrawer(GravityCompat.START);
	}
	
	public void close() {
		if (mDrawerLayout != null && isOpen())
			mDrawerLayout.closeDrawer(GravityCompat.START);
	}
	
	public boolean isOpen() {
		return (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START));
	}

	@Override
	public void onClick(View v) {
        close();
		int resId = v.getId();
		switch (resId) {
			case R.id.location_item :
				launchActivity(Const.LocationEntryActivity);
				break;
			case R.id.login_item :
				launchActivity(Const.LoginActivity);
                break;
			case R.id.my_address_item :
				if(!mSharedPreferenceUtil.isUserDetailsPresent())
					launchActivity(Const.LoginActivity);
				else
					launchActivity(Const.AddressActivity);
				break;
			case R.id.my_order_item :
				if(!mSharedPreferenceUtil.isUserDetailsPresent())
					launchActivity(Const.LoginActivity);
				else
					launchActivity(Const.OrderActivity);
                break;
			case R.id.my_cart_item :
				launchActivity(Const.CartActivity);
				break;
            case R.id.call_item :
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+mSharedPreferenceUtil.getVendorContact()));
                mContext.startActivity(intent);
                break;
            case R.id.share_item :
				openShareVia();
				break;
			case R.id.logout_item :
                 if (mSharedPreferenceUtil.isUserDetailsPresent()) {
					 AlertDialog dialog;
					 AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
					 dialogBuilder.setTitle("Logout");
					 dialogBuilder.setMessage("Are you sure you want to logout?");
					 dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						 @Override
						 public void onClick(DialogInterface dialog, int which) {
							 mSharedPreferenceUtil.removeUserDetails();
                             invalidateDrawerLayout();
						 }
					 });
					 dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
						 @Override
						 public void onClick(DialogInterface dialog, int which) {
							 if (dialog != null) {
								 dialog.dismiss();
							 }
						 }
					 });
					 dialog = dialogBuilder.create();
					 dialog.show();
				 }
				break;
			default:
				Toast.makeText(mContext,"Coming Soon!!!!",Toast.LENGTH_SHORT).show();
				break;
		}
		invalidateDrawerLayout();
	}

	public void openShareVia() {
		/*ApplicationInfo app = mContext.getApplicationContext().getApplicationInfo();
		String filePath = app.sourceDir;
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("**//**//*");
		//intent.setPackage("com.android.bluetooth");
		intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));
		mContext.startActivity(Intent.createChooser(intent, "Share app"));*/

        try{
            ArrayList<Uri> uris = new ArrayList<Uri>();
            Intent sendIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            sendIntent.setType("application/vnd.android.package-archive");
            ApplicationInfo app = mContext.getApplicationContext().getApplicationInfo();
            uris.add(Uri.fromFile(new File(app.publicSourceDir)));
            sendIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            mContext.startActivity(Intent.createChooser(sendIntent, null));
        }catch(Exception e){
            ArrayList<Uri> uris = new ArrayList<Uri>();
            Intent sendIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            sendIntent.setType("application/zip");
            ApplicationInfo app = mContext.getApplicationContext().getApplicationInfo();
            uris.add(Uri.fromFile(new File(app.publicSourceDir)));
            sendIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            mContext.startActivity(Intent.createChooser(sendIntent, null));
        }
	}

}
