package com.laymanizer.rashan.Utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.laymanizer.rashan.Address.AddressManager;
import com.laymanizer.rashan.Cart.CartItemVariantModel;
import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.Model.CategoryModel;
import com.laymanizer.rashan.Model.CityModel;
import com.laymanizer.rashan.DateTime.DateTimeModel;
import com.laymanizer.rashan.Model.ItemVariantModel;
import com.laymanizer.rashan.Model.LocationModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Model.ItemModel;
import com.laymanizer.rashan.Model.SubCategoryModel;
import com.laymanizer.rashan.DateTime.TimeModel;
import com.laymanizer.rashan.Model.UserModel;
import com.laymanizer.rashan.Order.OrderItemModel;
import com.laymanizer.rashan.Order.OrderModel;
import com.laymanizer.rashan.Order.OrderStoreModel;
import com.laymanizer.rashan.Volley.CustomJsonObjectRequest;
import com.laymanizer.rashan.Volley.VolleyDataRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Abhimanyu on 09-09-2015.
 */
public class QueryHelper {
    final String TAG = "QueryHelper";
    List<Model> mModelList;
    Model mModel;
    IQueryHelperCallback mIQueryHelperCallback;
    static Context mContext;
    private String mSearchUrl;
    private String mVolleyRequestTag;
    private String mError;

    SharedPreferenceUtil mSharedPreferenceUtil;
    AddressManager mAddressManager;

    public QueryHelper(Context context) {
        mContext = context;
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mAddressManager = AddressManager.getInstance(mContext);
    }

    public void setQueryHelperCallback(IQueryHelperCallback queryHelperCallback) {
        mIQueryHelperCallback = queryHelperCallback;
    }

    public  void getList(final int modelType, JSONObject queryParam) {
        String searchUrl;
        mVolleyRequestTag = ""+modelType;
        switch (modelType) {
            case Const.ORDER_ADD_MODEL_TYPE:
            case Const.ORDER_MODEL_LIST_TYPE:
                searchUrl = Const.BASE_URL+Const.PLACE_ORDER_URL;
                break;
            default:
                return;
        }
        queryServer(modelType, searchUrl, queryParam);
        Log.e(TAG, "getList url : " + searchUrl);
        Log.e(TAG, queryParam.toString());
    }

    public void getList(final int modelType, final Map<String, String> map) {
        String searchUrl;
        mVolleyRequestTag = ""+modelType;
        switch (modelType) {
            case Const.CITY_MODEL_LIST_TYPE:
                searchUrl = Const.BASE_URL+Const.CITY_URL;
                break;
            case Const.LOCATION_MODEL_LIST_TYPE:
                searchUrl = Const.BASE_URL+Const.LOCATION_URL;
                break;
            case Const.CATEGORY_MODEL_LIST_TYPE:
                searchUrl = Const.BASE_URL+Const.CATEGORY_LIST_URL;
                break;
            case Const.SEARCH_ITEM_MODEL_LIST_TYPE:
            case Const.STORE_ITEM_MODEL_LIST_TYPE:
            case Const.CART_ITEM_MODEL_LIST_TYPE:
                searchUrl = Const.BASE_URL+Const.STORE_ITEM_URL;
                break;
            case Const.ADDRESS_MODEL_LIST_TYPE:
                searchUrl = Const.BASE_URL+Const.ADDRESS_ITEM_URL;
                break;
            case Const.ADDRESS_AND_TIME_MODEL_LIST_TYPE:
                searchUrl = Const.BASE_URL+Const.CHECKOUT_DATA_URL;
                break;
            default:
                return;
        }
        queryServer(modelType, searchUrl, map);
        Log.e(TAG, "getList url : " + searchUrl);
        for (Map.Entry<String,String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.e(TAG, "key : " + key + " value : "+ value);
        }
    }


    public void getModel(final int modelType, final Map<String, String> map) {
        String searchUrl;
        mVolleyRequestTag = ""+modelType;
        switch (modelType) {
            case Const.USER_ITEM_MODEL_TYPE:
                searchUrl = Const.BASE_URL+Const.CUSTOMER_MANAGE_URL;
                break;
            case Const.INIT_MODEL_TYPE:
                searchUrl = Const.BASE_URL+Const.INIT_MODEL_URL;
                break;
            default:
                return;
        }
        queryServer(modelType, searchUrl, map);
        Log.e(TAG, "getList url : " + searchUrl);
        for (Map.Entry<String,String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.e(TAG, "key : " + key + " value : "+ value);
        }
    }

    public void cancelPreviousRequest(){
        Log.e(TAG, mVolleyRequestTag + " abhi request to cancel previous request");
        VolleyDataRequest.getInstance().cancelPendingRequests(mVolleyRequestTag);
        //mVolleyRequestTag = null;
    }

    private void queryServer(final int modelType, final String url, final JSONObject queryParams) {
        CustomJsonObjectRequest jsonObjReq = new CustomJsonObjectRequest(
                Request.Method.POST, // the request method
                url, // the URL
                queryParams, // the parameters for the php
                new Response.Listener<JSONObject>() { // the response listener
                    @Override
                    public void onResponse(JSONObject response){
                        Log.e(TAG, "response length : "+response.length());
                        Log.e(TAG, "response : "+response.toString());
                        try {
                            if((Const.ALL_MODEL_LIST_TYPE & modelType) != 0) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                createModelListFromJSONArray(modelType, jsonArray);
                                if (mIQueryHelperCallback != null) {
                                    mIQueryHelperCallback.onQueryCompleted(Const.NO_ERROR);
                                    mIQueryHelperCallback.copyModelList(mModelList); //it returns the list of Model
                                } else {
                                    Log.e(TAG, "mIQueryHelperCallback null");
                                }
                            } else if((Const.ALL_MODEL_TYPE & modelType) != 0) {
                                if(modelType == Const.ORDER_ADD_MODEL_TYPE) {
                                    Log.e(TAG, "ORDER_ADD_MODEL_TYPE response");
                                    if (mIQueryHelperCallback != null) {
                                        String errorMessage = response.getString("error");
                                        if (!errorMessage.equals("")) {
                                            mIQueryHelperCallback.onQueryCompleted(Const.ERROR_IN_ORDER, errorMessage);
                                        } else {
                                            HashMap<String, String> args= new HashMap<String, String>();
                                            args.put("delivery_date", response.getString("delivery_date"));
                                            args.put("delivery_time", response.getString("delivery_time"));
                                            mIQueryHelperCallback.onQueryCompleted(Const.NO_ERROR, args);
                                        }
                                    }
                                } else {
                                    createModelFromJSONObject(modelType, response);
                                    if (mIQueryHelperCallback != null) {
                                        mIQueryHelperCallback.copyModel(mModel);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() { // the error listener
                    @Override
                    public void onErrorResponse(VolleyError volleyerror) {
                        Log.d(TAG, "Error POST "+volleyerror.getMessage());
                        int errorType = Const.ERROR_NO_CONNECTION;
                        if (volleyerror instanceof NoConnectionError) {
                            errorType = Const.ERROR_NO_CONNECTION;
                        } else if (volleyerror instanceof TimeoutError) {
                            errorType = Const.ERROR_TIME_OUT;
                        }
                        if (mIQueryHelperCallback != null) {
                            mIQueryHelperCallback.onQueryCompleted(errorType);
                        }
                    }
                });
        jsonObjReq.setPriority(Request.Priority.HIGH);
        VolleyDataRequest.getInstance().addToRequestQueue(jsonObjReq, mVolleyRequestTag);
    }


    private void queryServer(final int modelType, final String url, final Map<String, String> map) {
        CustomJsonObjectRequest jsonObjReq = new CustomJsonObjectRequest(
                Request.Method.POST, // the request method
                url, // the URL
                new JSONObject(map), // the parameters for the php
                new Response.Listener<JSONObject>() { // the response listener
                    @Override
                    public void onResponse(JSONObject response){
                        Log.e(TAG, "response length : "+response.length());
                        Log.e(TAG, "response : "+response.toString());
                        try {
                            if((Const.ALL_MODEL_LIST_TYPE & modelType) != 0) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                createModelListFromJSONArray(modelType, jsonArray);
                                if (mIQueryHelperCallback != null) {
                                    mIQueryHelperCallback.onQueryCompleted(Const.NO_ERROR);
                                    mIQueryHelperCallback.copyModelList(mModelList); //it returns the list of Model
                                } else {
                                    Log.e(TAG, "mIQueryHelperCallback null");
                                }
                            } else if((Const.ALL_MODEL_TYPE & modelType) != 0) {
                                createModelFromJSONObject(modelType, response);
                                if (mIQueryHelperCallback != null) {
                                    mIQueryHelperCallback.onQueryCompleted(Const.NO_ERROR);
                                    mIQueryHelperCallback.copyModel(mModel);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() { // the error listener
                    @Override
                    public void onErrorResponse(VolleyError volleyerror) {
                        Log.d(TAG, "Error POST "+volleyerror.getMessage());
                        int errorType = Const.ERROR_NO_CONNECTION;
                        if (volleyerror instanceof NoConnectionError) {
                                errorType = Const.ERROR_NO_CONNECTION;
                        } else if (volleyerror instanceof TimeoutError) {
                            errorType = Const.ERROR_TIME_OUT;
                        }
                        if (mIQueryHelperCallback != null) {
                            mIQueryHelperCallback.onQueryCompleted(errorType);
                        }
                    }
                });
        if(modelType == Const.USER_ITEM_MODEL_TYPE)
            jsonObjReq.setPriority(Request.Priority.IMMEDIATE);
        else
            jsonObjReq.setPriority(Request.Priority.HIGH);
        VolleyDataRequest.getInstance().addToRequestQueue(jsonObjReq, mVolleyRequestTag);
    }

    private void createModelListFromJSONArray(int modelType, JSONArray jsonArray) {
        try {
            mModelList = new ArrayList<Model>();
            switch (modelType) {
                case Const.CITY_MODEL_LIST_TYPE:
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject modelObject = (JSONObject) jsonArray.get(i);
                        CityModel cityModel = new CityModel();
                        Log.e("Mannu", modelObject.getString("city_name"));
                        cityModel.setmCityId(modelObject.getString("city_id"));
                        cityModel.setCityName(modelObject.getString("city_name"));
                        mModelList.add(cityModel);
                    }
                    break;
                case Const.LOCATION_MODEL_LIST_TYPE:
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject modelObject = (JSONObject) jsonArray.get(i);
                        LocationModel locationModel = new LocationModel();
                        locationModel.setmCityId(modelObject.getString("city_id"));
                        locationModel.setmLocationId(modelObject.getString("location_id"));
                        locationModel.setmLocationName(modelObject.getString("location_name"));
                        locationModel.setmStoreIds(modelObject.getString("store_ids"));
                        mModelList.add(locationModel);
                    }
                    break;
                case Const.CATEGORY_MODEL_LIST_TYPE:
                    for (int i = 0; i < jsonArray.length()/2; i++) {
                        Log.e(TAG, "creating categoryModel");
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i * 2);
                        JSONObject categoryObject = jsonObject.getJSONObject("category");
                        CategoryModel categoryModel = new CategoryModel();
                        categoryModel.setCategoryId(categoryObject.getString("category_id"));
                        categoryModel.setCategoryName(categoryObject.getString("category_name"));
                        categoryModel.setCategoryDetail(categoryObject.getString("category_detail"));
                        categoryModel.setImageUrl(Const.BASE_URL + categoryObject.getString("image_url"));

                        List<Model> subCategoryModelList = new ArrayList<>();
                        jsonObject = (JSONObject) jsonArray.get((i * 2) + 1);
                        JSONArray subCategoryArray = jsonObject.getJSONArray("subcategory");
                        for (int j = 0; j < subCategoryArray.length(); j++) {
                            JSONObject subCategoryObject = subCategoryArray.getJSONObject(j);
                            SubCategoryModel subCategoryModel = new SubCategoryModel();
                            subCategoryModel.setSubCategoryId(subCategoryObject.getString("subcategory_id"));
                            subCategoryModel.setSubCategoryName(subCategoryObject.getString("subcategory_name"));
                            subCategoryModelList.add(subCategoryModel);
                            categoryModel.addToCategoryModelList(subCategoryModel);
                            //Log.e("keelback", subCategoryModel.getId()+" "+subCategoryModel.getName()+" added");
                            mSharedPreferenceUtil.addToSubCategoryMap(subCategoryModel);
                        }
                        mSharedPreferenceUtil.addToCategoryModelList(categoryModel);
                        mModelList.add(categoryModel);
                    }
                    break;
                case Const.SEARCH_ITEM_MODEL_LIST_TYPE:
                case Const.STORE_ITEM_MODEL_LIST_TYPE:
                    String prevItemId="";
                    ItemModel storeItemModel = null;
                    for (int i = 0; i<jsonArray.length(); i++) {
                        JSONObject modelObject = (JSONObject) jsonArray.get(i);
                        if(!prevItemId.equals(modelObject.getString("item_id"))) {
                            if (storeItemModel != null) {
                                //if ((modelType & Const.STORE_ITEM_MODEL_LIST_TYPE) != 0)
                                storeItemModel = (ItemModel)((SubCategoryModel)mSharedPreferenceUtil.getSubCategoryModel(storeItemModel.getSubCategoryId())).addToItemModelList(storeItemModel);
                                mModelList.add(storeItemModel);
                            }
                            prevItemId = modelObject.getString("item_id");
                            storeItemModel = new ItemModel();
                            List<Model> list = new ArrayList<Model>();
                            storeItemModel.setVariantsList(list);
                            storeItemModel.setItemId(modelObject.getString("item_id"));
                            storeItemModel.setCategoryId(modelObject.getString("category_id"));
                            storeItemModel.setSubCategoryId(modelObject.getString("subcategory_id"));
                        }
                        ItemVariantModel itemVariantModel = new ItemVariantModel();
                        itemVariantModel.setItemName(modelObject.getString("item_name"));
                        itemVariantModel.setRegionalName(modelObject.getString("regional_name"));
                        itemVariantModel.setUnitId(modelObject.getString("unit_id"));
                        itemVariantModel.setStoreId(modelObject.getString("store_id"));
                        itemVariantModel.setStoreName(modelObject.getString("store_name"));
                        itemVariantModel.setPrice(modelObject.getString("price"));
                        itemVariantModel.setMRP(modelObject.getString("mrp"));
                        itemVariantModel.setQuantity(modelObject.getString("quantity"));
                        itemVariantModel.setMeasureMent(modelObject.getString("measurement"));
                        itemVariantModel.setIsInStock(modelObject.getString("is_in_stock"));
                        itemVariantModel.setImageUri(Const.BASE_URL + modelObject.getString("image_url"));
                        itemVariantModel.setLimit(modelObject.getInt("limit"));
                        if(storeItemModel != null) {
                            storeItemModel.addToVariantList(itemVariantModel);
                        }
                    }
                    if (storeItemModel != null) {
                        //if ((modelType & Const.STORE_ITEM_MODEL_LIST_TYPE) != 0)
                        storeItemModel = (ItemModel)((SubCategoryModel)mSharedPreferenceUtil.getSubCategoryModel(storeItemModel.getSubCategoryId())).addToItemModelList(storeItemModel);
                        mModelList.add(storeItemModel);
                    }
                    break;
                case Const.CART_ITEM_MODEL_LIST_TYPE:
                    for (int i=0; i<jsonArray.length(); i++) {
                        JSONObject modelObject = (JSONObject) jsonArray.get(i);
                        CartItemVariantModel cartItemVariantModel = new CartItemVariantModel();
                        cartItemVariantModel.setUnitId(modelObject.getString("unit_id"));
                        cartItemVariantModel.setItemName(modelObject.getString("item_name"));
                        cartItemVariantModel.setRegionalName(modelObject.getString("regional_name"));
                        cartItemVariantModel.setStoreId(modelObject.getString("store_id"));
                        cartItemVariantModel.setStoreName(modelObject.getString("store_name"));
                        cartItemVariantModel.setPrice(modelObject.getString("price"));
                        cartItemVariantModel.setMRP(modelObject.getString("mrp"));
                        cartItemVariantModel.setQuantity(modelObject.getString("quantity"));
                        cartItemVariantModel.setMeasureMent(modelObject.getString("measurement"));
                        cartItemVariantModel.setIsInStock(modelObject.getString("is_in_stock"));
                        cartItemVariantModel.setImageUri(Const.BASE_URL + modelObject.getString("image_url"));
                        cartItemVariantModel.setLimit(modelObject.getInt("limit"));
                        mModelList.add(cartItemVariantModel);
                    }
                    break;
                case Const.ADDRESS_MODEL_LIST_TYPE:
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject modelObject = (JSONObject) jsonArray.get(i);
                        AddressModel addressModel = new AddressModel();
                        addressModel.setAddressId(modelObject.getString("address_id"));
                        addressModel.setAddressLabel(modelObject.getString("address_label"));
                        addressModel.setAddressReceiver(modelObject.getString("address_receiver"));
                        addressModel.setAddress1(modelObject.getString("address1"));
                        addressModel.setAddress2(modelObject.getString("address2"));
                        addressModel.setLocationId(modelObject.getString("location_id"));
                        addressModel.setLocationName(modelObject.getString("location_name"));
                        mModelList.add(addressModel);
                    }
                    break;
                case Const.ADDRESS_AND_TIME_MODEL_LIST_TYPE:
                    JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                    JSONObject addressModelObject = jsonObject.getJSONObject("address");
                    if(addressModelObject.getString("has_address").equals("yes")) {
                        AddressModel addressModel = new AddressModel();
                        addressModel.setAddressId(addressModelObject.getString("address_id"));
                        addressModel.setAddressLabel(addressModelObject.getString("address_label"));
                        addressModel.setAddressReceiver(addressModelObject.getString("address_receiver"));
                        addressModel.setAddress1(addressModelObject.getString("address1"));
                        addressModel.setAddress2(addressModelObject.getString("address2"));
                        addressModel.setLocationId(addressModelObject.getString("location_id"));
                        addressModel.setLocationName(addressModelObject.getString("location_name"));
                        mSharedPreferenceUtil.setCurrentDeliveryAddressId(addressModel.getAddressId());
                        mAddressManager.setDeliveryAddress(addressModel);
                    } else {
                        mAddressManager.setDeliveryAddress(null);
                        mSharedPreferenceUtil.removeCurrentDeliveryAddressId();
                    }
                    for (int i = 0; i < jsonArray.length()/2; i++) {
                        jsonObject = (JSONObject) jsonArray.get(i*2+1);
                        JSONObject dateObject = jsonObject.getJSONObject("date");
                        DateTimeModel dateTimeModel = new DateTimeModel();
                        dateTimeModel.setName(dateObject.getString("name"));
                        dateTimeModel.setDate(dateObject.getString("date"));

                        jsonObject = (JSONObject)jsonArray.get((i+1)*2);
                        JSONArray timeArray = jsonObject.getJSONArray("time");
                        for(int j=0; j<timeArray.length(); j++) {
                            JSONObject timeObject = timeArray.getJSONObject(j);
                            TimeModel timeModel = new TimeModel();
                            timeModel.setTime(timeObject.getString("time"));
                            dateTimeModel.addToTimeModelList(timeModel);
                        }
                        mModelList.add(dateTimeModel);
                    }
                    break;
                case Const.ORDER_MODEL_LIST_TYPE:
                    for (int i=0; i<jsonArray.length(); i++) {
                        JSONObject orderObject = (JSONObject) jsonArray.get(i);
                        OrderModel orderModel = new OrderModel();

                        JSONObject addressObject = orderObject.getJSONObject("address");
                        AddressModel deliveryAddress = new AddressModel();
                        deliveryAddress.setAddressLabel(addressObject.getString("label"));
                        deliveryAddress.setAddressReceiver(addressObject.getString("receiver"));
                        deliveryAddress.setAddress1(addressObject.getString("address1"));
                        deliveryAddress.setAddress2(addressObject.getString("address2"));
                        deliveryAddress.setLocationName(addressObject.getString("location_name"));

                        orderModel.setDeliveryAddress(deliveryAddress);
                        orderModel.setOrderId(orderObject.getString("order_id"));
                        orderModel.setSubTotal(orderObject.getString("sub_total"));
                        orderModel.setDeliveryCharges(orderObject.getString("delivery_charges"));
                        orderModel.setPayableAmount(orderObject.getString("payable_amount"));
                        orderModel.setOrderDate(orderObject.getString("order_date"));
                        orderModel.setDeliveryDateTime(orderObject.getString("delivery_datetime"));
                        orderModel.setOrderStatus(orderObject.getString("order_status"));

                        JSONArray storeOrders = orderObject.getJSONArray("store_items");
                        for(int j=0; j<storeOrders.length(); j++) {
                            JSONObject storeOrderObject = storeOrders.getJSONObject(j);
                            OrderStoreModel orderStoreModel = new OrderStoreModel();
                            orderStoreModel.setStoreOrderNumber(storeOrderObject.getString("order_number"));
                            orderStoreModel.setStoreName(storeOrderObject.getString("store_name"));
                            orderStoreModel.setStoreOrderAmount(storeOrderObject.getString("order_amount"));
                            orderStoreModel.setStoreOrderStatus(storeOrderObject.getString("order_status"));

                            JSONArray orderItems = storeOrderObject.getJSONArray("items");
                            for(int k=0; k<orderItems.length(); k++) {
                                JSONObject orderItemObject = orderItems.getJSONObject(k);
                                OrderItemModel orderItemModel = new OrderItemModel();
                                orderItemModel.setItemName(orderItemObject.getString("item_name"));
                                orderItemModel.setRegionalName(orderItemObject.getString("regional_name"));
                                orderItemModel.setQuantity(orderItemObject.getString("quantity"));
                                orderItemModel.setPrice(orderItemObject.getString("price"));
                                orderItemModel.setMRP(orderItemObject.getString("mrp"));
                                orderItemModel.setImageUri(Const.BASE_URL + orderItemObject.getString("image_uri"));
                                orderItemModel.setCount(orderItemObject.getString("count"));
                                orderStoreModel.addItemInStore(orderItemModel);
                            }
                            orderModel.addStoresOrderInOrder(orderStoreModel);
                        }
                        mModelList.add(orderModel);
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void createModelFromJSONObject(int modelType, JSONObject modelObject) {
        try {
            switch (modelType) {
                case Const.USER_ITEM_MODEL_TYPE:
                    UserModel userModel = new UserModel();
                    userModel.setUserId(modelObject.getString("user_id"));
                    userModel.setUserName(modelObject.getString("user_name"));
                    userModel.setPassword(modelObject.getString("password"));
                    userModel.setMobileNo(modelObject.getString("mobile_number"));
                    userModel.setEmailId(modelObject.getString("email_id"));
                    mModel = userModel;
                    break;
                case Const.INIT_MODEL_TYPE:
                    mSharedPreferenceUtil.setVendorContact(modelObject.getString("vendor_contact"));
                    mSharedPreferenceUtil.setCognalysAppID(modelObject.getString("app_id"));
                    mSharedPreferenceUtil.setCognalysAccessToken(modelObject.getString("access_token"));
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public interface IQueryHelperCallback {
        void copyModelList(List<Model> modelList);
        void copyModel(Model model);
        void onQueryCompleted(int errorType);
        void onQueryCompleted(int errorType, HashMap <String, String> args);
        void onQueryCompleted(int errorType, String errorMessage);
    }
}

