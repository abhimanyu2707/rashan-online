package com.laymanizer.rashan.Model;

/**
 * Created by Abhimanyu on 09-09-2015.
 */
public interface Model {
    public String getId();
    public Model getItem();
    public String getName();
}
