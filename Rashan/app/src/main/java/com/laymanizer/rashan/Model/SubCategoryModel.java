package com.laymanizer.rashan.Model;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Abhimanyu on 09-10-2015.
 */
public class SubCategoryModel implements Model {
    String mSubCategoryId;
    String mSubCategoryName;
    List<Model> mItemModelList = null;
    Map<String, Model> mItemModelMap;

    @Override
    public String getId() {
        return mSubCategoryId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mSubCategoryName;
    }

    public String getSubCategoryId() {
        return mSubCategoryId;
    }

    public void setSubCategoryId(String mSubCategoryId) {
        this.mSubCategoryId = mSubCategoryId;
    }

    public String getSubCategoryName() {
        return mSubCategoryName;
    }

    public void setSubCategoryName(String mSubCategoryName) {
        this.mSubCategoryName = mSubCategoryName;
    }

    public List<Model> getItemModelList() {
        return mItemModelList;
    }

    public void setItemModelList(List<Model> mItemModelList) {
        this.mItemModelList = mItemModelList;
    }

    public Model addToItemModelList(Model model) {
       if (mItemModelList == null) {
           mItemModelList = new ArrayList<Model>();
           mItemModelMap = new HashMap<String, Model>();
       }
        Log.i("keelback", "adding model");
        if(mItemModelMap.get(model.getId()) == null) {
            mItemModelList.add(model);
            mItemModelMap.put(model.getId(), model);
        }else{
            model = mItemModelMap.get(model.getId());
            Log.i("keelback", "model not added");
        }
        return model;
    }

    public Model getItemModelById(String itemId) {
        if(mItemModelMap != null) {
            return mItemModelMap.get(itemId);
        }
        return null;
    }
}