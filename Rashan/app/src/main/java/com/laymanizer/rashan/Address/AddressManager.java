package com.laymanizer.rashan.Address;

import android.content.Context;
import com.laymanizer.rashan.Model.Model;

import java.util.List;

/**
 * Created by Abhimanyu on 02-11-2015.
 */
public class AddressManager {
    public static AddressManager mAddressManager = null;
    List<Model> mAddressModelList;
    Model mDeliveryAddress = null;
    Model mEditableAddress = null;

    private static Context mContext;
    //private static SharedPreferenceUtil mSharedPreferenceUtil;

    private AddressManager() {
        //mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
    }

    public static AddressManager getInstance(Context context) {
        if(mAddressManager == null) {
            mContext = context;
            mAddressManager = new AddressManager();
        }
        return mAddressManager;
    }

    public List<Model> getAddressModelList() {
        return mAddressModelList;
    }

    public void setAddressModelList(List<Model> mAddressModelList) {
        this.mAddressModelList = mAddressModelList;
    }

    public Model getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(Model mDeliveryAddress) {
        this.mDeliveryAddress = mDeliveryAddress;
    }

    public void setEditableAddress (Model editableAddressModel) {
        if (editableAddressModel != null) {
            mEditableAddress = editableAddressModel;
        } else {
            mEditableAddress = null;
        }
    }

    public Model getEditableAddress() {
        return mEditableAddress;
    }
}
