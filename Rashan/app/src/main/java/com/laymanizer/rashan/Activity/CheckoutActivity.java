package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.laymanizer.rashan.Address.AddressManager;
import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.DateTime.DateTimeManager;
import com.laymanizer.rashan.DateTime.DateTimeModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.DateTime.TimeModel;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;
import com.laymanizer.rashan.View.DateTimeDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckoutActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback, DateTimeDialog.IDateTimeDialogCallback {

    View mRootLayout;
    LayoutInflater mLayoutInflater;
    RelativeLayout mCheckoutContent;
    LinearLayout mLayoutProgress;

    TextView mTotalAmount;
    TextView mDeliveryCharges;
    RelativeLayout mLayoutDiscount;
    TextView mDiscount;
    TextView mPayableAmount;
    TextView mEnableCoupon;
    LinearLayout mLayoutApplyCoupon;
    EditText mCouponCode;
    Button mApplyCoupon;
    Boolean mApplyCouponOn;
    LinearLayout mAddDeliveryAddress;
    LinearLayout mPickDeliveryAddress;
    TextView mAddressLabel;
    TextView mAddress;
    LinearLayout mPickDeliveryTime;
    TextView mDeliveryTimeLabel;
    TextView mDeliveryTimeText;
    DateTimeDialog mDateTimeDialog;
    TextView mGoToPayment;

    Context mContext;
    CartManager mCartManager;
    AddressManager mAddressManager;
    DateTimeManager mDateTimeManager;
    QueryHelper mQueryHelper;
    SharedPreferenceUtil mSharedPreferenceUtil;

    List<Model> mDateTimeModelList;
    List<Model> mTimeModelList;
    DateTimeModel mDeliveryDate;
    TimeModel mDeliveryTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("abhi", "create checkoutactivity");
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void initViews() {
        mLayoutInflater = LayoutInflater.from(mContext);
        mRootLayout = (View)mLayoutInflater.inflate(R.layout.checkout_activity, null);
        mCartManager = CartManager.getInstance(mContext);
        mAddressManager = AddressManager.getInstance(mContext);
        mDateTimeManager = DateTimeManager.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);/////

        mCheckoutContent = (RelativeLayout)mRootLayout.findViewById(R.id.checkout_content);
        mLayoutProgress = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);

        mTotalAmount = (TextView)mRootLayout.findViewById(R.id.total_amount);
        mDeliveryCharges = (TextView)mRootLayout.findViewById(R.id.delivery_charges);
        mLayoutDiscount = (RelativeLayout)mRootLayout.findViewById(R.id.layout_discount);
        mDiscount = (TextView)mRootLayout.findViewById(R.id.discount);
        mPayableAmount = (TextView)mRootLayout.findViewById(R.id.amount_payable);
        mEnableCoupon = (TextView)mRootLayout.findViewById(R.id.have_coupon_code);
        mEnableCoupon.setVisibility(View.GONE);
        mEnableCoupon.setOnClickListener(mOnEnableCouponClickListener);
        mLayoutApplyCoupon = (LinearLayout)mRootLayout.findViewById(R.id.layout_apply_coupon);
        mLayoutApplyCoupon.setVisibility(View.GONE);
        mCouponCode = (EditText)mRootLayout.findViewById(R.id.coupon_code);
        mApplyCoupon = (Button)mRootLayout.findViewById(R.id.coupon_code_apply);
        mApplyCoupon.setText(mContext.getResources().getString(R.string.checkout_cancel_coupon));
        mApplyCoupon.setOnClickListener(mOnApplyCouponListener);
        mApplyCouponOn = false;
        mAddDeliveryAddress = (LinearLayout)mRootLayout.findViewById(R.id.layout_add_delivery_address);
        mAddDeliveryAddress.setVisibility(View.VISIBLE);
        mAddDeliveryAddress.setOnClickListener(mOnAddDeliveryAddressListener);
        mPickDeliveryAddress = (LinearLayout)mRootLayout.findViewById(R.id.layout_delivery_address);
        mPickDeliveryAddress.setVisibility(View.GONE);
        mPickDeliveryAddress.setOnClickListener(mOnPickDeliveryAddressListener);
        mAddressLabel = (TextView)mRootLayout.findViewById(R.id.checkout_address_label);
        mAddress = (TextView)mRootLayout.findViewById(R.id.checkout_address_text);
        mPickDeliveryTime = (LinearLayout)mRootLayout.findViewById(R.id.layout_delivery_time);
        mPickDeliveryTime.setOnClickListener(mOnPickDeliveryTimeListener);
        mDeliveryTimeLabel = (TextView)mRootLayout.findViewById(R.id.checkout_delivery_time_label);
        mDeliveryTimeText = (TextView)mRootLayout.findViewById(R.id.checkout_delivery_time_text);
        mGoToPayment = (TextView)mRootLayout.findViewById(R.id.go_to_payment);
        mGoToPayment.setOnClickListener(mOnGoToPaymentListener);
    }

    View.OnClickListener mOnEnableCouponClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, "No Coupons available!", Toast.LENGTH_SHORT).show();
            //mEnableCoupon.setVisibility(View.GONE);
            //mLayoutApplyCoupon.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener mOnApplyCouponListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!mApplyCouponOn) {
                mCouponCode.setText("");
                mLayoutApplyCoupon.setVisibility(View.GONE);
                mEnableCoupon.setVisibility(View.VISIBLE);
            }
        }
    };

    View.OnClickListener mOnAddDeliveryAddressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, Const.AddressManagerActivity);
            intent.putExtra(Const.LaunchFrom, Const.CheckoutActivityName);
            startActivityForResult(intent, 1);
        }
    };

    View.OnClickListener mOnPickDeliveryAddressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, Const.AddressActivity);
            intent.putExtra(Const.LaunchFrom, Const.CheckoutActivityName);
            startActivityForResult(intent, 1);
        }
    };

    View.OnClickListener mOnPickDeliveryTimeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showDateTimeDialog();
        }
    };

    private void showDateTimeDialog() {
        if(mDateTimeDialog == null) {
            mDateTimeDialog = new DateTimeDialog(mContext);
            mDateTimeDialog.setIDateTimeDialogCallback(this);
        }
        mDateTimeDialog.setDateTimeList(mDateTimeModelList);
        mDateTimeDialog.showDialog();
    }

    View.OnClickListener mOnGoToPaymentListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mAddressManager.getDeliveryAddress() != null) {
                Intent intent = new Intent(mContext, Const.PaymentActivity);
                startActivity(intent);
            } else {
                Toast.makeText(mContext, "Please add a delivery address", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == AppCompatActivity.RESULT_OK){
                updateCheckoutAddress();
            }
            if (resultCode == AppCompatActivity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCheckoutContent.setVisibility(View.GONE);
        mLayoutProgress.setVisibility(View.VISIBLE);
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_list", "1");
        queryParams.put("user_id", mSharedPreferenceUtil.getCurrentUser().getId());
        queryParams.put("user_password", mSharedPreferenceUtil.getCurrentUser().getPassword());
        queryParams.put("location_id", mSharedPreferenceUtil.getCurrentLocation().getmLocationId());
        queryParams.put("current_delivery_address_id", mSharedPreferenceUtil.getCurrentDeliveryAddressId());
        mQueryHelper.getList(Const.ADDRESS_AND_TIME_MODEL_LIST_TYPE, queryParams);
        fillCheckoutContent();
    }

    private void fillCheckoutContent() {
        float totalAmount = mCartManager.getTotalPrice();
        float deliveryCharges = 0;
        float payableAmount = 0;
        mTotalAmount.setText(mContext.getResources().getString(R.string.Rs) + totalAmount);
        if(mCartManager.getMinimumOrderForFree() <= totalAmount) {
            mDeliveryCharges.setText("Free");
            mDeliveryCharges.setTextColor(mContext.getResources().getColor(R.color.checkout_green_accent));
        } else {
            deliveryCharges = mCartManager.getDeliveryCharges();
            mDeliveryCharges.setText(mContext.getResources().getString(R.string.Rs) + deliveryCharges);
            mDeliveryCharges.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
        }
        payableAmount = totalAmount + deliveryCharges;
        mPayableAmount.setText(mContext.getResources().getString(R.string.Rs) + payableAmount);
    }

    public void updateCheckoutAddress() {
        AddressModel addressModel = (AddressModel)mAddressManager.getDeliveryAddress();
        if(addressModel != null){
            mAddressLabel.setText(addressModel.getAddressLabel());
            String address = addressModel.getAddress1()+", "+addressModel.getAddress2()+", "+addressModel.getLocationName();
            mAddress.setText(address);
            mAddDeliveryAddress.setVisibility(View.GONE);
            mPickDeliveryAddress.setVisibility(View.VISIBLE);
        }
    }

    public void updateCheckoutDateTime() {
        mDateTimeManager.setDeliveryDate(mDeliveryDate);
        mDateTimeManager.setDeliveryTime(mDeliveryTime);
        mDeliveryTimeLabel.setText(mDeliveryDate.getName());
        mDeliveryTimeText.setText(mDeliveryDate.getDate() + ", " + mDeliveryTime.getName());
    }

    public void initializeCheckoutDateTime() {
        mDeliveryDate = (DateTimeModel)mDateTimeModelList.get(0);
        if(mDeliveryDate.getTimeModelListSize() == 0) {
            mDeliveryDate = (DateTimeModel) mDateTimeModelList.get(1);
        }
        mTimeModelList = mDeliveryDate.getTimeModelList();
        if(mTimeModelList != null && mTimeModelList.size()>0) {
            mDeliveryTime = (TimeModel)mTimeModelList.get(0);
            updateCheckoutDateTime();
        }
    }

    @Override
    public void copyModelList(List<Model> modelList) {
        updateCheckoutAddress();
        mDateTimeModelList = modelList;
        initializeCheckoutDateTime();
        mCheckoutContent.setVisibility(View.VISIBLE);
        mLayoutProgress.setVisibility(View.GONE);

    }

    @Override
    public void copyModel(Model model) {
    }

    @Override
    public void onQueryCompleted(int errorType) {
    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }

    @Override
    public void onTimeSelected(int dateModelIndex, int timeModelIndex) {
        mDeliveryDate = (DateTimeModel)mDateTimeModelList.get(dateModelIndex);
        mTimeModelList = mDeliveryDate.getTimeModelList();
        mDeliveryTime = (TimeModel)mTimeModelList.get(timeModelIndex);
        updateCheckoutDateTime();
    }
}
