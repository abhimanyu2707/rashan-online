package com.laymanizer.rashan.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sayal on 13-09-2015.
 */
public class ItemModel implements Model{
    //String mItemName;
    String mItemId;
    String mCategoryId;
    String mSubCategoryId;
    List<Model> mVariantsList;

    public ItemModel(){}

    public ItemModel(ItemModel model) {
        if (model != null) {
            mItemId = model.getId();
            //mItemName = model.getName();
        }
    }


    @Override
    public String getId() {
        return mItemId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return null;
    }

    public String getGroceryId() {
        return mItemId;
    }

    /*public String getGroceryName() {
        return mItemName;
    }*/

    public List<Model> getVariantsList() {
        return mVariantsList;
    }

    public String getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(String mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public String getSubCategoryId() {
        return mSubCategoryId;
    }

    public void setSubCategoryId(String mSubCategoryId) {
        this.mSubCategoryId = mSubCategoryId;
    }

    public void setVariantsList(List<Model> mVariantsList) {
        this.mVariantsList = mVariantsList;
    }

    public void setItemId(String mItemId) {
        this.mItemId = mItemId;
    }

   /* public void setItemName(String mItemName) {
        this.mItemName = mItemName;
    }*/


    public void addToVariantList(Model model){
        if(mVariantsList == null) {
            mVariantsList = new ArrayList<Model>();
        }

        mVariantsList.add(model);
    }

    public void removeFromList(Model model) {
        if (mVariantsList != null) {
            mVariantsList.remove(model);
            if (mVariantsList.size() == 0) {
                mVariantsList = null;
            }
        }
    }

    public int getVariantsSize() {
        if (mVariantsList == null) {
            return 0;
        }
        return mVariantsList.size();
    }
}
