package com.laymanizer.rashan.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.laymanizer.rashan.Adapter.ItemAdapter;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Model.SubCategoryModel;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.List;

/**
 * Created by VK on 18-09-2015.
 */
public class ListItemFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */

    final String TAG = "ListItemFragment";
    SharedPreferenceUtil mSharedPreferenceUtil;
    ItemAdapter mItemAdapter;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;

    View mRootLayout;
    Context mContext;
    QueryHelper mQueryHelper;
    CartManager mCartManager;
    Model mCurrentSubCategoryModel;

    List<Model> mStoreItemList;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
/*    public static ListItemFragment newInstance(int sectionNumber) {
        ListItemFragment fragment = new ListItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }*/

    public ListItemFragment() {
    }

    public void setArguments(Context cntxt, Model subModel) {
        mContext = cntxt;
        mCurrentSubCategoryModel = subModel;
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mCartManager = CartManager.getInstance(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = initViews(inflater, container);
        return rootView;
    }

    public View initViews(LayoutInflater inflater, ViewGroup container) {
        mRootLayout = inflater.inflate(R.layout.fragment_items_list, null);
        mRecyclerView = (RecyclerView) mRootLayout.findViewById(R.id.search_list);
        mItemAdapter = new ItemAdapter(getActivity(),mRecyclerView);
        Log.i("Bojja",""+((SubCategoryModel)mCurrentSubCategoryModel).getItemModelList());
        mItemAdapter.setDataList(((SubCategoryModel)mCurrentSubCategoryModel).getItemModelList());

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mItemAdapter);
        mItemAdapter.notifyDataSetChanged();
        Log.i("Mannu", "called initView requested for list to server");
        return mRootLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void notifyAdapter(){
        Log.i("Bojja",""+mCurrentSubCategoryModel.getId()+"Name: "+mCurrentSubCategoryModel.getName());
        Log.i("Bojja",""+((SubCategoryModel)mCurrentSubCategoryModel).getItemModelList());
        mItemAdapter.setDataList(((SubCategoryModel)mCurrentSubCategoryModel).getItemModelList());
        mItemAdapter.notifyDataSetChanged();
    }
}
