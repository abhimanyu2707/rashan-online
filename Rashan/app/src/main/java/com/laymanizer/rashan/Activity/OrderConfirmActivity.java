package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;

public class OrderConfirmActivity extends AppCompatActivity {
    View mRootLayout;
    LayoutInflater mLayoutInflater;

    Context mContext;
    TextView mDeliveryDate;
    TextView mDeliveryTime;
    TextView mCheckOrders;
    TextView mContinueShopping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
        }
        return true;
    }

    public void initViews() {
        mLayoutInflater = LayoutInflater.from(mContext);
        mRootLayout = mLayoutInflater.inflate(R.layout.order_confirm_activity, null);
        mCheckOrders = (TextView)mRootLayout.findViewById(R.id.check_orders);
        mCheckOrders.setOnClickListener(mOnCheckOrderListener);
        mDeliveryDate = (TextView)mRootLayout.findViewById(R.id.order_confirm_date);
        mDeliveryDate.setText(getIntent().getStringExtra("delivery_date"));
        mDeliveryTime = (TextView)mRootLayout.findViewById(R.id.order_confirm_time);
        mDeliveryTime.setText(getIntent().getStringExtra("delivery_time"));
        mContinueShopping = (TextView)mRootLayout.findViewById(R.id.continue_shopping);
        mContinueShopping.setOnClickListener(mContinueShoppingListener);
    }

    View.OnClickListener mOnCheckOrderListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            launchActivity(Const.OrderActivity);
        }
    };


    View.OnClickListener mContinueShoppingListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };


    private void launchActivity(Class activityName){
        Intent intent = new Intent(this,activityName);
        intent.putExtra(Const.LaunchFrom, activityName);
        startActivity(intent);
        finish();
    }
}
