package com.laymanizer.rashan.DBUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.HashMap;

/**
 * Created by Abhimanyu on 01-11-2015.
 */
public class MyCartDBManager extends DBManager{
    //Database Table
    public static final String TABLE = "mycart";
    public static final String COLUMN_ID = "unit_id";
    public static final String COLUMN_COUNT = "unit_count";

    //Table Creation SQL Statement
    public static final String CREATE_TABLE = "CREATE TABLE "
            + TABLE
            + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY, "
            + COLUMN_COUNT + " INTEGER DEFAULT 0"
            + ")";

    /**
     * Constructor
     *
     * @param ctx
     */
    public MyCartDBManager(Context ctx) {
        super(ctx);
    }

    public boolean createCartItem(long id, long count) {
        ContentValues args = new ContentValues();
        args.put(COLUMN_ID, id);
        args.put(COLUMN_COUNT, count);
        open();
        mDataBase.insert(TABLE, null, args);
        close();
        return true;
    }

    public boolean deleteCartItem(long id) {
        open();
        boolean ret = mDataBase.delete(TABLE, COLUMN_ID + "=" + id, null) > 0;
        close();
        return ret;
    }

    public void clearTable() {
        open();
        mDataBase.delete(TABLE, "1", null);
        close();
    }

    public boolean updateCartItem(long id, long count) {
        ContentValues args = new ContentValues();
        args.put(COLUMN_ID, id);
        args.put(COLUMN_COUNT, count);
        open();
        boolean ret = mDataBase.update(TABLE, args, COLUMN_ID + "=" + id, null) > 0;
        close();
        return ret;
    }

    public HashMap<String, Integer> getCartItemsCount() {
        HashMap <String, Integer> cartItemCountMap = new HashMap<String, Integer>();
        String selectQuery = "SELECT *  FROM " + MyCartDBManager.TABLE + " WHERE 1";
        open();
        Cursor cursor = mDataBase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do{
                cartItemCountMap.put(cursor.getString(0), cursor.getInt(1));
            }while (cursor.moveToNext());
        }
        close();
        return  cartItemCountMap;
    }
}
