package com.laymanizer.rashan.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.laymanizer.rashan.Activity.AddressActivity;
import com.laymanizer.rashan.Address.AddressManager;
import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;

import java.util.List;

/**
 * Created by Abhimanyu on 01-11-2015.
 */
public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    Context mContext;
    List<Model> mAddressList;
    AddressManager mAddressManager;
    boolean mIsSelectable;
    boolean mIsEditable;
    boolean mIsDeletable;

    public AddressAdapter(Context context) {
        mContext = context;
        mAddressManager = AddressManager.getInstance(mContext);
        mIsSelectable = false;
        mIsEditable = false;
        mIsDeletable = false;
    }

    public void setActions(boolean isSelectable, boolean isEditable, boolean isDeletable) {
        mIsSelectable = isSelectable;
        mIsEditable = isEditable;
        mIsDeletable = isDeletable;
    }

    public void setDataList(List<Model> list) {
        mAddressList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        if(mIsSelectable)
            itemView.setOnClickListener(mOnAddressClickListener);
        if(mIsEditable)
            viewHolder.editAddress.setOnClickListener(mOnEditAddressClickListener);
        if(mIsDeletable)
            viewHolder.deleteAddress.setOnClickListener(mOnDeleteAddressClickListener);
        else
            viewHolder.deleteAddress.setVisibility(View.GONE);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AddressModel addressModel = (AddressModel) mAddressList.get(position);
        holder.addressLabel.setText(addressModel.getAddressLabel());
        holder.receiverName.setText(addressModel.getAddressReceiver());
        holder.address1.setText(addressModel.getAddress1());
        holder.address2.setText(addressModel.getAddress2());
        holder.locationName.setText(addressModel.getLocationName());
        holder.editAddress.setTag(position);
        holder.deleteAddress.setTag(position);
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        if(mAddressList!=null)
            return mAddressList.size();
        return 0;
    }


    View.OnClickListener mOnAddressClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((AddressActivity)mContext).selectAddress(v.getTag().toString());
        }
    };

    View.OnClickListener mOnEditAddressClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            AlertDialog dialog;
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            dialogBuilder.setTitle("Edit Address?");
            dialogBuilder.setMessage("Are you sure you want to edit this address?");
            dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mAddressManager.setEditableAddress(mAddressList.get((int) v.getTag()));
                    launchActivity(Const.AddressManagerActivity);
                }
            });
            dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            dialog = dialogBuilder.create();
            dialog.show();
        }
    };

    View.OnClickListener mOnDeleteAddressClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            AlertDialog dialog;
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            dialogBuilder.setTitle("Delete Address?");
            dialogBuilder.setMessage("Are you sure you want to delete this address?");
            dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((AddressActivity)mContext).deleteAddress(v.getTag().toString());
                    /*mAddressList.remove(position);
                    notifyDataSetChanged();*/
                }
            });
            dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            dialog = dialogBuilder.create();
            dialog.show();
        }
    };

    public void launchActivity(Class targetClass) {
        Intent i = new Intent(mContext, targetClass);
        i.putExtra("EditAddress", true);
        mContext.startActivity(i);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private View itemView;
        private TextView addressLabel;
        private TextView receiverName;
        private TextView address1;
        private TextView address2;
        private TextView locationName;
        private ImageView editAddress;
        private ImageView deleteAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            addressLabel = (TextView)itemView.findViewById(R.id.address_label);
            receiverName = (TextView)itemView.findViewById(R.id.receiver_name);
            address1     = (TextView)itemView.findViewById(R.id.address1);
            address2     = (TextView)itemView.findViewById(R.id.address2);
            locationName = (TextView)itemView.findViewById(R.id.location_name);
            editAddress  = (ImageView)itemView.findViewById(R.id.edit_address);
            deleteAddress= (ImageView)itemView.findViewById(R.id.delete_address);
        }
    }
}
