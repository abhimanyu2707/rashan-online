package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.laymanizer.rashan.Adapter.ItemVariantAdapter;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.ItemModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Model.SubCategoryModel;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.List;

public class ItemVariantActivity extends AppCompatActivity {


    final String TAG = "ItemVariantActivity";
    SharedPreferenceUtil mSharedPreferenceUtil;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    ItemVariantAdapter mItemVariantAdapter;
    LinearLayout mCartLayout;
    TextView mTotalCount;
    TextView mTotalPrice;

    View mRootLayout;
    LayoutInflater mInflater;
    Context mContext;
    CartManager mCartManager;

    String mSubCategoryId;
    String mItemId;
    List<Model> mItemVariantList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mSubCategoryId = getIntent().getStringExtra("subcategory_id");
        mItemId = getIntent().getStringExtra("item_id");
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initViews() {
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mInflater = LayoutInflater.from(mContext);
        mCartManager = CartManager.getInstance(mContext);
        mRootLayout = (View)mInflater.inflate(R.layout.item_variant_activity, null);
        mRecyclerView = (RecyclerView) mRootLayout.findViewById(R.id.item_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ItemModel itemModel = (ItemModel)((SubCategoryModel)mSharedPreferenceUtil.getSubCategoryModel(mSubCategoryId)).getItemModelById(mItemId);
        mItemVariantList = itemModel.getVariantsList();
        mItemVariantAdapter = new ItemVariantAdapter(mContext, mRecyclerView);
        mItemVariantAdapter.setDataList(mItemVariantList);
        mRecyclerView.setAdapter(mItemVariantAdapter);
        mTotalCount = (TextView)mRootLayout.findViewById(R.id.total_count);
        mTotalPrice = (TextView)mRootLayout.findViewById(R.id.total_price);
        mCartLayout = (LinearLayout) mRootLayout.findViewById(R.id.my_cart_checkout);
        mCartLayout.setOnClickListener(mOnCartClickListener);
    }


    View.OnClickListener mOnCartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mCartManager.getTotalCount() == 0) {
                Toast.makeText(mContext, "Please add items to cart", Toast.LENGTH_SHORT).show();
                return;
            }
            launchActivity(Const.CartActivity);
        }
    };

    private void launchActivity(Class activityName){
        Intent intent = new Intent(this,activityName);
        intent.putExtra(Const.LaunchFrom, activityName);
        startActivity(intent);
    }


    public void updateValues() {
        mTotalPrice.setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getTotalPrice());
        mTotalCount.setText("" + mCartManager.getTotalCount());
        if(mCartManager.getTotalCount() <= 0)
            mCartLayout.setVisibility(View.GONE);
        else
            mCartLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateValues();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                exitActivity();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        exitActivity();
    }

    public View getCartLayout() {
        return mCartLayout;
    }

    public TextView getTotalPriceTextView() {
        return mTotalPrice;
    }

    public TextView getTotalCountTextView() {
        return mTotalCount;
    }

    private void exitActivity(){
        finish();
        //overridePendingTransition(R.anim.left_side_in, R.anim.rignt_side_out);
    }
}
