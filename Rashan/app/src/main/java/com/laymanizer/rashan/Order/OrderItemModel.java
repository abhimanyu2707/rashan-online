package com.laymanizer.rashan.Order;

import com.laymanizer.rashan.Model.ItemVariantModel;

/**
 * Created by Abhimanyu on 10-01-2016.
 */
public class OrderItemModel extends ItemVariantModel{
    String mCount;

    public String getCount() {
        return mCount;
    }

    public void setCount(String mCount) {
        this.mCount = mCount;
    }
}
