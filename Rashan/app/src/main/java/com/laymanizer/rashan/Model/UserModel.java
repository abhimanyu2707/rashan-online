package com.laymanizer.rashan.Model;

/**
 * Created by sayal on 11-09-2015.
 */
public class UserModel implements Model {

    private String mUserId;
    private String mMobileNo;
    private String mPassword;
    private String mUserName;
    private String mEmailId;


    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mMobileNo) {
        this.mMobileNo = mMobileNo;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String mEmailId) {
        this.mEmailId = mEmailId;
    }

    @Override
    public String getId() {
        return mUserId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mUserName;
    }


}
