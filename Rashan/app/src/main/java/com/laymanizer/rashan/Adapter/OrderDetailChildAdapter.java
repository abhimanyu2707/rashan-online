package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Order.OrderItemModel;
import com.laymanizer.rashan.Order.OrderManager;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Volley.ImageRequestQueue;

import java.util.List;

/**
 * Created by Abhimanyu on 24-01-2016.
 */
public class OrderDetailChildAdapter extends RecyclerView.Adapter<OrderDetailChildAdapter.ViewHolder> {
    Context mContext;
    List<Model> mOrderDetailChildList;
    OrderManager mOrderManager;

    public OrderDetailChildAdapter(Context context) {
        mContext = context;
        mOrderManager = OrderManager.getInstance(mContext);
    }


    public void setDataList(List<Model> list) {
        mOrderDetailChildList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_detail_child_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderItemModel orderItemModel = (OrderItemModel) mOrderDetailChildList.get(position);
        String itemImageUri = orderItemModel.getItemImageUri();
        ImageLoader imageLoader = ImageRequestQueue.getInstance(mContext).getImageLoader();
        if(!itemImageUri.equals(null) && itemImageUri.length()>0)
            holder.mStoreItemImage.setImageUrl(itemImageUri, imageLoader);
        holder.mStoreItemImage.setDefaultImageResId(R.drawable.default_image);

        String itemName = orderItemModel.getName();
        String regionalName = orderItemModel.getRegionalName();
        if(regionalName != null && regionalName.length()>0)
            itemName += " (" + regionalName + ")";
        holder.mStoreItemName.setText(itemName);
        holder.mStoreItemAmount.setText((Float.parseFloat(orderItemModel.getPrice()) * Integer.parseInt(orderItemModel.getCount())) + "");
        holder.mStoreItemQuantity.setText(orderItemModel.getQuantity());
        holder.mStoreItemPrice.setText(orderItemModel.getPrice() + " X " + orderItemModel.getCount());

        //holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        if(mOrderDetailChildList!=null)
            return mOrderDetailChildList.size();
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private View itemView;
        private NetworkImageView mStoreItemImage;
        private TextView mStoreItemName;
        private TextView mStoreItemAmount;
        private TextView mStoreItemQuantity;
        private TextView mStoreItemPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView      = itemView;
            mStoreItemImage    = (NetworkImageView)itemView.findViewById(R.id.store_item_image);
            mStoreItemName     = (TextView)itemView.findViewById(R.id.text_store_item_name);
            mStoreItemAmount   = (TextView)itemView.findViewById(R.id.text_store_item_amount);
            mStoreItemQuantity = (TextView)itemView.findViewById(R.id.text_store_item_quantity);
            mStoreItemPrice    = (TextView)itemView.findViewById(R.id.text_store_item_price_into_count);
        }
    }
}
