package com.laymanizer.rashan.DateTime;

import com.laymanizer.rashan.Model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhimanyu on 22-11-2015.
 */
public class DateTimeModel implements Model {

    private String mName;
    private String mDate;
    List<Model> mTimeModelList;

    @Override
    public String getId() {
        return null;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public List<Model> getTimeModelList() {
        return mTimeModelList;
    }

    public void addToTimeModelList(Model model){
        if(mTimeModelList == null) {
            mTimeModelList = new ArrayList<Model>();
        }

        mTimeModelList.add(model);
    }

    public void removeFromList(Model model) {
        if (mTimeModelList != null) {
            mTimeModelList.remove(model);
            if (mTimeModelList.size() == 0) {
                mTimeModelList = null;
            }
        }
    }

    public int getTimeModelListSize() {
        if (mTimeModelList == null) {
            return 0;
        }
        return mTimeModelList.size();
    }

}
