package com.laymanizer.rashan.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.laymanizer.rashan.Model.CityModel;
import com.laymanizer.rashan.Model.LocationModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Model.UserModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sayal on 11-09-2015.
 */



public class SharedPreferenceUtil {

    private CityModel mCurrentCity;
    private LocationModel mCurrentLocation;
    private UserModel mCurrentUser;
    private static SharedPreferences mSharedPreferences;
    private static SharedPreferences.Editor mEditor;
    List<Model> mCategoryModelList;
    HashMap<String, Model> mSubCategoryMap;
    List<String> mLoadedCategoryList;


    static Context mContext;
    static String NAME = "RASHAN_ONLINE";

    //init
    private static final String VENDOR_CONTACT = "vendor_contact_number";
    private static final String COGNALYS_APP_ID = "cognalys_app_id";
    private static final String COGNALYS_ACCESS_TOKEN = "cognalys_app_token";

    private static final String CITY_NAME="city_name";
    private static final String CITY_ID="city_id";

    private static final String LOCATION_NAME="location_name";
    private static final String LOCATION_ID="location_id";
    private static final String LOCATION_CITY_ID="location_city_id";

    //User Information
    private static final String USER_ID="user_id";
    private static final String MOBILE_NUMBER="mobile_number";
    private static final String PASSWORD="password";
    private static final String USER_NAME="user_name";
    private static final String EMAIL_ID="email_id";

    //checkout information
    private static final String DELIVERY_ADDRESS_ID = "delivery_address_id";


    private static SharedPreferenceUtil mSharedPreferenceUtil = null;


    private SharedPreferenceUtil(){};

    public static SharedPreferenceUtil getInstance(Context cntxt){
        if(mSharedPreferenceUtil==null) {
            mSharedPreferenceUtil = new SharedPreferenceUtil();
            mContext = cntxt;
            mSharedPreferences = mContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
            mEditor = mSharedPreferences.edit();
        }
        return mSharedPreferenceUtil;
    }

    public void setVendorContact(String contact) {
        mEditor.putString(VENDOR_CONTACT, contact);
        mEditor.commit();
    }
    public String getVendorContact() {
        String contact = mSharedPreferences.getString(VENDOR_CONTACT, null);
        if(contact != null)
            return contact;
        return "+919907096667";
    }

    public void setCognalysAppID(String app_id) {
        Log.e("keelback", "app_id"+app_id);
        mEditor.putString(COGNALYS_APP_ID, app_id);
        mEditor.commit();
    }

    public String getCognalysAppId() {
        String app_id = mSharedPreferences.getString(COGNALYS_APP_ID, null);
        if(app_id != null)
            return app_id;
        return "e0c0546605a247909a219b9";
    }

    public void setCognalysAccessToken(String app_token) {
        Log.e("keelback", "app_token"+app_token);
        mEditor.putString(COGNALYS_ACCESS_TOKEN, app_token);
        mEditor.commit();
    }

    public String getCognalysAppToken() {
        String app_token = mSharedPreferences.getString(COGNALYS_ACCESS_TOKEN, null);
        if(app_token != null)
            return app_token;
        return "78049d9c153274ba3390160c45a34a1a134b65d4";
    }

    public void setCurrentDeliveryAddressId(String currentDeliveryAddressId) {
        mEditor.putString(DELIVERY_ADDRESS_ID, currentDeliveryAddressId);
        mEditor.commit();
    }

    public void removeCurrentDeliveryAddressId() {
        mEditor.remove(DELIVERY_ADDRESS_ID);
        mEditor.commit();
    }

    public String getCurrentDeliveryAddressId() {
        String currentDeliveryAddressId = mSharedPreferences.getString(DELIVERY_ADDRESS_ID, null);
        if(currentDeliveryAddressId != null) {
            return currentDeliveryAddressId;
        }
        return "-1";
    }

    public CityModel getCurrentCity() {
        if (mCurrentCity != null) {
            return mCurrentCity;
        }
        String city_id = mSharedPreferences.getString(CITY_ID,null);
        String city_name = mSharedPreferences.getString(CITY_NAME,null);
        if (city_id != null && city_name != null) {
            CityModel city = new CityModel();
            city.setmCityId(city_id);
            city.setCityName(city_name);
            mCurrentCity = city;
            return mCurrentCity;
        }
        return null;
    }

    public UserModel getUserDetails() {
        if (mCurrentUser != null) {
            return mCurrentUser;
        }
        String userId = mSharedPreferences.getString(USER_ID, null);
        String mobileNumber = mSharedPreferences.getString(MOBILE_NUMBER,null);
        String password = mSharedPreferences.getString(PASSWORD,null);
        String useName = mSharedPreferences.getString(USER_NAME,null);
        String emailId = mSharedPreferences.getString(EMAIL_ID,null);
        if (userId != null) {
            UserModel user = new UserModel();
            user.setUserId(userId);
            user.setMobileNo(mobileNumber);
            user.setPassword(password);
            user.setUserName(useName);
            user.setEmailId(emailId);
            mCurrentUser = user;
            return mCurrentUser;
        }
        return null;
    }

    public String getUserMobileNubmer(){
        if(mCurrentUser!=null){
            return mCurrentUser.getMobileNo();
        }
        return null;
    }

    public LocationModel getCurrentLocation() {
        if (mCurrentLocation != null) {
            return mCurrentLocation;
        }
        String location_id = mSharedPreferences.getString(LOCATION_ID,null);
        String location_name = mSharedPreferences.getString(LOCATION_NAME,null);
        String location_city_id = mSharedPreferences.getString(LOCATION_CITY_ID,null);
        if (location_id != null && location_name != null && location_city_id != null) {
            LocationModel location = new LocationModel();
            location.setmLocationId(location_id);
            location.setmLocationName(location_name);
            location.setmCityId(location_city_id);
            mCurrentLocation = location;
            return mCurrentLocation;
        }
        return null;
    }

    public UserModel getCurrentUser() {
        return mCurrentUser;
    }

    public void setCurrentCity(Model city) {
        mCurrentCity = (CityModel)city;
        mEditor.putString(CITY_ID,mCurrentCity.getmCityId());
        mEditor.putString(CITY_NAME,mCurrentCity.getCityName());
        mEditor.commit();
    }

    public void setCurrentLocation(Model location) {
        mCurrentLocation = (LocationModel)location;
        mEditor.putString(LOCATION_ID,mCurrentLocation.getmLocationId());
        mEditor.putString(LOCATION_NAME,mCurrentLocation.getmLocationName());
        mEditor.putString(LOCATION_CITY_ID,mCurrentLocation.getmCityId());
        mEditor.commit();
    }

    public String getCurrentLocationCityText() {
       String locationText =  getCurrentLocation().getName()+","+getCurrentCity().getName();
       return locationText;
    }

    public void setUserDetails(Model user) {
        mCurrentUser = (UserModel)user;
        mEditor.putString(USER_ID, mCurrentUser.getId());
        mEditor.putString(MOBILE_NUMBER, mCurrentUser.getMobileNo());
        mEditor.putString(PASSWORD, mCurrentUser.getPassword());
        mEditor.putString(USER_NAME, mCurrentUser.getUserName());
        mEditor.putString(EMAIL_ID, mCurrentUser.getEmailId());
        mEditor.commit();
    }

    public void removeUserDetails() {
        mEditor.remove(USER_ID);
        mEditor.remove(MOBILE_NUMBER);
        mEditor.remove(PASSWORD);
        mEditor.remove(USER_NAME);
        mEditor.remove(EMAIL_ID);
        mEditor.commit();
        mCurrentUser = null;
    }

    public void setCurrentUser(Model user) {
         setUserDetails(user);
    }

    public boolean isLocationPresent() {
       return getCurrentCity() != null && getCurrentLocation() != null ;
    }

    public boolean isUserDetailsPresent() {
        return getUserDetails() != null  ;
    }

    public List<Model> getCategoryModelList() {
        return mCategoryModelList;
    }

    public void setCategoryModelList(List<Model> mCategoryModelList) {
        this.mCategoryModelList = mCategoryModelList;
    }
    public void addToCategoryModelList(Model model) {
        if (mCategoryModelList == null) {
            mCategoryModelList = new ArrayList<Model>();
        }
        mCategoryModelList.add(model);
    }

    public void addToSubCategoryMap (Model model) {
        if (mSubCategoryMap == null)
            mSubCategoryMap = new HashMap<String, Model>();
        mSubCategoryMap.put(model.getId(), model);
    }

    public Model getSubCategoryModel (String id) {
        return mSubCategoryMap.get(id);
    }

    public Boolean isNeedToLoadCategoryItems(String category) {
        if (mLoadedCategoryList == null) {
            return true;
        }
        return !mLoadedCategoryList.contains(category);
    }

    public void setCategoryLoaded(String value) {
        if (mLoadedCategoryList == null) {
            mLoadedCategoryList = new ArrayList<>();
        }
            mLoadedCategoryList.add(value);
    }

    public static void exit() {
        mSharedPreferenceUtil = null;
    }

    public void clearCategoryList() {
        if (mCategoryModelList != null)
            mCategoryModelList.clear();
        if (mSubCategoryMap != null)
            mSubCategoryMap.clear();
        if (mLoadedCategoryList != null)
            mLoadedCategoryList.clear();
    }
}
