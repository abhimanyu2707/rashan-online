package com.laymanizer.rashan.Model;

/**
 * Created by Abhimanyu on 09-09-2015.
 */
public class CityModel implements Model{
    private String mCityId;
    private String mCityName;

    public String getmCityId() {
        return mCityId;
    }

    public void setmCityId(String mCityId) {
        this.mCityId = mCityId;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String mCityName) {
        this.mCityName = mCityName;
    }

    @Override
    public String getId() {
        return mCityId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mCityName;
    }

}
