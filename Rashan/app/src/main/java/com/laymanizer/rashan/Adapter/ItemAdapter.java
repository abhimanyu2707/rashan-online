package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.laymanizer.rashan.Activity.CategoryItemActivity;
import com.laymanizer.rashan.Activity.ItemVariantActivity;
import com.laymanizer.rashan.Activity.SearchActivity;
import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.ItemModel;
import com.laymanizer.rashan.Model.ItemVariantModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Volley.ImageRequestQueue;

import java.util.List;

/**
 * Created by Abhimanyu on 16-10-2015.
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<Model> mStoreItemList;
    CartManager mCartManager;
    RecyclerView mRecyclerView;

    public ItemAdapter(Context context, RecyclerView recyclerView) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mRecyclerView = recyclerView;
        mCartManager = CartManager.getInstance(mContext);
    }

    public void setDataList(List<Model> list) {
        mStoreItemList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.common_card_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        viewHolder.mPlusButton.setOnClickListener(mOnPlusButtonClickListener);
        viewHolder.mMinusButton.setOnClickListener(mOnMinusButtonClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemModel itemModel = (ItemModel)mStoreItemList.get(position);
        ItemVariantModel itemVariantModel = (ItemVariantModel)itemModel.getVariantsList().get(0);

        String itemImageUri = itemVariantModel.getItemImageUri();
        String itemName = itemVariantModel.getName();
        String regionalName = itemVariantModel.getRegionalName();
        if(regionalName != null && regionalName.length()>0)
            itemName += " (" + regionalName + ")";
        String itemQuantityPU = itemVariantModel.getQuantityPerUnit();
        String itemPrice = itemVariantModel.getPrice();
        String itemMRP = itemVariantModel.getMRP();
        String itemCount = ""+mCartManager.getCount(itemVariantModel);
        String storeName = itemVariantModel.getStoreName();

        //Log.v("abhi1", "itemName : " + itemName + " itemImageUri : " + itemImageUri);
        ImageLoader imageLoader = ImageRequestQueue.getInstance(mContext).getImageLoader();
        if(!itemImageUri.equals(null) && itemImageUri.length()>0)
            holder.mItemImage.setImageUrl(itemImageUri, imageLoader);
        holder.mItemImage.setDefaultImageResId(R.drawable.default_image);
        holder.mItemName.setText(itemName);
        holder.mItemQuantityPU.setText(itemQuantityPU);
        if(itemModel.getVariantsList().size()>1) {
            holder.mMoreItemLayout.setVisibility(View.VISIBLE);
            holder.mCardView.setOnClickListener(mOnItemClickListener);
            holder.mCardView.setClickable(true);
        } else {
            holder.mMoreItemLayout.setVisibility(View.GONE);
            holder.mCardView.setClickable(false);
        }
        holder.mItemPrice.setText(mContext.getResources().getString(R.string.Rs)+itemPrice);
        if(itemMRP != itemPrice && Integer.parseInt(itemMRP) != 0) {
            holder.mItemMRP.setText(mContext.getResources().getString(R.string.Rs)+itemMRP);
            holder.mItemMRP.setVisibility(View.VISIBLE);
            holder.mItemMRP.setPaintFlags(holder.mItemMRP.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        holder.mItemCount.setText(itemCount);
        holder.mStoreName.setText(storeName);
        holder.mItemModel = itemModel;
        holder.mItemVariantModel = itemVariantModel;
    }

    @Override
    public int getItemCount() {
        if (mStoreItemList != null) {
            return mStoreItemList.size();
        }
        return 0;
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        NetworkImageView mItemImage;
        TextView mItemName;
        TextView mItemQuantityPU;
        LinearLayout mMoreItemLayout;
        TextView mItemPrice;
        TextView mItemMRP;
        TextView mMinusButton;
        TextView mItemCount;
        TextView mPlusButton;
        TextView mStoreName;

        ItemModel mItemModel;
        ItemVariantModel mItemVariantModel;
        View mCardView;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemImage = (NetworkImageView)itemView.findViewById(R.id.item_image);
            mItemName = (TextView)itemView.findViewById(R.id.item_name);
            mItemQuantityPU = (TextView)itemView.findViewById(R.id.item_quantity_pu);
            mMoreItemLayout = (LinearLayout)itemView.findViewById(R.id.more_items_layout);
            mItemPrice = (TextView)itemView.findViewById(R.id.item_price);
            mItemMRP = (TextView)itemView.findViewById(R.id.item_mrp);
            mMinusButton = (TextView)itemView.findViewById(R.id.minus_button);
            mItemCount = (TextView)itemView.findViewById(R.id.item_count);
            mPlusButton = (TextView)itemView.findViewById(R.id.plus_button);
            mPlusButton.setTag(this);
            mMinusButton.setTag(this);
            mStoreName = (TextView)itemView.findViewById(R.id.store_name);
            mCardView = itemView;
        }
    }

    View.OnClickListener mOnMinusButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateCount(view,true);

        }
    };

    View.OnClickListener mOnPlusButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateCount(view,false);
        }
    };


    View.OnClickListener mOnItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ItemVariantActivity.class);
            int pos = mRecyclerView.getChildAdapterPosition(v);
            // intent.putExtra("category",mSharedPreferenceUtil.getCategoryModelList().get(pos).getId());
            ItemModel itemModel = (ItemModel)mStoreItemList.get(pos);
            intent.putExtra("item_id", itemModel.getId());
            intent.putExtra("subcategory_id", itemModel.getSubCategoryId());
            mContext.startActivity(intent);
            //((Activity)mContext).overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        }
    };

    public void updateCount(View view, boolean isMinus) {
        ViewHolder viewHolder = (ViewHolder)view.getTag();
        Model variantModel = viewHolder.mItemVariantModel;
        if (isMinus) {
            mCartManager.removeFromCart(/*(ItemModel) itemModel, */(ItemVariantModel) variantModel);
        } else {
            mCartManager.addToCart(/*(ItemModel) itemModel, */(ItemVariantModel) variantModel);
        }
        viewHolder.mItemCount.setText(""+mCartManager.getCount((ItemVariantModel)variantModel));
        if (mContext != null && mContext instanceof SearchActivity) {
            ((SearchActivity) mContext).getTotalPriceTextView().setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getTotalPrice());
            ((SearchActivity) mContext).getTotalCountTextView().setText("" + mCartManager.getTotalCount());
            if(mCartManager.getTotalCount() <= 0)
                ((SearchActivity)mContext).getCartLayout().setVisibility(View.GONE);
            else
                ((SearchActivity)mContext).getCartLayout().setVisibility(View.VISIBLE);
        }else if (mContext != null && mContext instanceof CategoryItemActivity) {
            ((CategoryItemActivity)mContext).getTotalPriceTextView().setText(mContext.getResources().getString(R.string.Rs) + mCartManager.getTotalPrice());
            ((CategoryItemActivity)mContext).getTotalCountTextView().setText("" + mCartManager.getTotalCount());
            if(mCartManager.getTotalCount() <= 0)
                    ((CategoryItemActivity)mContext).getCartLayout().setVisibility(View.GONE);
            else
                ((CategoryItemActivity)mContext).getCartLayout().setVisibility(View.VISIBLE);
        }
    }
}
