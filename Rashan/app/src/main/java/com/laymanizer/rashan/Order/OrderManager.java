package com.laymanizer.rashan.Order;

import android.content.Context;

import com.laymanizer.rashan.Model.Model;

import java.util.List;

/**
 * Created by Abhimanyu on 03-01-2016.
 */
public class OrderManager {
    private static OrderManager mOrderManager = null;
    List<Model> mOrderList;
    Model mDetailedOrder;

    private OrderManager() {}

    public static OrderManager getInstance(Context context) {
        if(mOrderManager == null){
            mOrderManager = new OrderManager();
        }
        return mOrderManager;
    }

    public List<Model> getOrderList() {
        return mOrderList;
    }

    public void setOrderList(List<Model> mOrderList) {
        this.mOrderList = mOrderList;
    }

    public Model getDetailedOrder() {
        return mDetailedOrder;
    }

    public void setDetailedOrder(Model detailedOrder) {
        this.mDetailedOrder = detailedOrder;
    }
}
