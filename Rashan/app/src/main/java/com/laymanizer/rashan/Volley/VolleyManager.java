package com.laymanizer.rashan.Volley;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by tj on 6/8/15.
 */
public class VolleyManager {

    private static RequestQueue mRequestQueue;
    private static ImageLoader mImageLoader;

    private VolleyManager(Context context) {
        init(context);
    }

    private static void init(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);

        //int memClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        // Use 1/8th of the available memory for this memory cache.
        //int cacheSize = 1024 * 1024 * memClass / 8;
      //  mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
    }

    public static ImageLoader getImageLoader(Context context) {
        if (mImageLoader == null) {
      //      mImageLoader = new ImageLoader(getRequestQueue(context), new LruBitmapCache());

        }
        return mImageLoader;
    }

    private static RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {
            // throw new IllegalStateException("RequestQueue not initialized");
            init(context);

            return mRequestQueue;
        }
    }

    /*public static void addToQueue(NNacresRequest<?> nacresRequest) {
        getRequestQueue(nacresRequest.getContext()).add(nacresRequest);
    }*/



}
