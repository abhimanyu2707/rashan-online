package com.laymanizer.rashan.Model;

/**
 * Created by sayal on 13-09-2015.
 */
public class ItemVariantModel implements Model{

    String mItemName;
    String mRegionalName;
    String mUnitId;
    String mStoreId;
    String mStoreName;
    String mMRP;
    String mPrice;
    String mQuantity;
    String mMeasurement;
    String mIsInStock;
    String mImageUri;
    Integer mLimit;

    public ItemVariantModel(){}

    public ItemVariantModel(ItemVariantModel model) {
        if (model != null) {
            mUnitId = model.getUnitId();
            mItemName = model.getName();
            mStoreId = model.getStoreId();
            mStoreName = model.getStoreName();
            mPrice = model.getPrice();
            mMRP = model.getMRP();
            mIsInStock = model.getIsInStock();
            mMeasurement = model.getMeasureMent();
            mQuantity = model.getQuantity();
            mImageUri = model.getItemImageUri();
            mLimit = model.getLimit();
        }
    }


    @Override
    public String getId() {
        return mUnitId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mItemName;
    }

    public String getItemName() {
        return mItemName;
    }

    public void setItemName(String mItemName) {
        this.mItemName = mItemName;
    }

    public String getRegionalName() {
        return mRegionalName;
    }

    public void setRegionalName(String mRegionalName) {
        this.mRegionalName = mRegionalName;
    }

    public String getUnitId() {
        return mUnitId;
    }

    public void setUnitId(String mItemId) {
        this.mUnitId = mItemId;
    }

    public String getStoreId() {
        return mStoreId;
    }

    public void setStoreId(String mStoreId) {
        this.mStoreId = mStoreId;
    }

    public String getStoreName() {
        return mStoreName;
    }

    public void setStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public String getMRP() {
        return mMRP;
    }

    public void setMRP(String mrp) {
        this.mMRP = mrp;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String mQuantity) {
        this.mQuantity = mQuantity;
    }

    public String getMeasureMent() {
        return mMeasurement;
    }

    public void setMeasureMent(String mMeasureMent) {
        this.mMeasurement = mMeasureMent;
    }

    public String getIsInStock() {
        return mIsInStock;
    }

    public void setIsInStock(String mIsInStock) {
        this.mIsInStock = mIsInStock;
    }

    public String getQuantityPerUnit() {
        return mQuantity + " " + mMeasurement;
    }


    public String getItemImageUri() {
        return mImageUri;
    }

    public void setImageUri(String uri) {
        mImageUri = uri;
    }

    public Integer getLimit() {
        return mLimit;
    }

    public void setLimit(Integer mLimit) {
        this.mLimit = mLimit;
    }
}
