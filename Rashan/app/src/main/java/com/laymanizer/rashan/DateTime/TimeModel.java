package com.laymanizer.rashan.DateTime;

import com.laymanizer.rashan.Model.Model;

/**
 * Created by Abhimanyu on 24-11-2015.
 */
public class TimeModel implements Model {

    String mTime;
    @Override
    public String getId() {
        return null;
    }

    @Override
    public Model getItem() {
        return null;
    }

    @Override
    public String getName() {
        return mTime;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }
}
