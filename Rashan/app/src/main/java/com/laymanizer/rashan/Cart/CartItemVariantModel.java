package com.laymanizer.rashan.Cart;

import com.laymanizer.rashan.Model.ItemVariantModel;

/**
 * Created by sayal on 15-09-2015.
 */
public class CartItemVariantModel extends ItemVariantModel {

    int mCount;

    public CartItemVariantModel () {

    }

    public CartItemVariantModel(ItemVariantModel model) {
        super(model);
    }


    public float getTotalPrice() {
        float price = Float.parseFloat(getPrice());
        return (float)mCount*price;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public int getCount() {
        return mCount;
    }

    public int incrementCount(){
        mCount = mCount+1;
        return mCount;
    }

    public int decrementCount() {
        if (mCount > 0) {
            mCount = mCount - 1;
         }
        return mCount;
    }


}
