package com.laymanizer.rashan.Utils;

import com.laymanizer.rashan.Activity.*;

/**
 * Created by Laymanizer Pvt Ltd on 18-08-2015.
 */
public class Const {
    //URLs
    public static final String BASE_URL =  "http://www.laymanizer.com/dailyneeds";//"http://192.168.0.3/dailyneeds";//
    public static final String CITY_URL = "/fetchcities.php";
    public static final String LOCATION_URL = "/fetchlocations.php";
    public static final String CATEGORY_LIST_URL = "/fetchcategories.php";
    public static final String INIT_MODEL_URL = "/fetchinitdata.php";
    public static final String STORE_URL = "/fetchstores.php";
    public static final String STORE_ITEM_URL = "/fetchstoreitems.php";
    public static final String ADDRESS_ITEM_URL = "/fetchaddresses.php";
    public static final String CUSTOMER_MANAGE_URL = "/managecustomer.php";
    public static final String CHECKOUT_DATA_URL = "/fetchcheckoutdata.php";
    public static final String PLACE_ORDER_URL = "/placeorder.php";


    public static final Class LocationEntryActivity = LocationEntryActivity.class;
    public static final Class ConnectionErrorActivity = ConnectionErrorActivity.class;
    public static final Class HomeActivity = HomeActivity.class;
    public static final Class SplashActivity = SplashActivity.class;
    public static final Class SearchActivity = SearchActivity.class;
    public static final Class CartActivity = CartActivity.class;
    public static final Class AddressActivity = AddressActivity.class;
    public static final Class AddressManagerActivity = AddressManagerActivity.class;
    public static final Class LoginActivity = LoginActivity.class;
    public static final Class CheckoutActivity = CheckoutActivity.class;
    public static final Class PaymentActivity = PaymentActivity.class;
    public static final Class OrderActivity = OrderActivity.class;
    public static final Class OrderDetailActivity = OrderDetailActivity.class;
    public static final Class OrderConfirmActivity = OrderConfirmActivity.class;


    public static final String LaunchFrom = "launch_from";
    public static final String SplashActivityName = "SplashActivity";
    public static final String LoginActivityName = "LoginActivity";
    public static final String CheckoutActivityName = "CheckoutActivity";
    public static final String PaymentActivityName = "PaymentActivity";

    public static final int NO_ERROR = 0x0000;
    public static final int ERROR_NO_CONNECTION = 0x0001;
    public static final int ERROR_TIME_OUT      = 0x0002;
    public static final int ERROR_IN_ORDER      = 0x0004;


    //for querying server
    public static final int CITY_MODEL_LIST_TYPE = 0x0001;
    public static final int LOCATION_MODEL_LIST_TYPE = 0x0002;
    public static final int STORE_ITEM_MODEL_LIST_TYPE = 0x0004;
    public static final int ADDRESS_MODEL_LIST_TYPE = 0x0008;
    public static final int CATEGORY_MODEL_LIST_TYPE = 0x0010;
    public static final int SEARCH_ITEM_MODEL_LIST_TYPE = 0x0020;
    public static final int CART_ITEM_MODEL_LIST_TYPE = 0x0040;
    public static final int ADDRESS_AND_TIME_MODEL_LIST_TYPE = 0x0080;
    public static final int ORDER_MODEL_LIST_TYPE = 0x0100;
    public static final int ALL_MODEL_LIST_TYPE = 0x0FFF;

    public static final int USER_ITEM_MODEL_TYPE = 0x1000;
    public static final int ORDER_ADD_MODEL_TYPE = 0x2000;
    public static final int INIT_MODEL_TYPE = 0x4000;
    public static final int ALL_MODEL_TYPE = 0xF000;


/*
    public static final int[] Category = new int[] {
            R.drawable.fruits_and_vegetables,
            R.drawable.groceries,
            R.drawable.baby_care,
            R.drawable.pet_care
    };

    public static final String[] CategoryNames = new String[]{
            "Veg & fruits",
            "groceries",
                "baby care"," pet care"
    };*/


}
