package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.laymanizer.rashan.Fragments.AddressItemFragment;

/**
 * Created by Abhimanyu on 22-09-2015.
 */
public class AddressPagerAdapter extends FragmentPagerAdapter {

    private enum fragmentpos {
        HOME_FRAGMENT_POSITION,
        OFFICE_FRAGMENT_POSITION,
        OTHERS_FRAGMENT_POSITION
    }

    Context mContext;
    public AddressPagerAdapter(FragmentManager fm, Context cntxt) {
        super(fm);
        mContext = cntxt;
    }

    @Override
    public Fragment getItem(int position) {
        return new AddressItemFragment();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position) {
            case 0:
                return "Home";
            case 1:
                return "Office";
            case 2:
                return "Others";
        }
        return null;
    }
}