package com.laymanizer.rashan.View;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.laymanizer.rashan.Adapter.CustomArrayAdapter;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.QueryHelper.*;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Abhimanyu on 09-09-2015.
 */
public class CustomDialog implements IQueryHelperCallback{

    final String TAG = "CustomDialog";
    SharedPreferenceUtil mSharedPreferenceUtil;
    ListView mModelListView;
    CustomArrayAdapter mAdapter;
    QueryHelper mQueryHelper;
    int mModelType;
    AlertDialog mModelDialog;
    Context mContext;
    List<Model> mModelList;
    LayoutInflater mInflater;
    LinearLayout mRootLayout;
    EditText mEditText;
    ImageView mCancelButton;
    ImageView mBackButton;
    Handler mHandler;
    LinearLayout mLoadingLayout;

    String mSearchKey;

    OnModelSelectedListener mOnModelSelectedListener= null;


    final int DISMISS_DIALOG = 100;

    public CustomDialog(Context cntxt) {
        mContext = cntxt;
        mQueryHelper = new QueryHelper(mContext);
        mAdapter = new CustomArrayAdapter(mContext);
        mInflater = LayoutInflater.from(mContext);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        initViews();
        setHandler();

    }


    public void setListType(int listType) {
        mModelType = listType;
    }

    public void initViews() {
        mRootLayout = (LinearLayout)mInflater.inflate(R.layout.dialog_view, null);
        mModelListView = (ListView) mRootLayout.findViewById(R.id.list_dialog);
        mEditText = (EditText)mRootLayout.findViewById(R.id.dialog_search);
        mCancelButton = (ImageView)mRootLayout.findViewById(R.id.dialog_clear_text);
        mBackButton = (ImageView)mRootLayout.findViewById(R.id.dialog_back_button);
        mLoadingLayout = (LinearLayout)mRootLayout.findViewById(R.id.loading_layout);
        mModelListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RadioButton button = (RadioButton) view.findViewById(R.id.dialog_radio_button);
                button.setChecked(true);
                if (mOnModelSelectedListener != null && mModelList != null) {
                    mOnModelSelectedListener.onModelSelected(mModelList.get(position));
                }
                mHandler.sendEmptyMessageDelayed(DISMISS_DIALOG, 200);
            }
        });
        mModelListView.setVisibility(View.GONE);
        //mEditText.setOnClickListener(mOnEditTextClickListener);
        mEditText.addTextChangedListener(mTextWatcher);
        mEditText.setOnKeyListener(null);
        mLoadingLayout.setVisibility(View.VISIBLE);
        mBackButton.setOnClickListener(mOnBackButtonClickListener);
        mCancelButton.setOnClickListener(mOnClearTextClickListener);
    }

/*    View.OnClickListener mOnEditTextClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mEditText.setHint("");
        }
    };*/


    View.OnClickListener mOnBackButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismissDialog();
        }
    };


    View.OnClickListener mOnClearTextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mEditText.setText("");
            mSearchKey= "";
        }
    };


    TextWatcher	mTextWatcher = 	new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mSearchKey = mEditText.getText().toString();
            mAdapter.getFilter().filter(mSearchKey);
            //setModelList();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void showDialog(String cityId) {
        if (mModelDialog == null) {
            createDialog();
        }
        mModelDialog.show();     //set Layout Params after dailog is shown
        WindowManager.LayoutParams lp = mModelDialog.getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        mModelDialog.getWindow().setAttributes(lp);
        LinearLayout.LayoutParams progressParams = (LinearLayout.LayoutParams) mLoadingLayout.getLayoutParams();
        progressParams.gravity = Gravity.CENTER;
        mLoadingLayout.setLayoutParams(progressParams);
        setModelList(cityId);
    }

    public void setModelList(String cityId) {
        Map<String, String> queryParams= new HashMap<String, String>();
        if (mModelType == Const.CITY_MODEL_LIST_TYPE) {
            queryParams.put("get_list", "1");
            mQueryHelper.getList(mModelType, queryParams);
        } else {
            Log.i(TAG, "city: " + cityId);
            queryParams.put("get_list", "1");
            queryParams.put("city_id", cityId);
            mQueryHelper.getList(mModelType, queryParams);
        }
    }

    public void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(mRootLayout);
        mModelDialog = builder.create();
    }

    public void dismissDialog() {
        if (mModelDialog != null && mModelDialog.isShowing()) {
            mQueryHelper.cancelPreviousRequest();
            mModelDialog.dismiss();
            mModelDialog = null;
        }
    }

    public void setHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case DISMISS_DIALOG :
                        dismissDialog();
                        break;

                }

            }
        };
    }


    public void setOnModelSelectedListener(OnModelSelectedListener listener) {
        mOnModelSelectedListener = listener;
    }

    public interface OnModelSelectedListener {
         void onModelSelected(Model model);
    }

    @Override
    public void copyModelList(List<Model> modelList) {
        mModelList = modelList;
        mAdapter.setDataList(mModelList);
        mLoadingLayout.setVisibility(View.GONE);
        mModelListView.setVisibility(View.VISIBLE);
        mModelListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {
        switch (errorType) {
            case Const.ERROR_NO_CONNECTION :
                 mLoadingLayout.setVisibility(View.GONE);
                 //need to add empty layout since no cities
                 Toast.makeText(mContext,"need to add empty layout since no cities",Toast.LENGTH_SHORT).show();
                dismissDialog();
                break;
        }


    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }

    public void updateModelList(List<Model> listModel) {
        mModelList = listModel;
    }
}
