package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laymanizer.rashan.Adapter.AddressAdapter;
import com.laymanizer.rashan.Address.AddressManager;
import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddressActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback{
    Context mContext;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    AddressAdapter mAddressAdapter;

    View mRootLayout;
    LinearLayout mAddAddress;
    TextView mCurrentLocation;
    LinearLayout mLayoutProgress;
    LayoutInflater mInflater;

    List<Model> mAddressList;
    QueryHelper mQueryHelper;
    AddressManager mAddressManager;
    SharedPreferenceUtil mSharedPreferenceUtil;
    //Boolean mFromCheckOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("abhi", "create addressactivity");
        super.onCreate(savedInstanceState);
        //mFromCheckOut = false;
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initViews() {
        mAddressManager = AddressManager.getInstance(mContext);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mInflater = LayoutInflater.from(mContext);
        mRootLayout = mInflater.inflate(R.layout.address_activity, null);

        mLayoutProgress = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);
        mAddAddress = (LinearLayout)mRootLayout.findViewById(R.id.layout_add_address);
        mAddAddress.setOnClickListener(mOnAddAddressClickListener);
        mCurrentLocation = (TextView)mRootLayout.findViewById(R.id.address_location_name);
        mCurrentLocation.setText(mSharedPreferenceUtil.getCurrentLocation().getName() + ", " + mSharedPreferenceUtil.getCurrentCity().getName());

        mRecyclerView = (RecyclerView) mRootLayout.findViewById(R.id.item_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setVisibility(View.GONE);

        mLayoutProgress = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);
        mLayoutProgress.setVisibility(View.VISIBLE);

        mAddressAdapter = new AddressAdapter(mContext);
        if (Const.CheckoutActivityName.equals(getIntent().getStringExtra(Const.LaunchFrom))) {
            mAddressAdapter.setActions(true, true, false);
        } else {
            mAddressAdapter.setActions(false, true, true);
        }
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_list", "1");
        queryParams.put("user_id", mSharedPreferenceUtil.getCurrentUser().getId());
        queryParams.put("user_password", mSharedPreferenceUtil.getCurrentUser().getPassword());
        if (Const.CheckoutActivityName.equals(getIntent().getStringExtra(Const.LaunchFrom)) && mSharedPreferenceUtil.getCurrentLocation() != null)
            queryParams.put("location_id", mSharedPreferenceUtil.getCurrentLocation().getmLocationId());
        mQueryHelper.getList(Const.ADDRESS_MODEL_LIST_TYPE, queryParams);
    }

    View.OnClickListener mOnAddAddressClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            launchActivity(Const.AddressManagerActivity);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        mAddressList = mAddressManager.getAddressModelList();
        mAddressAdapter.setDataList(mAddressList);
        mRecyclerView.setAdapter(mAddressAdapter);
        mAddressAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }

    //override backbutton in actionbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                exitActivity();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        exitActivity();
    }

    private void exitActivity(){
        finish();
    }

    @Override
    public void copyModelList(List<Model> modelList) {
        mAddressManager.setAddressModelList(modelList);
        mAddressList = modelList;
        mLayoutProgress.setVisibility(View.GONE);
        mAddressAdapter.setDataList(mAddressList);
        mRecyclerView.setAdapter(mAddressAdapter);
        mAddressAdapter.notifyDataSetChanged();
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {

    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }

    public void launchActivity(Class targetClass) {
        Intent i = new Intent(mContext, targetClass);
        i.putExtra("EditAddress", false);
        mContext.startActivity(i);
    }

    public void selectAddress(String tag) {
        Intent returnIntent = new Intent();
        AddressModel addressModel = (AddressModel)mAddressList.get(Integer.parseInt(tag));
        mSharedPreferenceUtil.setCurrentDeliveryAddressId(addressModel.getAddressId());
        mAddressManager.setDeliveryAddress(addressModel);
        setResult(AppCompatActivity.RESULT_OK, returnIntent);
        finish();
    }

    public void deleteAddress(String tag) {
        AddressModel addressModel = (AddressModel)mAddressList.get(Integer.parseInt(tag));
        mLayoutProgress.setVisibility(View.VISIBLE);
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_list", "1");
        queryParams.put("delete", "1");
        queryParams.put("user_id", mSharedPreferenceUtil.getCurrentUser().getId());
        queryParams.put("user_password", mSharedPreferenceUtil.getCurrentUser().getPassword());
        queryParams.put("address_id", addressModel.getId());
        mQueryHelper.getList(Const.ADDRESS_MODEL_LIST_TYPE, queryParams);
    }
}
