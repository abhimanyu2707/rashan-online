package com.laymanizer.rashan.Cart;

import android.content.Context;
import android.widget.Toast;

import com.laymanizer.rashan.DBUtils.MyCartDBManager;
import com.laymanizer.rashan.Model.ItemVariantModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sayal on 15-09-2015.
 */
public class CartManager implements QueryHelper.IQueryHelperCallback{

    private static CartManager mCartManager = null;
    List<Model> mCartStoreList = new ArrayList<Model>();
    //List<Model> mCartItemVariantList = new ArrayList<Model>();
    List<Model> mCartSortedList = new ArrayList<>();
    HashMap<String,CartStoreModel> mCartStoreHashMap= new HashMap<String,CartStoreModel>();
    HashMap<String,CartItemVariantModel> mCartItemVariantHashMap= new HashMap<String,CartItemVariantModel>();
    private static Context mContext;
    private static MyCartDBManager mMyCartDBManager;
    private static QueryHelper mQueryHelper;
    private static SharedPreferenceUtil mSharedPreferenceUtil;

    private static float mDeliveryCharges;
    private static float mMinimumOrderForFree;

    private CartManager(){
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mMyCartDBManager = new MyCartDBManager(mContext);
        mDeliveryCharges = 49;
        mMinimumOrderForFree = 150;
    }

    public static CartManager getInstance(Context context) {
        if (mCartManager == null) {
            mContext = context;
            mCartManager = new CartManager();
        }
        return mCartManager;
    }

    public float getMinimumOrderForFree() {
        return mMinimumOrderForFree;
    }

    public void setMinimumOrderForFree(float mFreeDeliveryAmount) {
        CartManager.mMinimumOrderForFree = mFreeDeliveryAmount;
    }

    public float getDeliveryCharges() {
        return mDeliveryCharges;
    }

    public void setDeliveryCharges(float mDeliveryCharges) {
        CartManager.mDeliveryCharges = mDeliveryCharges;
    }

    public float getTotalPrice() {
        float price = 0.0f;
        if (mCartStoreList != null && mCartStoreList.size() > 0) {
            for (Model cartStoreModel : mCartStoreList) {
                List<Model> cartItemVariantList = ((CartStoreModel)cartStoreModel).getStoreItems();
                if(cartItemVariantList != null && cartItemVariantList.size() > 0) {
                    for (Model itemVariantModel : cartItemVariantList) {
                        price += ((CartItemVariantModel) itemVariantModel).getTotalPrice();
                    }
                }
            }
        }
        return price;
    }

    public float getPayableAmount() {
        float amount = 0.0f;
        if (mCartStoreList != null && mCartStoreList.size() > 0) {
            for (Model cartStoreModel : mCartStoreList) {
                List<Model> cartItemVariantList = ((CartStoreModel)cartStoreModel).getStoreItems();
                if(cartItemVariantList != null && cartItemVariantList.size() > 0) {
                    for (Model itemVariantModel : cartItemVariantList) {
                        amount += ((CartItemVariantModel) itemVariantModel).getTotalPrice();
                    }
                }
            }
        }
        if(amount < mMinimumOrderForFree)
            amount = amount + mDeliveryCharges;
        return amount;
    }

    public int getTotalCount() {
        int count = 0;
        if (mCartStoreList != null && mCartStoreList.size() > 0) {
            for (Model cartStoreModel : mCartStoreList) {
                List<Model> cartItemVariantList = ((CartStoreModel)cartStoreModel).getStoreItems();
                if(cartItemVariantList != null && cartItemVariantList.size() > 0) {
                    for (Model itemVariantModel : cartItemVariantList) {
                        count += ((CartItemVariantModel) itemVariantModel).getCount();
                    }
                }
            }
        }
        return count;
    }
/* old
    public float getTotalPrice() {
        float price = 0.0f;
        if (mCartItemVariantList != null && mCartItemVariantList.size() > 0) {
            for (Model itemVariantModel : mCartItemVariantList) {
                price += (float) ((CartItemVariantModel) itemVariantModel).getTotalPrice();
            }
        }
        return price;
    }

    public float getPayableAmount() {
        float amount = 0.0f;
        if (mCartItemVariantList != null && mCartItemVariantList.size() > 0) {
            for (Model itemVariantModel : mCartItemVariantList) {
                amount += (float) ((CartItemVariantModel) itemVariantModel).getTotalPrice();
            }
        }
        if(amount < mMinimumOrderForFree)
            amount = amount + mDeliveryCharges;
        return amount;
    }
    public int getTotalCount() {
        int count = 0;
        if (mCartItemVariantList != null && mCartItemVariantList.size() > 0) {
            for (Model itemVariantModel : mCartItemVariantList) {
                count += ((CartItemVariantModel) itemVariantModel).getCount();
            }
        }
        return count;
    }
*/
    public float getStoreItemPrice(String storeId) {
        float price = 0.0f;
        CartStoreModel cartStoreModel = mCartStoreHashMap.get(storeId);
        if(cartStoreModel != null) {
            List<Model> storeList = cartStoreModel.getStoreItems();
            if(storeList != null && storeList.size() > 0) {
                for(Model itemVariantModel : storeList) {
                    price += ((CartItemVariantModel) itemVariantModel).getTotalPrice();
                }
            }
        }
        return price;
    }

    public void loadPreviousItems() {
        String ids = mMyCartDBManager.getIds();
        if(ids != null && !ids.isEmpty()) {
            Map<String, String> queryParams= new HashMap<String, String>();
            queryParams.put("get_list", "1");
            queryParams.put("location_id", mSharedPreferenceUtil.getCurrentLocation().getmLocationId());
            queryParams.put("unit_ids", ids);
            mQueryHelper.getList(Const.CART_ITEM_MODEL_LIST_TYPE, queryParams);
        }
    }

    //[add for store
    private void addToStoreList(Model model) {
        CartItemVariantModel cartItemVariantModel = (CartItemVariantModel) model;
        if(cartItemVariantModel != null) {
            CartStoreModel cartStoreModel = mCartStoreHashMap.get(cartItemVariantModel.getStoreId());
            if(cartStoreModel == null) {
                cartStoreModel = new CartStoreModel();
                cartStoreModel.setStoreId(cartItemVariantModel.getStoreId());
                cartStoreModel.setStoreName(cartItemVariantModel.getStoreName());
                mCartStoreHashMap.put(cartItemVariantModel.getStoreId(), cartStoreModel);
                mCartStoreList.add(cartStoreModel);
            }
            cartStoreModel.addToStore(cartItemVariantModel);
        }
    }

    private void removeFromStoreList(Model model) {
        CartItemVariantModel cartItemVariantModel = (CartItemVariantModel) model;
        CartStoreModel cartStoreModel = mCartStoreHashMap.get(cartItemVariantModel.getStoreId());
        cartStoreModel.removeFromStore(cartItemVariantModel);
        if(cartStoreModel.isStoreEmpty()) {
            mCartStoreList.remove(cartStoreModel);
            mCartStoreHashMap.remove(cartItemVariantModel.getStoreId());
        }
    }

    //add for store]


    public void addToCart(ItemVariantModel itemVariantModel) {
        if (itemVariantModel != null) {
            CartItemVariantModel cartItemVariantModel = mCartItemVariantHashMap.get(itemVariantModel.getId());
            if (cartItemVariantModel == null) {
                cartItemVariantModel = new CartItemVariantModel(itemVariantModel);
                mMyCartDBManager.createCartItem(Long.parseLong(itemVariantModel.getId()), 0);
                mCartItemVariantHashMap.put(itemVariantModel.getId(), cartItemVariantModel);
                //mCartItemVariantList.add(cartItemVariantModel);
                addToStoreList(cartItemVariantModel);
            }
            if(cartItemVariantModel.getCount() < cartItemVariantModel.getLimit()) {
                cartItemVariantModel.incrementCount();
                mMyCartDBManager.updateCartItem(Long.parseLong(itemVariantModel.getId()), cartItemVariantModel.getCount());
            } else {
                Toast.makeText(mContext, "Sorry, you can't add more of these items", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void removeFromCart(ItemVariantModel itemVariantModel) {
        if (itemVariantModel != null) {
            CartItemVariantModel cartItemVariantModel = mCartItemVariantHashMap.get(itemVariantModel.getId());
            if (cartItemVariantModel == null) {
                return;
            }
            if (cartItemVariantModel.decrementCount() <= 0) {
                mMyCartDBManager.deleteCartItem(Long.parseLong(itemVariantModel.getId()));
                mCartItemVariantHashMap.remove(cartItemVariantModel.getId());
                //mCartItemVariantList.remove(cartItemVariantModel);
                removeFromStoreList(cartItemVariantModel);
            }
            mMyCartDBManager.updateCartItem(Long.parseLong(itemVariantModel.getId()), cartItemVariantModel.getCount());
        }
    }


    public int getCount(ItemVariantModel itemVariantModel) {
        if (itemVariantModel == null) {
            return 0;
        }
        CartItemVariantModel cartItemVariantModel = mCartItemVariantHashMap.get(itemVariantModel.getId());
        if (cartItemVariantModel == null) {
            return 0;
        }
        return cartItemVariantModel.getCount();
    }

    public List<Model> getCartItemVariantList() {
        //Log.e("jd", "getCartItemVariantList");
        mCartSortedList.clear();
        int i = 0;
        for (Model cartStoreModel : mCartStoreList) {
            //Log.e("jd", "" + i++);
            mCartSortedList.addAll(((CartStoreModel)cartStoreModel).getStoreItems());
        }
        //return mCartItemVariantList;
        return mCartSortedList;
    }

    public List<Model> getCartStoreList() {
        return mCartStoreList;
    }


    public void clearCart() {
        mMyCartDBManager.clearTable();
        mCartStoreList.clear();
        mCartStoreHashMap.clear();
        //mCartItemVariantList.clear();
        mCartItemVariantHashMap.clear();
    }
/**
 * loaded from splash activity
 * Todo what if this is not loaded due to network error
 */
    private void updateCartFromList(List<Model> cartItemVariantList){
        if (cartItemVariantList != null && cartItemVariantList.size() > 0) {
            HashMap <String, Integer> cartItemCountMap= mMyCartDBManager.getCartItemsCount();
            for (Model cartItemVariantModel : cartItemVariantList) {
                mCartItemVariantHashMap.put(cartItemVariantModel.getId(), (CartItemVariantModel)cartItemVariantModel);
                ((CartItemVariantModel) cartItemVariantModel).setCount(cartItemCountMap.get(cartItemVariantModel.getId()));
                addToStoreList(cartItemVariantModel); //concept change for store
            }
        } else {
            mMyCartDBManager.clearTable();
        }
    }

    @Override
    public void copyModelList(List<Model> modelList) {
        List<Model> cartItemVariantList = modelList;
        updateCartFromList(cartItemVariantList);
    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {

    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }
}
