package com.laymanizer.rashan.Cart;

import android.util.Log;

import com.laymanizer.rashan.Model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sayal on 15-09-2015.
 * to implement cart with different stores -- abhimanyu
 */
public class CartStoreModel implements Model {

    String mStoreId;
    String mStoreName;
    List<Model> mStoreItems = new ArrayList<>();

    @Override
    public String getId() {
        return mStoreId;
    }

    @Override
    public Model getItem() {
        return this;
    }

    @Override
    public String getName() {
        return mStoreName;
    }

    public String getStoreId() {
        return mStoreId;
    }

    public void setStoreId(String mStoreId) {
        this.mStoreId = mStoreId;
    }

    public String getStoreName() {
        return mStoreName;
    }

    public void setStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }

    public List<Model> getStoreItems() {
        return mStoreItems;
    }

    public void setStoreItems(List<Model> mStoreItems) {
        this.mStoreItems = mStoreItems;
    }

    public void addToStore(Model model) {
        mStoreItems.add(model);
    }

    public void removeFromStore(Model model) {
        Log.e("jd", "remove " + model.getName());
        mStoreItems.remove(model);
    }

    public boolean isStoreEmpty() {
        if(mStoreItems.size() <= 0)
            return true;
        return false;
    }


}
