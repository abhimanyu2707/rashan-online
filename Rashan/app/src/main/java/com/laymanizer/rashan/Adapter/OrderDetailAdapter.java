package com.laymanizer.rashan.Adapter;

import android.content.Context;
import android.graphics.AvoidXfermode;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.laymanizer.rashan.LayoutManager.CustomLinearLayoutManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.Order.OrderManager;
import com.laymanizer.rashan.Order.OrderStoreModel;
import com.laymanizer.rashan.R;

import java.util.List;

/**
 * Created by Abhimanyu on 24-01-2016.
 */
public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {
    Context mContext;
    List<Model> mOrderDetailList;
    OrderManager mOrderManager;

    public OrderDetailAdapter(Context context) {
        mContext = context;
        mOrderManager = OrderManager.getInstance(mContext);
    }


    public void setDataList(List<Model> list) {
        mOrderDetailList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_detail_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderStoreModel orderStoreModel = (OrderStoreModel) mOrderDetailList.get(position);
        holder.storeName.setText(orderStoreModel.getStoreName());
        holder.storeOrderId.setText("Order id: "+orderStoreModel.getId());
        String orderStatus = orderStoreModel.getStoreOrderStatus();
        if(orderStatus.equals("0")){
            holder.orderStatusText.setText("Order Processing");
            holder.orderStatusText.setTextColor(mContext.getResources().getColor(R.color.order_processing));
            holder.orderStatusImage.setBackgroundResource(R.drawable.ic_action_process_start);
        }else if(orderStatus.equals("-1")){
            holder.orderStatusText.setText("Order Canceled");
            holder.orderStatusText.setTextColor(mContext.getResources().getColor(R.color.order_canceled));
            holder.orderStatusImage.setBackgroundResource(R.drawable.ic_action_cancel);
        }else if(orderStatus.equals("1")){
            holder.orderStatusText.setText("Order Delivered");
            holder.orderStatusText.setTextColor(mContext.getResources().getColor(R.color.order_delivered));
            holder.orderStatusImage.setBackgroundResource(R.drawable.ic_action_tick);
        }

        OrderDetailChildAdapter orderDetailChildAdapter = new OrderDetailChildAdapter(mContext);
        orderDetailChildAdapter.setDataList(orderStoreModel.getStoreItemList());
        holder.orderDetailChildRecyclerView.setAdapter(orderDetailChildAdapter);
        orderDetailChildAdapter.notifyDataSetChanged();

        holder.storeSubTotal.setText(mContext.getResources().getString(R.string.Rs) + orderStoreModel.getStoreOrderAmount());
    }

    @Override
    public int getItemCount() {
        if(mOrderDetailList!=null)
            return mOrderDetailList.size();
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        //private View itemView;
        private TextView storeName;
        private TextView storeOrderId;
        private ImageView orderStatusImage;
        private TextView orderStatusText;
        private TextView storeSubTotal;
        private RecyclerView orderDetailChildRecyclerView;

        public ViewHolder(View itemView) {
            super(itemView);
            //this.itemView = itemView;
            storeName        = (TextView)itemView.findViewById(R.id.text_store_name);
            storeOrderId     = (TextView)itemView.findViewById(R.id.store_order_id);
            orderStatusImage = (ImageView)itemView.findViewById(R.id.image_order_status);
            orderStatusText  = (TextView)itemView.findViewById(R.id.text_order_status);
            storeSubTotal      = (TextView)itemView.findViewById(R.id.store_sub_total);
            orderDetailChildRecyclerView = (RecyclerView)itemView.findViewById(R.id.list_store_items);
            LinearLayoutManager layoutManager = new CustomLinearLayoutManager(mContext);/*, LinearLayoutManager.VERTICAL, false);*/
            orderDetailChildRecyclerView.setHasFixedSize(true);
            orderDetailChildRecyclerView.setLayoutManager(layoutManager);
            orderDetailChildRecyclerView.setNestedScrollingEnabled(false);
        }
    }
}
