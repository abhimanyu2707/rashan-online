package com.laymanizer.rashan.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.laymanizer.rashan.Address.AddressManager;
import com.laymanizer.rashan.Address.AddressModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Abhimanyu on 22-09-2015.
 */
public class AddressManagerActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback {

    Context mContext;
    View mRootLayout;
    LinearLayout mLayoutProgress;
    RadioGroup mLabelRadioGroup;
    RadioButton mHome;
    RadioButton mOffice;
    RadioButton mOther;
    TextInputLayout mLabelLayout;
    EditText mLabel;
    EditText mName;
    EditText mAddress1;
    EditText mAddress2;
    EditText mLocation;
    TextInputLayout mLocationLayout;
    boolean mIsForUpdate = false;
    AddressManager mAddressManager;
    AddressModel mEditableAddress;

    SharedPreferenceUtil mSharedPreferenceUtil;
    QueryHelper mQueryHelper;
    String mLocationId;
    String mLocationName;
    Boolean mFromCheckOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFromCheckOut = false;
        mContext = this;
        initViews();
        setContentView(mRootLayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(mIsForUpdate)
            getSupportActionBar().setTitle("Update Address");
        else
            getSupportActionBar().setTitle("Add Address");
    }

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }

    public void initViews() {
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        mRootLayout = LayoutInflater.from(mContext).inflate(R.layout.address_manager_activity, null);
        mLayoutProgress = (LinearLayout)mRootLayout.findViewById(R.id.layout_progress);
        mLabelRadioGroup = (RadioGroup)mRootLayout.findViewById(R.id.label_group);
        mLabelRadioGroup.setOnCheckedChangeListener(mOnLabelChangeListener);
        mHome = (RadioButton)mRootLayout.findViewById(R.id.label_home);
        mOffice = (RadioButton)mRootLayout.findViewById(R.id.label_office);
        mOther = (RadioButton)mRootLayout.findViewById(R.id.label_other);
        mLabelLayout = (TextInputLayout)mRootLayout.findViewById(R.id.input_layout_label);
        mLabel = (EditText)mRootLayout.findViewById(R.id.label_text);
        mName = (EditText)mRootLayout.findViewById(R.id.name_text);
        mAddress1 = (EditText)mRootLayout.findViewById(R.id.address1_text);
        mAddress2 = (EditText)mRootLayout.findViewById(R.id.address2_text);
        mLocationLayout = (TextInputLayout)mRootLayout.findViewById(R.id.input_layout_location);
        mLocation = (EditText)mRootLayout.findViewById(R.id.location_text);
        mLocation.setOnClickListener(mOnLocationClickListener);
        mLocation.setFocusable(false);
        mLocation.setClickable(true);
        mLocation.setKeyListener(null);
        mAddressManager = AddressManager.getInstance(mContext);
        if(getIntent().getExtras().getBoolean("EditAddress")) {
            mEditableAddress = (AddressModel) mAddressManager.getEditableAddress();
            mIsForUpdate = true;
            if(mEditableAddress != null) {
                mLocationId = mEditableAddress.getAddressId();
                mLocationName = mEditableAddress.getLocationName();
                fillEditableAddress();
            }
        } else {
            mIsForUpdate = false;
            mLocationId = mSharedPreferenceUtil.getCurrentLocation().getId();
            mLocationName = mSharedPreferenceUtil.getCurrentLocation().getName()+", "+mSharedPreferenceUtil.getCurrentCity().getName();
            mLocation.setText(mLocationName);
            mHome.setChecked(true);
        }
        if (Const.CheckoutActivityName.equals(getIntent().getStringExtra(Const.LaunchFrom))) {
            mFromCheckOut = true;
        }
    }


    View.OnClickListener mOnLocationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog dialog;
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            dialogBuilder.setTitle("Location can't be changed");
            dialogBuilder.setMessage("Your Location should be in " +mLocationName+ " as you are shopping here");
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            dialog = dialogBuilder.create();
            dialog.show();
        }
    };


    RadioGroup.OnCheckedChangeListener mOnLabelChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == R.id.label_other) {
                mLabelLayout.setVisibility(View.VISIBLE);
            } else {
                mLabel.setText("");
                mLabelLayout.setVisibility(View.GONE);
            }
        }
    };

    private void fillEditableAddress() {
        if(mEditableAddress.getAddressLabel().equals("Home")) {
            mHome.setChecked(true);
        } else if(mEditableAddress.getAddressLabel().equals("Office")) {
            mOffice.setChecked(true);
        } else {
            mOther.setChecked(true);
        }
        mName.setText(mEditableAddress.getAddressReceiver());
        mAddress1.setText(mEditableAddress.getAddress1());
        mAddress2.setText(mEditableAddress.getAddress2());
        mLocation.setText(mEditableAddress.getLocationName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.address_manager_menu, menu);
        MenuItem actionItem = menu.findItem(R.id.action_manage_address);
        if(mIsForUpdate) {
            actionItem.setTitle("Update");
        } else {
            actionItem.setTitle("Add");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                exitActivity();
                break;
            case R.id.action_manage_address :
                onActionManageAddressClickListener();
        }
        return true;
    }

    void onActionManageAddressClickListener() {
        RadioButton radioButton = (RadioButton)findViewById(mLabelRadioGroup.getCheckedRadioButtonId());
        String label = radioButton.getText().toString();
        if((radioButton.getId() == mOther.getId()) && (!mLabel.getText().toString().equals(""))) {
            label = mLabel.getText().toString();
        }
        String name = mName.getText().toString();
        String address1 = mAddress1.getText().toString();
        String address2 = mAddress2.getText().toString();
        if(name == null || name.isEmpty() || address1 == null || address1.isEmpty() || address2 == null || address2.isEmpty() || mLocationName == null || mLocationName.isEmpty()) {
            AlertDialog dialog;
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
            dialogBuilder.setTitle("Please fill all the information");
            dialogBuilder.setMessage("Please fill your full address for correct delivery of items");
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            dialog = dialogBuilder.create();
            dialog.show();
        } else {
            mLayoutProgress.setVisibility(View.VISIBLE);
            Map<String, String> queryParams= new HashMap<String, String>();
            queryParams.put("get_list", "1");
            queryParams.put("user_id", mSharedPreferenceUtil.getCurrentUser().getId());
            queryParams.put("user_password", mSharedPreferenceUtil.getCurrentUser().getPassword());
            queryParams.put("label", label);
            queryParams.put("receiver", name);
            queryParams.put("address1", address1);
            queryParams.put("address2", address2);
            queryParams.put("location_id", mLocationId);
            queryParams.put("location_name", mLocationName);
            if(mIsForUpdate) {
                queryParams.put("address_id", mEditableAddress.getId());
                queryParams.put("update", "1");
            }
            else
                queryParams.put("add", "1");
            mQueryHelper.getList(Const.ADDRESS_MODEL_LIST_TYPE, queryParams);
        }
    }

    @Override
    public void onBackPressed() {
        exitActivity();
    }

    private void exitActivity(){
        finish();
    }

    @Override
    public void copyModelList(List<Model> modelList) {
        mAddressManager.setAddressModelList(modelList);
        mLayoutProgress.setVisibility(View.GONE);
        if(mFromCheckOut && modelList != null && modelList.size()>0) {
            Intent returnIntent = new Intent();
            AddressModel addressModel = (AddressModel)modelList.get(0);
            mSharedPreferenceUtil.setCurrentDeliveryAddressId(addressModel.getAddressId());
            mAddressManager.setDeliveryAddress(addressModel);
            setResult(AppCompatActivity.RESULT_OK, returnIntent);
        }
        exitActivity();
    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {

    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }
}
