package com.laymanizer.rashan.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.laymanizer.rashan.Cart.CartManager;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.ConnectionUtil;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.QueryHelper.*;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;
import com.laymanizer.rashan.View.ErrorDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SplashActivity extends Activity implements IQueryHelperCallback,ErrorDialog.IErrorDialogListener{

    private final String TAG = "ErrorDialog";
    Context mContext;
    ConnectionUtil mConnectionUtil;
    SharedPreferenceUtil mSharedPreferenceUtil;
    QueryHelper mQueryHelper;
    ErrorDialog mErrorDialog;
    CartManager mCartManager;
    View mRootLayout;
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("Abhi", "SplashActivity onCreate");
        super.onCreate(savedInstanceState);
        mContext = this;
        mRootLayout = LayoutInflater.from(mContext).inflate(R.layout.splash_activity, null);
        setContentView(mRootLayout);
        mProgressBar = (ProgressBar)mRootLayout.findViewById(R.id.splash_progress);
        mProgressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        mConnectionUtil = ConnectionUtil.getInstance();
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(this);
        mSharedPreferenceUtil.clearCategoryList();
        mQueryHelper = new QueryHelper(this);
        mQueryHelper.setQueryHelperCallback(this);
        mErrorDialog = new ErrorDialog(this);
        mErrorDialog.setOnRetrySelectedListener(this);
        mCartManager = CartManager.getInstance(mContext);
        launchNextActivity();
    }

    private void launchNextActivity() {
        Log.i(TAG, "launchNextActivity");
        if (!mConnectionUtil.isConnectedToInternet(this)) {
            mErrorDialog.showDialog(Const.ERROR_NO_CONNECTION);
        } else if (!mSharedPreferenceUtil.isLocationPresent()) {
            launchActivity(Const.LocationEntryActivity);
        }else {
            loadInitData();
            //loadCategories();
        }
    }

    private void loadInitData(){
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_list", "1");
        queryParams.put("location_id", mSharedPreferenceUtil.getCurrentLocation().getmLocationId());
        mCartManager.loadPreviousItems();
        mQueryHelper.getModel(Const.INIT_MODEL_TYPE, queryParams);
    }

/*    private void loadCategories() {
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_list", "1");
        queryParams.put("location_id", mSharedPreferenceUtil.getCurrentLocation().getmLocationId());
        mCartManager.loadPreviousItems();
        mQueryHelper.getList(Const.CATEGORY_MODEL_LIST_TYPE, queryParams);
    }*/

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }

    private void launchActivity(Class activityName){
        Intent intent = new Intent(this,activityName);
        intent.putExtra(Const.LaunchFrom, Const.SplashActivityName);
       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void copyModelList(List<Model> modelList) {

    }

    @Override
    public void copyModel(Model model) {

    }

    @Override
    public void onQueryCompleted(int errorType) {
        if (errorType == Const.NO_ERROR) {
            launchActivity(Const.HomeActivity);
        } else {
            mErrorDialog.showDialog(errorType);
        }
    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }

    @Override
    public void onRetrySelected() {
        mErrorDialog.dismissDialog();
        launchNextActivity();
    }
}
