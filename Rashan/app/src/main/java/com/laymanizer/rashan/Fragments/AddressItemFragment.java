package com.laymanizer.rashan.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laymanizer.rashan.R;

/**
 * Created by Abhimanyu on 22-09-2015.
 */
public class AddressItemFragment extends Fragment{

    View mRootLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = initViews(inflater, container);
        return rootView;
    }

    public View initViews(LayoutInflater inflater, ViewGroup container) {
        mRootLayout = inflater.inflate(R.layout.address_manager_fragment, null);
        Log.i("Mannu", "called initView requested for list to server");
        return mRootLayout;
    }
}
