package com.laymanizer.rashan.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.laymanizer.rashan.Cognalys.Constants;
import com.laymanizer.rashan.Cognalys.VerifyMobile;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.Const;
import com.laymanizer.rashan.Utils.QueryHelper;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements QueryHelper.IQueryHelperCallback{

    View mRootLayout;
    EditText mCountryCode;
	EditText mMobileNumber;
	Button mSubmitButton;
    Context mContext;
    SharedPreferenceUtil mSharedPreferenceUtil;
    QueryHelper mQueryHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
        setContentView(mRootLayout);
	}
    public void initViews() {
        LayoutInflater layoutInflater;
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(this);
        mQueryHelper = new QueryHelper(mContext);
        mQueryHelper.setQueryHelperCallback(this);
        layoutInflater = LayoutInflater.from(mContext);
        mRootLayout = (View)layoutInflater.inflate(R.layout.login_activity, null);
        mCountryCode = (EditText) mRootLayout.findViewById(R.id.country_code);
        mMobileNumber = (EditText) mRootLayout.findViewById(R.id.mobile_number);
        mCountryCode.setText(VerifyMobile
                .getCountryCode(getApplicationContext()));
        mSubmitButton = (Button) mRootLayout.findViewById(R.id.login_submit);
        mSubmitButton.setOnClickListener(mSubmitLoginListener);
    }

    View.OnClickListener mSubmitLoginListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
              String mobile = mCountryCode.getText().toString()
                    + mMobileNumber.getText().toString();
            Intent in = new Intent(LoginActivity.this, VerifyMobile.class);
            Log.e("keelback", mSharedPreferenceUtil.getCognalysAppId() + " " + mSharedPreferenceUtil.getCognalysAppToken());
            in.putExtra("app_id", mSharedPreferenceUtil.getCognalysAppId());
            in.putExtra("access_token", mSharedPreferenceUtil.getCognalysAppToken());
            in.putExtra("mobile", mobile);
            if (mobile.length() == 0) {
                mCountryCode.setError("Please enter mobile number");
            }
            startActivityForResult(in,VerifyMobile.REQUEST_CODE);
            //registerUser();
        }
    };

//    private void profileActivity() {
//        BitmapFactory.Options options;
//        userimage = (ImageView) findViewById(R.id.userimage);
//        username1 = (TextView) findViewById(R.id.text1);
//        String mobile = mSharedPreferenceUtil.getUserDetails().getmMobileNumber().toString().substring(1);
//        File imageFile1 =new File(Environment.getExternalStorageDirectory()+"/WhatsApp/Profile Pictures/"+mobile+".jpg");
//        File imageFile2 =new File(Environment.getExternalStorageDirectory()+"/WhatsApp/Profile Pictures/"+mobile+".jpg");
//        Log.v("ankur","imageFile2= "+imageFile1);
//        if(imageFile1!=null) {
//            Bitmap bitmap1 = BitmapFactory.decodeFile(imageFile1.getAbsolutePath());
//            if(bitmap1 != null) {
//                userimage.setImageBitmap(bitmap1);
//            } else{
//            }
//        }
//    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
    // TODO Auto-generated method stub
        super.onActivityResult(arg0, arg1, arg2);
        if (arg1 !=  RESULT_CANCELED) {
            if (arg0 == VerifyMobile.REQUEST_CODE) {
                String message = arg2.getStringExtra("message");
                int result = arg2.getIntExtra("result", 0);
                String result1 = String.valueOf(result);
                if(message.equals(Constants.FOUR)) {
                    registerUser();
                }
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        mQueryHelper.cancelPreviousRequest();
        super.onDestroy();
    }

    private void registerUser() {
        String mobileNumber = mCountryCode.getText().toString()
                + mMobileNumber.getText().toString();
        Map<String, String> queryParams= new HashMap<String, String>();
        queryParams.put("get_model", "1");
        queryParams.put("mobile_number", mobileNumber);
        mQueryHelper.getModel(Const.USER_ITEM_MODEL_TYPE, queryParams);
    }

    @Override
    public void copyModelList(List<Model> modelList) {

    }

    @Override
    public void copyModel(Model model) {
        if (model != null) {
            mSharedPreferenceUtil.setUserDetails(model);
            finish();
            /*Intent i = new Intent(LoginActivity.this, HomeActivity.class);
            i.putExtra(Const.LaunchFrom, Const.LoginActivityName);
            startActivity(i);*/
        }
    }

    @Override
    public void onQueryCompleted(int errorType) {

    }

    @Override
    public void onQueryCompleted(int errorType, HashMap<String, String> args) {

    }

    @Override
    public void onQueryCompleted(int errorType, String errorMessage) {

    }
}

