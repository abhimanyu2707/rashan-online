package com.laymanizer.rashan.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.laymanizer.rashan.Activity.CategoryItemActivity;
import com.laymanizer.rashan.Model.CategoryModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.R;
import com.laymanizer.rashan.Utils.SharedPreferenceUtil;
import com.laymanizer.rashan.Volley.ImageRequestQueue;

import java.util.List;

/**
 * Created by Abhimanyu on 10-10-2015.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder>  {


    Context mContext;
    LayoutInflater mLayoutInflater;
    SharedPreferenceUtil mSharedPreferenceUtil;
    List<Model> mCategoryModelList;
    RecyclerView mRecyclerView;
    // need to use another way without obtaining object


    public HomeAdapter(Context context, RecyclerView view) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mSharedPreferenceUtil = SharedPreferenceUtil.getInstance(mContext);
        //mCategoryModelList = new ArrayList<Model>();
        mCategoryModelList = mSharedPreferenceUtil.getCategoryModelList();
        mRecyclerView = view;
    }

    public void setDataList(List<Model> categoryModelList) {
        mCategoryModelList = mSharedPreferenceUtil.getCategoryModelList();
    }

    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_category_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        itemView.setOnClickListener(mOnCategoryClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {

        String imageUrl = ((CategoryModel)mCategoryModelList.get(position)).getImageUrl();
        String name = mCategoryModelList.get(position).getName();
        String details = ((CategoryModel)mCategoryModelList.get(position)).getCategoryDetail();

        Log.e("Abhi1", "name = "+name +" imageUrl "+imageUrl);
        ImageLoader imageLoader = ImageRequestQueue.getInstance(mContext).getImageLoader();
        if(!imageUrl.equals(null) && imageUrl.length()>0)
            holder.categoryImage.setImageUrl(imageUrl, imageLoader);
        holder.categoryImage.setDefaultImageResId(R.drawable.default_image);
        holder.categoryName.setText(name);
        holder.categoryDetail.setText(details);
    }

    @Override
    public int getItemCount() {
        if(mCategoryModelList!=null)
           return mCategoryModelList.size();
        return 0;
    }

    View.OnClickListener mOnCategoryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, CategoryItemActivity.class);
            int pos = mRecyclerView.getChildAdapterPosition(v);
           // intent.putExtra("category",mSharedPreferenceUtil.getCategoryModelList().get(pos).getId());
            intent.putExtra("category",pos);
            mContext.startActivity(intent);
            ((Activity)mContext).overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        }
    };


    class ViewHolder extends RecyclerView.ViewHolder{

        public NetworkImageView categoryImage;
        public TextView categoryName;
        public TextView categoryDetail;

        public ViewHolder(View itemView) {
            super(itemView);
            categoryImage = (NetworkImageView)itemView.findViewById(R.id.category_image);
            categoryName = (TextView)itemView.findViewById(R.id.category_name);
            categoryDetail = (TextView)itemView.findViewById(R.id.category_detail);
        }
    }
}
