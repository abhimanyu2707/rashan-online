package com.laymanizer.rashan.View;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.laymanizer.rashan.DateTime.DateTimeModel;
import com.laymanizer.rashan.Model.Model;
import com.laymanizer.rashan.DateTime.TimeModel;
import com.laymanizer.rashan.R;
import java.util.List;

/**
 * Created by Abhimanyu on 25-11-2015.
 */
public class DateTimeDialog {
    final String TAG = "DateTimeDialog";
    final boolean TYPE_DATE = true;
    final boolean TYPE_TIME = false;

    Context mContext;
    AlertDialog mDialog;
    LayoutInflater mInflater;
    LinearLayout mRootLayout;
    NumberPicker mDatePicker;
    NumberPicker mTimePicker;
    TextView mCancel;
    TextView mSelect;

    List<Model> mDateTimeModelList;
    List<Model> mTimeModelList;
    DateTimeModel mDeliveryDateModel;
    TimeModel mDeliveryTimeModel;
    String []mDeliveryDates;
    String []mDeliveryTimes;
    IDateTimeDialogCallback mIDateTimeDialogCallback;

    public DateTimeDialog(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        initViews();
    }

    public void initViews() {
        mRootLayout = (LinearLayout)mInflater.inflate(R.layout.checkout_dialog_date_time, null);
        mDatePicker = (NumberPicker)mRootLayout.findViewById(R.id.picker_date);
        mDatePicker.setOnValueChangedListener(mOnDateValueChangeListener);
        mTimePicker = (NumberPicker)mRootLayout.findViewById(R.id.picker_time);
        mCancel = (TextView)mRootLayout.findViewById(R.id.cancel_action);
        mCancel.setOnClickListener(mOnCancelListener);
        mSelect = (TextView)mRootLayout.findViewById(R.id.select_action);
        mSelect.setOnClickListener(mOnSelectListener);
    }

    NumberPicker.OnValueChangeListener mOnDateValueChangeListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            int dateModelIndex = newVal;
            mDeliveryDateModel = (DateTimeModel)mDateTimeModelList.get(dateModelIndex);
            mTimeModelList = mDeliveryDateModel.getTimeModelList();
            mDeliveryTimes = null;
            mTimePicker.setDisplayedValues(null);
            updateTimePicker();
        }
    };



    View.OnClickListener mOnCancelListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismissDialog();
        }
    };

    View.OnClickListener mOnSelectListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mIDateTimeDialogCallback.onTimeSelected(mDatePicker.getValue(), mTimePicker.getValue());
            dismissDialog();
        }
    };

    private void createDateTimeString(boolean isDate) {
        if(isDate) {
            mDeliveryDates = new String[mDateTimeModelList.size()];
            for(int i=0; i<mDeliveryDates.length; i++) {
                mDeliveryDates[i] = mDateTimeModelList.get(i).getName();
            }
            mDeliveryTimes = null;
        } else {
            if(mTimeModelList != null) {
                mDeliveryTimes = new String[mTimeModelList.size()];
                for (int i = 0; i < mDeliveryTimes.length; i++) {
                    mDeliveryTimes[i] = mTimeModelList.get(i).getName();
                }
            }
        }
    }

    public void updateDatePicker() {
        createDateTimeString(TYPE_DATE);
        if (mDeliveryDates != null) {
            mDatePicker.setMinValue(0);
            mDatePicker.setMaxValue(mDeliveryDates.length - 1);
            mDatePicker.setValue(0);
            mDatePicker.setWrapSelectorWheel(false);
            mDatePicker.setDisplayedValues(mDeliveryDates);
        }
    }

    public void updateTimePicker() {
        createDateTimeString(TYPE_TIME);
        if(mDeliveryTimes != null) {
            mTimePicker.setMinValue(0);
            mTimePicker.setMaxValue(mDeliveryTimes.length - 1);
            mTimePicker.setValue(0);
            mTimePicker.setWrapSelectorWheel(false);
            mTimePicker.setDisplayedValues(mDeliveryTimes);
        }
    }

    public void setDateTimeList(List<Model> modelList) {
        if(modelList != null) {
            mDateTimeModelList = modelList;
            mDeliveryDateModel = (DateTimeModel)mDateTimeModelList.get(0);
            mTimeModelList = mDeliveryDateModel.getTimeModelList();
            updateDatePicker();
            updateTimePicker();
        }
    }

    public void showDialog() {
        if (mDialog == null) {
            createDialog();
        }
        mDialog.show();     //set Layout Params after dailog is shown
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mDialog.getWindow().setAttributes(lp);
    }

    public void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(mRootLayout);
        mDialog = builder.create();
    }

    public void dismissDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            //mDialog = null;
        }
    }

    public interface IDateTimeDialogCallback {
        void onTimeSelected(int dateModelIndex, int timeModelIndex);
    }

    public void setIDateTimeDialogCallback(IDateTimeDialogCallback iDateTimeDialogCallback) {
        mIDateTimeDialogCallback = iDateTimeDialogCallback;
    }
}
